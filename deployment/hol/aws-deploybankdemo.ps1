# project name to be passed in as a parameter to avoid duplication of resources and subsequent failures.
$PROJECTNAME=($args[0]).ToLower()
$USERARG=$args[1]
$GITBRANCH=(git rev-parse --abbrev-ref HEAD)
& {
Get-Date

#$env:path += 'C:\Users\user\.azure-kubectl'

# Install AWS CLI msi from https://s3.amazonaws.com/aws-cli/AWSCLI64PY3.msi

# Install EKSCTL for Windows - requires chocolatey

# Run aws configure with private key info from your account

if ("$PROJECTNAME" -eq "") {
  Write-Output "No project name passed as parameter"
  exit 1
}

$USERNUM=([int]$USERARG).ToString("00")
if (!($GITBRANCH -like "*$USERNUM")) {
    Write-Host "User number argument doesn't match users git branch $GITBRANCH"
    exit 1
}

& $PSScriptRoot\aws-configurecli.ps1

aws eks describe-cluster --name $PROJECTNAME
if ($LASTEXITCODE -ne "0") {
    Write-Output "Could not resolve cluster $PROJECTNAME"
    exit 1
}

& $PSScriptRoot\..\aws\aws-prepareyamls.ps1 $PROJECTNAME $USERNUM

& $PSScriptRoot\..\common\common-installbankdemo.ps1 aws $GITBRANCH-ns $PROJECTNAME

$serviceEndpoint = (kubectl get services ed80-bnkd-svc-mfdbfh-tn -o json | ConvertFrom-Json).status.loadBalancer.ingress[0].hostname

Write-Output "HostnameParameter
$serviceEndpoint" > $PSScriptRoot\..\..\Bankdemo\VuGen\Scripts\ScriptParameters.dat

Get-Date
}