cleanup_services () {
    kubectl get svc --all-namespaces | grep -v "EXTERNAL-IP" | while read line
    do
        namespace=$(echo "$line" | awk '{print $1}')
        serviceName=$(echo "$line" | awk '{print $2}')
        extip=$(echo "$line" | awk '{print $5}')
        if (echo $extip | grep -qv "<none>"); then
            echo "Deleting service $serviceName in namespace $namespace"
            kubectl delete service $serviceName -n $namespace
        fi
    done
}


set -x #echo on

export PROJECTNAME=`echo $1 | awk '{print tolower($0)}'`
export ROLE_ID=`curl http://169.254.169.254/latest/meta-data/iam/security-credentials`
./aws-configurecli.sh

#Running twice to make sure nothing left lying around
cleanup_services
sleep 10

reposNames=( "bankdemo" "openldap" "escwa" "es-metrics" "kubeproxy" "frontend" "microfocals" "microfocalsweb" "microfocus/entserver" "microfocus/entdevhub" "microfocus/vcdevhub" "microfocus/cobolserver" )
for i in "${reposNames[@]}"; do
    aws ecr delete-repository --repository-name "$PROJECTNAME-$i" --force
    export ERROR_CODE=$?
    if [ "$ERROR_CODE" != "0" ]; then
        echo "Repository $i creation failed"
    fi
done
../aws/aws-cleanup.sh $PROJECTNAME
