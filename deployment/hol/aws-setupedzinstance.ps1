$PROJECTNAME=$args[0]
$GITBRANCH=$args[1]
function createShortcut{
    param(
        [Parameter(Mandatory = $True)]
        [string]
        $href,

        [Parameter(Mandatory = $True)]
        [string]
        $label
    )
    $wshShell = New-Object -ComObject "WScript.Shell"
    $urlShortcut = $wshShell.CreateShortcut(
      (Join-Path $wshShell.SpecialFolders.Item("AllUsersDesktop") "$label.lnk")
    )
    $urlShortcut.TargetPath = "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"
    $urlShortcut.Arguments = $href
    $urlShortcut.Save()
}

& $PSScriptRoot\aws-configurecli.ps1

$masterIp = aws ec2 describe-instances --filters Name=tag:LabInstanceType,Values=ContJenkins Name=tag:LabId,Values=$PROJECTNAME --query 'Reservations[*].Instances[*].PrivateIpAddress' --output text
if ($masterIp -eq "") {
    Write-Host "Failed to retrieve master instance ID"
    exit 1
}
$JenkinsUri="http://" + $masterIp + ":8081"
createShortcut -href $JenkinsUri -label "Jenkins"

aws eks update-kubeconfig --name $($PROJECTNAME.toLower())
Start-Process -FilePath "kubectl" -ArgumentList "proxy"
createShortcut -href "http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#!/login" -label "Kubernetes Dashboard"

createShortcut -href "https://gitlab.com/mf-markchristie/hol-containerdemo/-/tree/$GITBRANCH" -label "Gitlab Repository"

$wshShell = New-Object -ComObject "WScript.Shell"
$urlShortcut = $wshShell.CreateShortcut(
    (Join-Path $wshShell.SpecialFolders.Item("AllUsersDesktop") "Git Repository.lnk")
)
$urlShortcut.TargetPath = "c:\Hol-ContainerDemo"
$urlShortcut.Save()

[System.Environment]::SetEnvironmentVariable("GIT_SSH", "C:\Windows\System32\OpenSSH\ssh.exe", "Machine")

Write-Output "Welcome to the Hands on Lab container demo environment.
Your git branch to use for development is $GITBRANCH.
To set up the cluster, start a powershell prompt and run:
cd c:\Hol-ContainerDemo\deployment\hol
.\aws-deploybankdemo $PROJECTNAME <User Number>

If running the Enterprise Server/Bankdemo lab, or if running the Visual COBOL/Micro Focals lab, run:

.\aws-deploymicrofocals $PROJECTNAME <User Number>

For example, if your username is SPEED01, then run the following:
.\aws-deploybankdemo $PROJECTNAME 1

Check the desktop for web shortcuts to jenkins and the kubernetes dashboard.
Below is the session token for logging in to the kubernetes dashboard:" > "C:\Users\Public\Desktop\Readme.txt"
Write-Output $(kubectl -n kube-system describe secret eks-admin-token-) >> "C:\Users\Public\Desktop\Readme.txt"

Add-Content -Path C:\windows\System32\drivers\etc\hosts. -Value "192.168.192.10    rhel-opsbridge rhel-opsbridge.localdomain localhost4 localhost4.localdomain4"

mkdir -path c:\tmp -ErrorAction SilentlyContinue
Set-Location c:\tmp
aws s3 cp s3://amc-hol-license-files/EDz.mflic .
Start-Process -FilePath "C:\Program Files (x86)\Common Files\SafeNet Sentinel\Sentinel RMS License Manager\WinNT\cesadmintool" -ArgumentList "-term install -f EDz.mflic" -Wait
