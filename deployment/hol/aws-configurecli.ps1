$ROLE_ID=(curl http://169.254.169.254/latest/meta-data/iam/security-credentials).Content
$ROLE=(curl "http://169.254.169.254/latest/meta-data/iam/security-credentials/$ROLE_ID").Content | ConvertFrom-Json
aws configure set aws_access_key_id $($ROLE.AccessKeyId)
aws configure set aws_secret_access_key $($ROLE.SecretAccessKey)
aws configure set aws_session_token $($ROLE.Token)