set -x #echo on

export PROJECTNAME=`echo $1 | awk '{print tolower($0)}'`
export REGION=$2
export ROLEID=$3
export USERCOUNT=$4

export SCRIPT_DIR=`pwd`
aws configure set output json
aws configure set region $REGION
$SCRIPT_DIR/aws-configurecli.sh

$SCRIPT_DIR/../aws/aws-preparecluster.sh $PROJECTNAME $USERCOUNT
export ERROR_CODE=$?

if [ "$ERROR_CODE" != "0" ]; then
    echo "Lab cluster preparation failed."
    set +x #echo on
    exit 1
fi

export adminBucket="amc-hol-docker-images"
export myAccount=$(aws sts get-caller-identity | jq -r '.Account')
export adminAccount='323977192729'
if [ "$myAccount" != "$adminAccount" ]; then
    export bucketName="$adminBucket-$myAccount"
    if [ "$REGION" != "us-east-1" ]; then
        extraArgs="--create-bucket-configuration LocationConstraint=$REGION" # AWS mandates this parameter, but refuses to accept it if it is set to us-east-1
    fi
    aws s3api create-bucket --bucket "$bucketName" $extraArgs #Might already exist

    aws s3 sync "s3://$adminBucket" "s3://$bucketName"
    export ERROR_CODE=$?
    if [ "$ERROR_CODE" != "0" ]; then
        echo "Failed to synchronise s3 buckets!"
        exit 2
    fi
else
    export bucketName=$adminBucket
fi

mkdir /home/administrator/dockerimages
cd /home/administrator/dockerimages
export objects=`aws s3api list-objects --bucket $bucketName`
export ERROR_CODE=$?
if [ "$ERROR_CODE" != "0" ]; then
    echo "Failed to load objects"
    exit 3
fi

for k in $(echo $objects | jq -r '.Contents[] | @base64' ); do
    key=`echo $k | base64 --decode | jq -r '.Key'`
    aws s3 cp s3://$bucketName/$key .
    docker load -i $key
    export ERROR_CODE=$?
    if [ "$ERROR_CODE" != "0" ]; then
        echo "Failed to load docker image $key"
        exit 4
    fi
    rm $key
done

cd -
rm -rf /home/administrator/dockerimages

reposNames=( "bankdemo" "openldap" "escwa" "es-metrics" "kubeproxy" "frontend" "microfocals" "microfocalsweb" "microfocus/entserver" "microfocus/entdevhub" "microfocus/vcdevhub" "microfocus/cobolserver" )
for i in "${reposNames[@]}"; do
    aws ecr create-repository --repository-name "$PROJECTNAME-$i"
    export ERROR_CODE=$?
    if [ "$ERROR_CODE" != "0" ]; then
        echo "Repository $i creation failed"
        set +x #echo on
        exit 5
    fi
done

$(aws ecr get-login --no-include-email)
export ERROR_CODE=$?

if [ "$ERROR_CODE" != "0" ]; then
    echo "Docker login failed!"
    set +x #echo on
    exit 6
fi

export reposUri=$(aws ecr describe-repositories --repository-names $PROJECTNAME-bankdemo | jq -r '.repositories[0].repositoryUri' | sed s/$PROJECTNAME-bankdemo//)
docker images | grep -v "login" | grep -v "$reposUri" | grep -v "IMAGE ID" | while read line
do
    name=$(echo "$line" | awk '{print $1}')
    tag=$(echo "$line" | awk '{print $2}')
    docker tag $name:$tag $reposUri$PROJECTNAME-$name
    docker push $reposUri$PROJECTNAME-$name
    if [ "$ERROR_CODE" != "0" ]; then
        echo "Failed to push $reposUri$PROJECTNAME-$name"
        exit 6
    fi
done


aws cloudformation describe-stacks --stack-name "count-macro"
export ERROR_CODE=$?

if [ "$ERROR_CODE" != "0" ]; then
    echo "Count macro doesn't exist, creating now"
    cd /home/administrator
    git clone https://github.com/awslabs/aws-cloudformation-templates.git
    cd ./aws-cloudformation-templates/aws/services/CloudFormation/MacrosExamples/Count

    bucketName="count-macro-$myAccount"
    aws s3api delete-bucket --bucket $bucketName
    if [ "$REGION" != "us-east-1" ]; then
        extraArgs="--create-bucket-configuration LocationConstraint=$REGION" # AWS mandates this parameter, but refuses to accept it if it is set to us-east-1
    fi
    aws s3api create-bucket --bucket $bucketName $extraArgs #Might already exist

    aws cloudformation package --template-file template.yaml --s3-bucket $bucketName --output-template-file packaged.yaml
    aws cloudformation deploy --stack-name "count-macro" --template-file packaged.yaml  --capabilities CAPABILITY_IAM
    
    aws s3api delete-bucket --bucket $bucketName
    cd -
fi

echo "Getting ROLE ARN for $ROLEID"
ROLEARN=`aws iam get-role --role-name $ROLEID | jq -r '.Role.Arn'`
eksctl create iamidentitymapping --cluster $PROJECTNAME --arn $ROLEARN --group system:masters

#deploy metrics server on eks
#DOWNLOAD_URL=$(curl -Ls "https://api.github.com/repos/kubernetes-sigs/metrics-server/releases/latest" | jq -r .tarball_url)
#DOWNLOAD_VERSION=$(grep -o '[^/v]*$' <<< $DOWNLOAD_URL)
#curl -Ls $DOWNLOAD_URL -o metrics-server-$DOWNLOAD_VERSION.tar.gz
#mkdir metrics-server-$DOWNLOAD_VERSION
#tar -xzf metrics-server-$DOWNLOAD_VERSION.tar.gz --directory metrics-server-$DOWNLOAD_VERSION --strip-components 1
#kubectl apply -f metrics-server-$DOWNLOAD_VERSION/deploy/1.8+/
kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
kubectl get deployment metrics-server -n kube-system

#deploy prometheus using Helm
kubectl create namespace prometheus
#helm repo add stable https://kubernetes-charts.storage.googleapis.com
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
cd $SCRIPT_DIR/../../metrics
#helm install prometheus stable/prometheus --namespace prometheus --set alertmanager.persistentVolume.storageClass="gp2",server.persistentVolume.storageClass="gp2" --set-file extraScrapeConfigs=extraScrapeConfigs.yaml
#helm install prometheus stable/prometheus --namespace prometheus --set-file extraScrapeConfigs=extraScrapeConfigs.yaml
helm upgrade -i prometheus prometheus-community/prometheus --namespace prometheus --set-file extraScrapeConfigs=extraScrapeConfigs.yaml
kubectl wait --for=condition=ready --namespace prometheus --all=true pod --timeout=100s

#kubectl apply --validate=false -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.11/deploy/manifests/00-crds.yaml
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.5.3/cert-manager.yaml
kubectl create namespace cert-manager
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install com-microfocus-certificates --namespace cert-manager --version v1.8.2 jetstack/cert-manager

kubectl create secret tls ca-key-pair --cert=$SCRIPT_DIR/../../certificates/ca.crt --key=$SCRIPT_DIR/../../certificates/ca.key
kubectl wait --for=condition=ready --namespace cert-manager --all=true pod

count=0
until kubectl apply -f $SCRIPT_DIR/../tls-certificates.yaml > /dev/null; do
    count=$((count+1))
    if [ $count -ge 30 ]; then
        echo "Exceeded maximum number of retries"
        exit 7
    fi
    sleep 3
done

#deploy prometheus adapter using helm
#helm install -f $SCRIPT_DIR/../../metrics/adaptervalues.yaml test-release stable/prometheus-adapter --set prometheus.url=http://prometheus-server.prometheus.svc --set prometheus.port=80 --set logLevel=6
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm install -f $SCRIPT_DIR/../../metrics/adaptervalues.yaml test-release prometheus-community/prometheus-adapter --set prometheus.url=http://prometheus-server.prometheus.svc --set prometheus.port=80 --set logLevel=6
kubectl wait --for=condition=ready --all=true pod --timeout=60s

#Install the kubernetes dashboard so the pod usage can be visualised
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.5.0/aio/deploy/recommended.yaml
kubectl apply -f $SCRIPT_DIR/../eks-admin-service-account.yaml

#Use the token from the following command to log in (or use the config file from the users .kube location)
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep eks-admin | awk '{print $1}')

#Run the following command to allow access to kubernetes dashboard
#kubectl proxy

#Open a browser Window to the following URL and enter the token that is displayed during the dashboard deploy
#http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#!/login

#########################################################

set +x #echo on