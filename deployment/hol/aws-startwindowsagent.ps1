$PROJECTNAME=$args[0]

& $PSScriptRoot\aws-configurecli.ps1

$masterIp = aws ec2 describe-instances --filters Name=tag:LabInstanceType,Values=ContJenkins Name=tag:LabId,Values=$PROJECTNAME --query 'Reservations[*].Instances[*].PrivateIpAddress' --output text
if ($masterIp -eq "") {
    Write-Host "Failed to retrieve master instance ID"
    exit 1
}

$JenkinsUri="http://" + $masterIp + ":8081"
Write-Host "Connecting to Jenkins endpoint $JenkinsUri"
$waitCount = 0
while ($waitCount -le 60) {
    Invoke-WebRequest -Uri "$JenkinsUri/jnlpJars/agent.jar" -OutFile "agent.jar"
    if (Test-Path "agent.jar") {
        break
    }
    Start-sleep -Seconds 20
    $waitCount += 1
}

if (!(Test-Path "agent.jar")) {
    Write-Host "Timeout waiting for jenkins host"
    exit 2
}

Start-Process -FilePath java -ArgumentList "-jar .\agent.jar -jnlpUrl $JenkinsUri/computer/Win2019Host/slave-agent.jnlp"