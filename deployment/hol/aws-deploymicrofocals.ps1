# project name to be passed in as a parameter to avoid duplication of resources and subsequent failures.
$PROJECTNAME=($args[0]).ToLower()
$USERARG=$args[1]
$GITBRANCH=(git rev-parse --abbrev-ref HEAD)
& {
Get-Date

#$env:path += 'C:\Users\user\.azure-kubectl'

# Install AWS CLI msi from https://s3.amazonaws.com/aws-cli/AWSCLI64PY3.msi

# Install EKSCTL for Windows - requires chocolatey

# Run aws configure with private key info from your account

if ("$PROJECTNAME" -eq "") {
  Write-Output "No project name passed as parameter"
  exit 1
}

$USERNUM=([int]$USERARG).ToString("00")
if (!($GITBRANCH -like "*$USERNUM")) {
    Write-Host "User number argument doesn't match users git branch $GITBRANCH"
    exit 1
}

& $PSScriptRoot\aws-configurecli.ps1

aws eks describe-cluster --name $PROJECTNAME
if ($LASTEXITCODE -ne "0") {
    Write-Output "Could not resolve cluster $PROJECTNAME"
    exit 1
}

Remove-Item -Path $PSScriptRoot\..\microfocals_deploy -Recurse -ErrorAction Ignore
mkdir $PSScriptRoot\..\microfocals_deploy
$Prefix = "$PROJECTNAME-"
$RegistryUrl = $(aws ecr describe-repositories --repository-names "$($Prefix)escwa" | ConvertFrom-Json).repositories[0].repositoryUri -replace "/$($Prefix)escwa", ""

((Get-Content -path $PSScriptRoot\..\..\escwa\escwa.yaml -Raw) -creplace "<registry_url>/","$RegistryUrl/$Prefix") | Set-Content -Path $PSScriptRoot\..\microfocals_deploy\cloudp-escwa.yaml
((Get-Content -path $PSScriptRoot\..\backend_microfocals.yaml -Raw) -creplace "<registry_url>/","$RegistryUrl/$Prefix") | Set-Content -Path $PSScriptRoot\..\microfocals_deploy\cloudp-backend_microfocals.yaml
((Get-Content -path $PSScriptRoot\..\openldap.yaml -Raw) -creplace "<registry_url>/","$RegistryUrl/$Prefix") | Set-Content -Path $PSScriptRoot\..\microfocals_deploy\cloudp-openldap.yaml
((Get-Content -path $PSScriptRoot\..\frontend_microfocals.yaml -Raw) -creplace "<registry_url>/","$RegistryUrl/$Prefix") | Set-Content -Path $PSScriptRoot\..\microfocals_deploy\cloudp-frontend_microfocals.yaml

& $PSScriptRoot\..\common\common-installmicrofocals.ps1 $PROJECTNAME $GITBRANCH

Get-Date
}