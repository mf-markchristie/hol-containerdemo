set -x #echo on
export PROJECTNAME=`echo $1 | awk '{print tolower($0)}'`
./aws-configurecli.sh
$(aws ecr get-login --no-include-email)
export ERROR_CODE=$?

if [ "$ERROR_CODE" != "0" ]; then
    echo "Docker login failed!"
    set +x #echo on
    exit 1
fi

cd ../../jenkins

export imageUrl=$(aws ecr describe-repositories --repository-names $PROJECTNAME-microfocus/entserver | jq -r '.repositories[0].repositoryUri')
if [ "$imageUrl" == "" ]; then
    echo "Failed to locate registry location."
    set +x #echo on
    exit 2
fi
docker pull $imageUrl
docker tag $imageUrl microfocus/entserver:sles15.1_8.0_x64
export registryUrl=$(echo $imageUrl | sed s~/$PROJECTNAME-microfocus/entserver~~)
export imageUrl="$registryUrl/$PROJECTNAME-microfocus/entdevhub"
docker pull $imageUrl
docker tag $imageUrl microfocus/entdevhub:sles15.1_8.0_x64

export imageUrl="$registryUrl/$PROJECTNAME-microfocus/cobolserver"
docker pull $imageUrl
docker tag $imageUrl microfocus/cobolserver:sles15.1_8.0_x64
export imageUrl="$registryUrl/$PROJECTNAME-microfocus/vcdevhub"
docker pull $imageUrl
docker tag $imageUrl microfocus/vcdevhub:sles15.1_8.0_x64


sed -i "s/#registry#/$registryUrl/" ./configuration/jobs/Build_Hol_BankDemo/config.xml
sed -i "s/#registry#/$registryUrl/" ./configuration/jobs/Build_Hol_MicroFocals/config.xml
sed -i "s/#registry#/$registryUrl/" ./configuration/jobs/Build_Hol_Escwa/config.xml
sed -i "s/#reposprefix#/$PROJECTNAME-/" ./configuration/jobs/Build_Hol_BankDemo/config.xml
sed -i "s/#reposprefix#/$PROJECTNAME-/" ./configuration/jobs/Build_Hol_MicroFocals/config.xml
sed -i "s/#reposprefix#/$PROJECTNAME-/" ./configuration/jobs/Build_Hol_Escwa/config.xml
./startjenkins.sh

crontab -l >/tmp/crontab
echo '0 * * * * /home/administrator/hol-containerdemo/deployment/hol/aws-configurecli.sh; $(aws ecr get-login --no-include-email)' >> /tmp/crontab
crontab /tmp/crontab
export ERROR_CODE=$?

set +x #echo on
exit $ERROR_CODE