export ROLE_ID=`curl http://169.254.169.254/latest/meta-data/iam/security-credentials`
export ROLE=`curl http://169.254.169.254/latest/meta-data/iam/security-credentials/$ROLE_ID`
aws configure set aws_access_key_id `echo $ROLE | jq -r '.AccessKeyId'`
aws configure set aws_secret_access_key `echo $ROLE | jq -r '.SecretAccessKey'`
aws configure set aws_security_token `echo $ROLE | jq -r '.Token'`
aws configure set aws_session_token `echo $ROLE | jq -r '.Token'`