# project name to be passed in as a parameter to avoid duplication of resources and subsequent failures.
$CLOUDNAME=($args[0]).ToLower()
$PROJECTNAME=($args[1]).ToLower()
& {
Get-Date

#Install Helm if not available
try {
    helm.exe version
} catch {
    #Helm isn't available, so downloading Helm
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
    Invoke-WebRequest -Uri https://get.helm.sh/helm-v3.9.0-windows-amd64.zip -OutFile $env:TEMP\helm.zip
    Expand-Archive -force $env:TEMP\helm.zip $env:TEMP\helm\
    $env:path += ";$($env:HOME)\.azure-kubectl;$($env:TEMP)\helm\windows-amd64"
}

#Install kubectl if not available
try {
    kubectl.exe version --client
} catch {
    #kubectl isn't available, so downloading kubectl
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
    Invoke-WebRequest -Uri https://storage.googleapis.com/kubernetes-release/release/v1.18.0/bin/windows/amd64/kubectl.exe -OutFile $env:TEMP\kubectl.exe
    $env:path += ";$($env:TEMP)"
}

if ("$PROJECTNAME" -eq "") {
  if ( "$CLOUDNAME" -ne "azure") {
    if ( "$CLOUDNAME" -ne "kind") {
      Write-Output "No project name passed as parameter"
      exit 1
    }
  }
}

Remove-Item $PSScriptRoot\..\aws\metrics*zip
Get-ChildItem -Path $PSScriptRoot\..\aws\metrics* -Recurse | Remove-Item -force -recurse
Remove-Item $PSScriptRoot\..\aws\metrics* -force
Remove-Item $PSScriptRoot\..\cloudp*yaml
Remove-Item $PSScriptRoot\..\aws\aws*yaml
Remove-Item $PSScriptRoot\..\aws*yaml
Remove-Item $PSScriptRoot\..\azure\az*yaml
Remove-Item $PSScriptRoot\..\az*yaml
Remove-Item $PSScriptRoot\..\..\escwa\cloudp*yaml

Push-Location $PSScriptRoot\..\$CLOUDNAME
& .\$CLOUDNAME-preparecluster.ps1 $PROJECTNAME
if ($LASTEXITCODE -ne "0") {
    exit $LASTEXITCODE
}
Pop-Location

Push-Location $PSScriptRoot\..\$CLOUDNAME
& .\$CLOUDNAME-prepareyamls.ps1 $PROJECTNAME 01
if ($LASTEXITCODE -ne "0") {
    exit $LASTEXITCODE
}
Pop-Location

#deploy prometheus using Helm
kubectl create namespace prometheus
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
Push-Location $PSScriptRoot\..\..\metrics
#original command picked up from AWS documentation
#helm install prometheus stable/prometheus --namespace prometheus --set alertmanager.persistentVolume.storageClass="gp2",server.persistentVolume.storageClass="gp2" --set-file extraScrapeConfigs=extraScrapeConfigs.yaml
#helm install prometheus stable/prometheus --namespace prometheus
helm install prometheus prometheus-community/prometheus --namespace prometheus
Pop-Location
kubectl wait --for=condition=ready --timeout=300s --namespace prometheus --all=true pod
if ($LASTEXITCODE -ne "0") {
    Write-Host "Prometheus failed to start. Exit code $LASTEXITCODE"
    exit 1
}

# kubectl apply --validate=false -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.11/deploy/manifests/00-crds.yaml
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.5.3/cert-manager.yaml
kubectl create namespace cert-manager
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install com-microfocus-certificates --namespace cert-manager --version v1.8.2 jetstack/cert-manager

kubectl create secret tls ca-key-pair --cert=$PSScriptRoot\..\..\certificates\ca.crt --key=$PSScriptRoot\..\..\certificates\ca.key --namespace=default
kubectl wait --for=condition=ready --timeout=300s --namespace cert-manager --all=true pod
do { Start-sleep -Seconds 3 } until (kubectl apply -f $PSScriptRoot\..\tls-certificates.yaml | Where-Object { $? } )
if ($LASTEXITCODE -ne "0") {
    Write-Host "Cert-manager failed to start. Exit code $LASTEXITCODE"
    exit 1
}

#deploy prometheus adapter using helm
helm install -f $PSScriptRoot\..\..\metrics\adaptervalues.yaml test-release prometheus-community/prometheus-adapter --set prometheus.url=http://prometheus-server.prometheus.svc --set prometheus.port=80 --set logLevel=6
kubectl wait --for=condition=ready --timeout=300s --all=true pod
if ($LASTEXITCODE -ne "0") {
    Write-Host "Prometheus adapter failed to start. Exit code $LASTEXITCODE"
    exit 1
}

#Install the kubernetes dashboard so the pod usage can be visualised
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.5.0/aio/deploy/recommended.yaml
kubectl apply -f $PSScriptRoot\..\eks-admin-service-account.yaml
kubectl apply -f $PSScriptRoot\..\dashboard-service-account.yaml

$kubeadmintoken = ($(kubectl -n kube-system get secret) | Select-String eks-admin) -split ' '
#Use the token from the following command to log in (or use the config file from the users .kube location)
kubectl -n kube-system describe secret $kubeadmintoken[0]

#Run the following command to allow access to kubernetes dashboard
#kubectl proxy

#Open a browser Window to the following URL and enter the token that is displayed during the dashboard deploy
#http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#!/login

switch ( "$CLOUDNAME" )
{
  "aws"
  {
    $NAMESPACE="user-ns"
    break
  }
  "azure"
  {
    $NAMESPACE="default"
    break
  }
  "kind"
  {
    $NAMESPACE="default"
    break
  }
}

& $PSScriptRoot\..\common\common-installbankdemo.ps1 $CLOUDNAME $NAMESPACE $PROJECTNAME

Get-Date
}