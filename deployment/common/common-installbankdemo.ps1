$CLOUDNAME=($args[0]).ToLower()
$NSPACE=($args[1]).ToLower()
$PROJECTNAME=($args[2]).ToLower()
& {
Get-Date

kubectl create namespace $NSPACE
kubectl config set-context --current --namespace=$NSPACE

kubectl apply -f $PSScriptRoot\..\bankdemo_deploy\cloudp-secrets.yaml

switch ( "$CLOUDNAME" )
{
  "aws"
  {
    kubectl apply -f $PSScriptRoot\..\bankdemo_deploy\cloudp-redis-service-deploy.yaml
    break
  }
  "azure"
  {
    kubectl apply -f $PSScriptRoot\..\redis.yaml
    break
  }
  "kind"
  {
    kubectl apply -f $PSScriptRoot\..\redis.yaml

	# Deploy local database
	kubectl apply -f https://raw.githubusercontent.com/rancher/local-path-provisioner/master/deploy/local-path-storage.yaml
    kubectl apply -f $PSScriptRoot\..\local_pvclaim.yaml
	kubectl apply -f $PSScriptRoot\..\espgsql.yaml
	kubectl wait --for=condition=Ready --timeout=600s --selector='app=postgres' pods
    break
  }
}

#apply certificates from certificates directory (see readme.txt)
kubectl create secret tls ca-key-pair --cert=$PSScriptRoot\..\..\certificates\ca.crt --key=$PSScriptRoot\..\..\certificates\ca.key
kubectl wait --for=condition=ready --namespace cert-manager --all=true pod
do { Start-sleep -Seconds 3 } until (kubectl apply -f $PSScriptRoot\..\tls-certificates.yaml | Where-Object { $? } )

kubectl create secret generic mfdbfh-config --from-file=MFDBFH.cfg=$PSScriptRoot\..\bankdemo_deploy\cloudp-MFDBFH.cfg

kubectl apply -f $PSScriptRoot\..\bankdemo_deploy\cloudp-pgsql-service-deploy.yaml

#initialise db
kubectl create configmap ed80-bnkd-odbc-config   --from-file=odbc.ini=$PSScriptRoot\..\config\odbc.ini

kubectl wait --for=condition=Ready --timeout=600s --selector='app=ed80-bnkd-mfdbfh' pods
if ($LASTEXITCODE -ne "0") {
  kubectl apply -f $PSScriptRoot\..\bankdemo_deploy\cloudp-initdb.yaml
  kubectl wait --for=condition=Complete --timeout=600s --selector='job-name=init-mfdbfh' job
  if ($LASTEXITCODE -ne "0") {
    Write-Output initdbfh job failed, exiting
    exit 1
  }
  kubectl delete job init-mfdbfh
}

$namespaceString='"Namespace":"default"'
$newNamespaceString='"Namespace":"' + "$NSPACE" + '"'
Move-Item $PSScriptRoot\..\bankdemo_deploy\cloudp-escwa.yaml $PSScriptRoot\..\bankdemo_deploy\cloudp-escwa1.yaml
((Get-Content -path $PSScriptRoot\..\bankdemo_deploy\cloudp-escwa1.yaml -Raw) -creplace $namespaceString,$newNamespaceString) | Set-Content -Path $PSScriptRoot\..\bankdemo_deploy\cloudp-escwa.yaml

Remove-Item -path $PSScriptRoot\..\bankdemo_deploy\cloudp-escwa1.yaml

#run az-demo.bat
kubectl apply -f $PSScriptRoot\..\rolebinding.yaml
kubectl apply -f $PSScriptRoot\..\bankdemo_deploy\cloudp-openldap.yaml
kubectl apply -f $PSScriptRoot\..\syslog.yaml
kubectl wait --for=condition=ready --all=true pod --timeout=180s
kubectl create configmap es-syslog-config --from-file=rsyslog.conf=$PSScriptRoot\..\config\rsyslog.conf
kubectl apply -f $PSScriptRoot\..\bankdemo_deploy\cloudp-backend_bankdemo.yaml
kubectl apply -f $PSScriptRoot\..\hpa_bankdemo.yaml
kubectl apply -f $PSScriptRoot\..\bankdemo_deploy\cloudp-escwa.yaml
kubectl apply -f $PSScriptRoot\..\bankdemo_deploy\cloudp-frontend_bankdemo.yaml

kubectl get all

Get-Date
}