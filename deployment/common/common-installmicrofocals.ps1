$PROJECTNAME=($args[0]).ToLower()
$NSPACE=($args[1]).ToLower()
& {
Get-Date

kubectl create namespace $NSPACE-ns
kubectl config set-context --current --namespace=$NSPACE-ns

#apply certificates from certificates directory (see readme.txt)
kubectl create secret tls ca-key-pair --cert=$PSScriptRoot\..\..\certificates\ca.crt --key=$PSScriptRoot\..\..\certificates\ca.key
kubectl wait --for=condition=ready --namespace cert-manager --all=true pod
do { Start-sleep -Seconds 3 } until (kubectl apply -f $PSScriptRoot\..\tls-certificates.yaml | Where-Object { $? } )

#cd to deployment directory and apply az-secrets.yaml
kubectl apply -f $PSScriptRoot\..\secrets.yaml

#initialise db
$namespaceString='"Namespace":"default"'
$newNamespaceString='"Namespace":"' + "$NSPACE-ns" + '"'
Move-Item $PSScriptRoot\..\microfocals_deploy\cloudp-escwa.yaml $PSScriptRoot\..\microfocals_deploy\cloudp-escwa1.yaml
((Get-Content -path $PSScriptRoot\..\microfocals_deploy\cloudp-escwa1.yaml -Raw) -creplace $namespaceString,$newNamespaceString) | Set-Content -Path $PSScriptRoot\..\microfocals_deploy\cloudp-escwa.yaml

Remove-Item -path $PSScriptRoot\..\microfocals_deploy\cloudp-escwa1.yaml

#run az-demo.bat
kubectl apply -f $PSScriptRoot\..\rolebinding.yaml
kubectl apply -f $PSScriptRoot\..\microfocals_deploy\cloudp-openldap.yaml
kubectl apply -f $PSScriptRoot\..\syslog.yaml
kubectl wait --for=condition=ready --all=true pod --timeout=180s
kubectl create configmap es-syslog-config --from-file=rsyslog.conf=$PSScriptRoot\..\config\rsyslog.conf
kubectl apply -f $PSScriptRoot\..\hpa_microfocals.yaml
kubectl apply -f $PSScriptRoot\..\microfocals_deploy\cloudp-escwa.yaml

kubectl get all

Get-Date
}