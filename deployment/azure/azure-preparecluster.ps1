# project name to be passed in as a parameter to avoid duplication of resources and subsequent failures.
$PROJECTNAME=($args[0]).ToLower()
$USERCOUNTARG=$args[1]
& {
Get-Date

#if ("$PROJECTNAME" -eq "") {
#  Write-Output "No project name passed as parameter"
#  exit 1
#}

$USERCOUNT = [int]$USERCOUNTARG
if ("$USERCOUNT" -eq 0) {
  $USERCOUNT = 1
}

$NODECOUNTCALC=[int]($USERCOUNT / 2 + 1)

#Read in settings values from side text file (settings.txt)
$configSettings = Get-Content .\az-settings.json | ConvertFrom-Json
Write-Output $configSettings.Azure

$subscription = az account show | ConvertFrom-Json
$subscriptionId = $subscription.id

$NODECOUNTOVERRIDE = $($configSettings.Azure.Kubernetes.NumberOfNodes)

$NODECOUNT = $NODECOUNTCALC
if ("$NODECOUNTOVERRIDE" -gt "$NODECOUNTCALC") {
  $NODECOUNT = $NODECOUNTOVERRIDE
}
Write-Output "Node Count is: $($NODECOUNT)"

# This section creates a new resource group for the application to run under, and creates a new kubernetes cluster:
try {
    az group create --location $($configSettings.Azure.Kubernetes.AzLocation) --name $($configSettings.Azure.Kubernetes.AppResourceGroup)

    #Create new Kube cluster and configure the local kubectl instance to work with it.

    if ($($configSettings.Azure.DatabaseConfig.DBSkuName) -eq "B_Gen5_1") {
        Write-Output "Basic Database types do not support extended vnet configurations"
    } else {
        az network vnet create --resource-group $($configSettings.Azure.Kubernetes.AppResourceGroup) --name $($configSettings.Azure.NetworkSettings.AzVnetName) --address-prefixes 192.168.0.0/16 -l $($configSettings.Azure.Kubernetes.AzLocation)
        az network vnet subnet create --resource-group $($configSettings.Azure.Kubernetes.AppResourceGroup) --name $($configSettings.Azure.NetworkSettings.AzSubnetName) --vnet-name $($configSettings.Azure.NetworkSettings.AzVnetName)  --address-prefix 192.168.1.0/24 --service-endpoints Microsoft.SQL
        $SUBNET_ID=$(az network vnet subnet show --resource-group $($configSettings.Azure.Kubernetes.AppResourceGroup) --vnet-name $($configSettings.Azure.NetworkSettings.AzVnetName) --name $($configSettings.Azure.NetworkSettings.AzSubnetName) --query id -o tsv)
        $NETWORK_CONFIG="--vnet-subnet-id $($SUBNET_ID) --service-cidr 10.0.0.0/16 --network-plugin kubenet --dns-service-ip 10.0.0.10 --pod-cidr 10.244.0.0/16 --docker-bridge-address 172.17.0.1/16"
        $NETWORK_OPTIONS=$NETWORK_CONFIG.Split(" ")
    }

    #Create a monitoring workspace the name is unique using your subscription ID - each name must be unique, so this allows only one per subscription, also uses day of year as workspaces are soft-delete
    $dayOfYear = $(GET-DATE).DayOfYear
    $monitorWorkspaceName = "$($configSettings.Azure.Kubernetes.AppResourceGroup)-ws-$($subscriptionId)-$($dayOfYear)"
    $curForeground = $($host.ui.rawui.foregroundcolor)
    $workspaceSettings = $(az monitor log-analytics workspace create --resource-group $($configSettings.Azure.Kubernetes.AppResourceGroup) --workspace-name $($monitorWorkspaceName) | ConvertFrom-Json)
    $host.ui.rawui.foregroundcolor = $($curForeground)

    #Create the cluster
    az aks create --resource-group $($configSettings.Azure.Kubernetes.AppResourceGroup) --name $($configSettings.Azure.Kubernetes.KubernetesClusterName) --kubernetes-version $($configSettings.Azure.Kubernetes.KubernetesVersion) --location $($configSettings.Azure.Kubernetes.AzLocation) --node-count $NODECOUNT --node-osdisk-size $($configSettings.Azure.Kubernetes.NodeOsDiskSize) --node-vm-size $($configSettings.Azure.Kubernetes.NodeVMSize) --generate-ssh-keys $($NETWORK_OPTIONS) --enable-addons monitoring --workspace-resource-id $($workspaceSettings.id)

    #Get the credentials from the newly created cluster
    az aks get-credentials --resource-group $($configSettings.Azure.Kubernetes.AppResourceGroup) --name $($configSettings.Azure.Kubernetes.KubernetesClusterName) --overwrite-existing
} catch {
    Write-Output "Failure creating cluster."
    exit 1
}

#Create a database name so that it is unique using your subscription ID - each name must be unique, so this allows only one per subscription.
$dbName = "$($configSettings.Azure.Kubernetes.AppResourceGroup)-db-$subscriptionId"
Write-Output "Database name is:" $dbName

#create postgres server
az postgres server create --name $dbName --resource-group $($configSettings.Azure.Kubernetes.AppResourceGroup) --admin-user $($configSettings.Azure.DatabaseConfig.DBAdminUser) --admin-password $($configSettings.Azure.DatabaseConfig.DBAdminPassword) --sku-name $($configSettings.Azure.DatabaseConfig.DBSkuName) --ssl-enforcement Enabled --version $($configSettings.Azure.DatabaseConfig.DBVersion)
if ($LASTEXITCODE -ne "0") {
    Write-Output "Failure creating Postgres Database"
}

if ($($configSettings.Azure.DatabaseConfig.DBSkuName) -eq "B_Gen5_1") {
    #Update security settings for db to allow access from cluster
    #Note - basic postgress databases are accessible from all Azure instances, do not use this in production
    az postgres server firewall-rule create --resource-group $($configSettings.Azure.Kubernetes.AppResourceGroup) --server-name $dbName --name AllowAllWindowsAzureIps --start-ip-address 0.0.0.0 --end-ip-address 0.0.0.0
} else {
    # Use the commands below as an alternative to those above to secure the database to just the bankdemo application
    # see https://docs.microsoft.com/en-us/azure/postgresql/concepts-firewall-rules and https://docs.microsoft.com/en-us/azure/postgresql/howto-manage-vnet-using-cli
    Write-Output "Applying firewall settings"
    az postgres server vnet-rule create --name bankdemo-rule --resource-group $($configSettings.Azure.Kubernetes.AppResourceGroup) --server-name $dbName --vnet-name $($configSettings.Azure.NetworkSettings.AzVnetName) --subnet $($configSettings.Azure.NetworkSettings.AzSubnetName)
}

$AzRegistryPasswordObj = (az acr credential show --name $($configSettings.Azure.UserConfig.AzContainerRegistry)) | ConvertFrom-Json
if ($LASTEXITCODE -ne "0") {
    # Look for local override file containing the credentials required to access the container registry
	$RegistryCredentialSettings = Get-Content $(-join(".\", $($configSettings.Azure.UserConfig.AzContainerRegistry), ".json")) | ConvertFrom-Json
	Write-Output $RegistryCredentialSettings
    $AzRegistryPassword = $RegistryCredentialSettings.UserConfig.AzRegistryPassword
    $AzContainerUserName = $RegistryCredentialSettings.UserConfig.AzContainerUserName
} else {
    $AzRegistryPassword = $AzRegistryPasswordObj.passwords[0].value
    $AzContainerUserName = $AzRegistryPasswordObj.username
}
#Create secret for the container registry based on the values held in the settings config file.
kubectl create secret docker-registry $($configSettings.Azure.UserConfig.AzContainerRegistry) --docker-server=$($configSettings.Azure.UserConfig.AzContainerRegistryServer) --docker-username=$AzContainerUserName --docker-password=$AzRegistryPassword --docker-email=$($configSettings.Azure.UserConfig.AzContainerUserEmail)

Get-Date
}