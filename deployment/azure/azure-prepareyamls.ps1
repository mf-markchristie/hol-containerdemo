# project name to be passed in as a parameter to avoid duplication of resources and subsequent failures.
$PROJECTNAME=($args[0]).ToLower()
& {
 Get-Date

 #Read in settings values from side text file (settings.txt)
$configSettings = Get-Content .\az-settings.json | ConvertFrom-Json
Write-Output $configSettings

$subscription = az account show | ConvertFrom-Json
$subscriptionId = $subscription.id

 Remove-Item -Path $PSScriptRoot\..\bankdemo_deploy -Recurse -ErrorAction Ignore
 mkdir $PSScriptRoot\..\bankdemo_deploy

 $RegistryUrl = "$($configSettings.Azure.UserConfig.AzContainerRegistryServer)"
 $RegistryName = "$($configSettings.Azure.UserConfig.AzContainerRegistry)"
 
 ((Get-Content -path $PSScriptRoot\..\..\escwa\escwa.yaml -Raw) -creplace "<registry_url>","$RegistryUrl" -creplace "<registry_name>","$RegistryName") | Set-Content -Path $PSScriptRoot\..\bankdemo_deploy\cloudp-escwa.yaml
 ((Get-Content -path $PSScriptRoot\..\openldap.yaml -Raw) -creplace "<registry_url>","$RegistryUrl" -creplace "<registry_name>","$RegistryName") | Set-Content -Path $PSScriptRoot\..\bankdemo_deploy\cloudp-openldap.yaml
 ((Get-Content -path $PSScriptRoot\..\initdb.yaml -Raw) -creplace "<registry_url>","$RegistryUrl" -creplace "<registry_name>","$RegistryName") | Set-Content -Path $PSScriptRoot\..\bankdemo_deploy\cloudp-initdb.yaml
 ((Get-Content -path $PSScriptRoot\..\backend_bankdemo.yaml -Raw) -creplace "<registry_url>","$RegistryUrl" -creplace "<registry_name>","$RegistryName") | Set-Content -Path $PSScriptRoot\..\bankdemo_deploy\cloudp-backend_bankdemo.yaml
 ((Get-Content -path $PSScriptRoot\..\frontend_bankdemo.yaml -Raw) -creplace "<registry_url>","$RegistryUrl" -creplace "<registry_name>","$RegistryName") | Set-Content -Path $PSScriptRoot\..\bankdemo_deploy\cloudp-frontend_bankdemo.yaml
 
 $dbName = "$($configSettings.Azure.Kubernetes.AppResourceGroup)-db-$subscriptionId"
 
 # create MFDBFH config secret
 (Get-Content $PSScriptRoot\..\..\secrets\MFDBFH.cfg) -creplace "mfdbfh","microfocusadmin@$dbName" -join "`n" | Set-Content -NoNewline -Force $PSScriptRoot\..\bankdemo_deploy\cloudp-MFDBFH.cfg
 
 # create MFDBFH credentials secret
 ((Get-Content -path $PSScriptRoot\..\secrets.yaml -Raw) -creplace "mfdbfh","microfocusadmin@$dbname") | Set-Content -Path $PSScriptRoot\..\bankdemo_deploy\cloudp-secrets.yaml
 
 #Apply the settings required to point to external db service name
 ((Get-Content -path $PSScriptRoot\..\cloud-pgsql-service.yaml -Raw) -creplace "cloudpgsqlendpointname","$dbName.postgres.database.azure.com") | Set-Content -Path $PSScriptRoot\..\bankdemo_deploy\cloudp-pgsql-service-deploy.yaml
 
}
