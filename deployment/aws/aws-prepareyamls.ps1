# project name to be passed in as a parameter to avoid duplication of resources and subsequent failures.
$PROJECTNAME=($args[0]).ToLower()
$USERARG=$args[1]
$GITBRANCH=(git rev-parse --abbrev-ref HEAD)
& {
 Get-Date
 $USERNUM=([int]$USERARG).ToString("00")
 if (!($GITBRANCH -like "*$USERNUM")) {
    Write-Host "not in HOL setup"
    $HOLSETUP=0
 } else {
    $HOLSETUP=1
 }

 if ("$HOLSETUP" -eq "0") {
    $Prefix = ""
 } else {
    $Prefix = "$PROJECTNAME-"
 }

 $RegistryUrl = $(aws ecr describe-repositories --repository-names "$($Prefix)escwa" | ConvertFrom-Json).repositories[0].repositoryUri -replace "/$($Prefix)escwa", ""

 Remove-Item -Path $PSScriptRoot\..\bankdemo_deploy -Recurse -ErrorAction Ignore
 mkdir $PSScriptRoot\..\bankdemo_deploy

 ((Get-Content -path $PSScriptRoot\..\..\escwa\escwa.yaml -Raw) -creplace "<registry_url>/","$RegistryUrl/$Prefix") | Set-Content -Path $PSScriptRoot\..\bankdemo_deploy\cloudp-escwa.yaml
 ((Get-Content -path $PSScriptRoot\..\openldap.yaml -Raw) -creplace "<registry_url>/","$RegistryUrl/$Prefix") | Set-Content -Path $PSScriptRoot\..\bankdemo_deploy\cloudp-openldap.yaml
 ((Get-Content -path $PSScriptRoot\..\initdb.yaml -Raw) -creplace "<registry_url>/","$RegistryUrl/$Prefix") | Set-Content -Path $PSScriptRoot\..\bankdemo_deploy\cloudp-initdb.yaml
 ((Get-Content -path $PSScriptRoot\..\copymodule.yaml -Raw) -creplace "<registry_url>/","$RegistryUrl/$Prefix") | Set-Content -Path $PSScriptRoot\..\bankdemo_deploy\cloudp-copymodule.yaml
 ((Get-Content -path $PSScriptRoot\..\backend_bankdemo.yaml -Raw) -creplace "<registry_url>/","$RegistryUrl/$Prefix") | Set-Content -Path $PSScriptRoot\..\bankdemo_deploy\cloudp-backend_bankdemo.yaml
 ((Get-Content -path $PSScriptRoot\..\frontend_bankdemo.yaml -Raw) -creplace "<registry_url>/","$RegistryUrl/$Prefix") | Set-Content -Path $PSScriptRoot\..\bankdemo_deploy\cloudp-frontend_bankdemo.yaml
 
 $RDS_DB_INSTANCE=$("$($PROJECTNAME)-instance-$USERNUM")
 $ELASTICACHE_CLUSTER_ID=$("$($PROJECTNAME)-redis-clust-$USERNUM")
 
 Write-Output "Waiting for Redis Endpoint"
 do { Write-Output "Waiting for redis cluster endpoint"; Start-Sleep -seconds 3; $redisEndpoint=aws elasticache describe-cache-clusters --cache-cluster-id ${ELASTICACHE_CLUSTER_ID} --show-cache-node-info | ConvertFrom-Json } until (-not ([string]::IsNullOrEmpty($($redisEndpoint.CacheClusters.CacheNodes.Endpoint.Address))) )
 Write-Output "Redis Endpoint name is " $($redisEndpoint.CacheClusters.CacheNodes.Endpoint.Address)
 
 ((Get-Content -path $PSScriptRoot\..\cloud-redis-service.yaml -Raw) -creplace "cloudredisendpointname","$($redisEndpoint.CacheClusters.CacheNodes.Endpoint.Address)") | Set-Content -Path $PSScriptRoot\..\bankdemo_deploy\cloudp-redis-service-deploy.yaml
 
 #cd to azure secrets and apply secret config
 (Get-Content $PSScriptRoot\..\..\secrets\MFDBFH.cfg) -creplace "mfdbfh","microfocusadmin" -join "`n" | Set-Content -NoNewline -Force $PSScriptRoot\..\bankdemo_deploy\cloudp-MFDBFH.cfg
 
 #Apply the settings required to point to external db service name
 do { Write-Output "Waiting for DB Endpoint"; Start-Sleep -seconds 3; $dbEndpoint=aws rds describe-db-instances --db-instance-identifier ${RDS_DB_INSTANCE} | ConvertFrom-Json } until (-not ([string]::IsNullOrEmpty($($dbEndpoint.DBInstances.Endpoint.Address))) )
 Write-Output "DB Endpoint name is " "$($dbEndpoint.DBInstances.Endpoint.Address)"
 
 ((Get-Content -path $PSScriptRoot\..\cloud-pgsql-service.yaml -Raw) -creplace "cloudpgsqlendpointname","$($dbEndpoint.DBInstances.Endpoint.Address)") | Set-Content -Path $PSScriptRoot\..\bankdemo_deploy\cloudp-pgsql-service-deploy.yaml
 
 ((Get-Content -path $PSScriptRoot\..\secrets.yaml -Raw) -creplace "mfdbfh","microfocusadmin") | Set-Content -Path $PSScriptRoot\..\bankdemo_deploy\cloudp-secrets.yaml

}
