set -x #echo on

#aws ecr delete-repository --force --repository-name bankdemo
#aws ecr delete-repository --force --repository-name openldap
#aws ecr delete-repository --force --repository-name escwa

export PROJECTNAME=$1

if [ "$PROJECTNAME" == "" ]; then
  echo "no project name passed as parameter"
  set +x #echo on
  exit 1
fi

export EC2_REGION_NAME=`aws ec2 describe-availability-zones | jq '.AvailabilityZones[0].RegionName'`
export EC2_REGION_NAME=`sed -e 's/^"//' -e 's/"$//' <<<"$EC2_REGION_NAME"`
export EC2_ZONE_NAME=`aws ec2 describe-availability-zones | jq '.AvailabilityZones[0].ZoneName'`
export EC2_ZONE_NAME=`sed -e 's/^"//' -e 's/"$//' <<<"$EC2_ZONE_NAME"`
export EC2_ZONE_NAME_1=`aws ec2 describe-availability-zones | jq '.AvailabilityZones[1].ZoneName'`
export EC2_ZONE_NAME_1=`sed -e 's/^"//' -e 's/"$//' <<<"$EC2_ZONE_NAME_1"`
export EC2_ZONE_NAME_2=`aws ec2 describe-availability-zones | jq '.AvailabilityZones[2].ZoneName'`
export EC2_ZONE_NAME_2=`sed -e 's/^"//' -e 's/"$//' <<<"$EC2_ZONE_NAME_2"`
export EC2_ZONE_NAME_3=`aws ec2 describe-availability-zones | jq '.AvailabilityZones[3].ZoneName'`
export EC2_ZONE_NAME_3=`sed -e 's/^"//' -e 's/"$//' <<<"$EC2_ZONE_NAME_3"`
export EC2_ZONE_NAME_4=`aws ec2 describe-availability-zones | jq '.AvailabilityZones[4].ZoneName'`
export EC2_ZONE_NAME_4=`sed -e 's/^"//' -e 's/"$//' <<<"$EC2_ZONE_NAME_4"`
export EC2_ZONE_NAME_5=`aws ec2 describe-availability-zones | jq '.AvailabilityZones[5].ZoneName'`
export EC2_ZONE_NAME_5=`sed -e 's/^"//' -e 's/"$//' <<<"$EC2_ZONE_NAME_5"`

#########################################################

export EKS_CLUSTER_NAME=$PROJECTNAME
export EKS_CLUSTER_NAME=`sed -e 's/^"//' -e 's/"$//' <<<"$EKS_CLUSTER_NAME"`

export RDS_DB_SUBNET_GRP_NAME=$PROJECTNAME-dbsubnetgroup
export RDS_DB_CLUSTER_NAME=$PROJECTNAME-db
# DB name should have ONLY alphanumeric characters
export RDS_DB_NAME=pgsql$PROJECTNAME
export RDS_DB_INSTANCE=$PROJECTNAME-instance

export ELASTICACHE_SNG_NAME=$PROJECTNAME-redis-sg
export ELASTICACHE_CLUSTER_ID=$PROJECTNAME-redis-clust
export VPC_NAME=eksctl-$PROJECTNAME-cluster/VPC

eksctl get cluster --name ${EKS_CLUSTER_NAME} --output json
export ERROR_CODE=$?

if [ "$ERROR_CODE" != "0" ]; then
  echo "cluster doesnt exist."
fi

aws rds describe-db-subnet-groups --db-subnet-group-name ${RDS_DB_SUBNET_GRP_NAME}
export ERROR_CODE=$?

if [ "$ERROR_CODE" != "0" ]; then
  echo "db subnet group doesnt exist"
fi

aws rds describe-db-clusters --db-cluster-identifier ${RDS_DB_CLUSTER_NAME}
export ERROR_CODE=$?

if [ "$ERROR_CODE" != "0" ]; then
  echo "db cluster doesnt exist."
fi

aws elasticache describe-cache-subnet-groups --cache-subnet-group-name ${ELASTICACHE_SNG_NAME}
export ERROR_CODE=$?

if [ "$ERROR_CODE" != "0" ]
then
  echo "redis subnet group doesnt exist."
fi

for cluster in $(aws rds describe-db-clusters | jq -r --arg reg_exp $RDS_DB_CLUSTER_NAME '.DBClusters[] | select(.DBClusterIdentifier | test($reg_exp)) | @base64' ); do
  INSTANCE_NAME=`echo $cluster | base64 --decode | jq -r '.DBClusterMembers[0].DBInstanceIdentifier'`
  aws rds delete-db-instance --db-instance-identifier $INSTANCE_NAME --skip-final-snapshot
done

RDS_DB_INST_DEL="N"
while [ "$RDS_DB_INST_DEL" != "Y" ]
do
  sleep 30
  RDS_DB_INST_DEL="Y"
  for len in $(aws rds describe-db-clusters | jq -r --arg reg_exp $RDS_DB_CLUSTER_NAME '.DBClusters[] | select(.DBClusterIdentifier | test($reg_exp)) | .DBClusterMembers | length' ); do
    if [ "$len" != "0" ]; then
      RDS_DB_INST_DEL="N"
    fi
  done
done

for cluster in $(aws rds describe-db-clusters | jq -r --arg reg_exp $RDS_DB_CLUSTER_NAME '.DBClusters[] | select(.DBClusterIdentifier | test($reg_exp)) | .DBClusterIdentifier' ); do
  aws rds delete-db-cluster --db-cluster-identifier $cluster --skip-final-snapshot
done

export RDS_DB_CLUST_COUNT=$(aws rds describe-db-clusters | jq -r --arg reg_exp $RDS_DB_CLUSTER_NAME '[ .DBClusters[] | select(.DBClusterIdentifier | test($reg_exp)) ] | length' )
while [ "$RDS_DB_CLUST_COUNT" != "0" ]
do
  sleep 30
  export RDS_DB_CLUST_COUNT=$(aws rds describe-db-clusters | jq -r --arg reg_exp $RDS_DB_CLUSTER_NAME '[ .DBClusters[] | select(.DBClusterIdentifier | test($reg_exp)) ] | length' )
done

aws rds delete-db-subnet-group --db-subnet-group-name ${RDS_DB_SUBNET_GRP_NAME}

for k in $(aws elasticache describe-cache-clusters | jq -r --arg reg_exp $ELASTICACHE_CLUSTER_ID '.CacheClusters[] | select(.CacheClusterId | test($reg_exp)) | .CacheClusterId' ); do
  aws elasticache delete-cache-cluster --cache-cluster-id $k
done

export ELASTICACHE_CLUST_COUNT=$(aws elasticache describe-cache-clusters | jq --arg reg_exp $ELASTICACHE_CLUSTER_ID '[ .CacheClusters[] | select(.CacheClusterId | test($reg_exp)) ] | length' )
while [ "$ELASTICACHE_CLUST_COUNT" != "0" ]
do
  sleep 30
  export ELASTICACHE_CLUST_COUNT=$(aws elasticache describe-cache-clusters | jq --arg reg_exp $ELASTICACHE_CLUSTER_ID '[ .CacheClusters[] | select(.CacheClusterId | test($reg_exp)) ] | length' )
done

aws elasticache delete-cache-subnet-group --cache-subnet-group-name ${ELASTICACHE_SNG_NAME}

date

eksctl delete nodegroup --cluster ${EKS_CLUSTER_NAME} --name ${EKS_CLUSTER_NAME}-nodegroup -w --timeout 60m0s

export vpcID=$(aws ec2 describe-vpcs --filters "Name=tag:Name,Values=$VPC_NAME" | jq -r ".Vpcs[0] | .VpcId")
for lb in $(aws elb describe-load-balancers | jq -r --arg reg_exp $vpcID '.LoadBalancerDescriptions[] | select(.VPCId | test($reg_exp)) | .LoadBalancerName'); do
  echo "Deleting load balancer $lb"
  aws elb delete-load-balancer --load-balancer-name $lb
done

for interface in $(aws ec2 describe-network-interfaces --filters "Name=status,Values=available" | jq -r ".NetworkInterfaces[] | .NetworkInterfaceId"); do
  echo "Deleting detached interface $interface"
  aws ec2 delete-network-interface --network-interface-id $interface
done

eksctl delete cluster --name ${EKS_CLUSTER_NAME} -w --timeout 60m0s

for volume in $(aws ec2 describe-volumes --filters 'Name="status",Values="available"' | jq -r ".Volumes[] | .VolumeId"); do
  echo "Deleting detached DBC volume $volume"
  aws ec2 delete-volume --volume-id $volume
done

date

set +x #echo on
