# project name to be passed in as a parameter to avoid duplication of resources and subsequent failures.
$PROJECTNAME=($args[0]).ToLower()
$USERCOUNTARG=$args[1]
& {
Get-Date

#$env:path += 'C:\Users\user\.azure-kubectl'

# Install AWS CLI msi from https://s3.amazonaws.com/aws-cli/AWSCLI64PY3.msi

# Install EKSCTL for Windows - requires chocolatey

# Run aws configure with private key info from your account

if ("$PROJECTNAME" -eq "") {
  Write-Output "No project name passed as parameter"
  exit 1
}

$USERCOUNT = [int]$USERCOUNTARG
if ("$USERCOUNT" -eq 0) {
  $USERCOUNT = 1
}

$NODECOUNT=[int]($USERCOUNT / 2 + 1)
Write-Output "Node Count is: $($NODECOUNT)"

$EC2_AVAILABILITY_ZONES=aws ec2 describe-availability-zones | ConvertFrom-Json

$EC2_REGION_NAME=$EC2_AVAILABILITY_ZONES.AvailabilityZones[0].RegionName
$EC2_ZONE_NAME=$EC2_AVAILABILITY_ZONES.AvailabilityZones[0].ZoneName
$EC2_ZONE_NAME_1=$EC2_AVAILABILITY_ZONES.AvailabilityZones[1].ZoneName
$EC2_ZONE_NAME_2=$EC2_AVAILABILITY_ZONES.AvailabilityZones[2].ZoneName
$EC2_ZONE_NAME_3=$EC2_AVAILABILITY_ZONES.AvailabilityZones[3].ZoneName
$EC2_ZONE_NAME_4=$EC2_AVAILABILITY_ZONES.AvailabilityZones[4].ZoneName
$EC2_ZONE_NAME_5=$EC2_AVAILABILITY_ZONES.AvailabilityZones[5].ZoneName

Write-Output "$($EC2_REGION_NAME) $($EC2_ZONE_NAME) $($EC2_ZONE_NAME_1) $($EC2_ZONE_NAME_2) $($EC2_ZONE_NAME_3) $($EC2_ZONE_NAME_4) $($EC2_ZONE_NAME_5) "

$EKS_CLUSTER_NAME="$($PROJECTNAME)"
Write-Output "Cluster Name is: $($EKS_CLUSTER_NAME)"

$RDS_DB_SUBNET_GRP_NAME="$($PROJECTNAME)-dbsubnetgroup"
Write-Output "Subnet Group Name is: $($RDS_DB_SUBNET_GRP_NAME)"

$RDS_DB_CLUSTER_NAME="$PROJECTNAME-db"
Write-Output "DB Cluster Name is: $($RDS_DB_CLUSTER_NAME)"

# DB name should have ONLY alphanumeric characters
$RDS_DB_NAME="pgsql$($PROJECTNAME)"
$RDS_DB_INSTANCE=$("$($PROJECTNAME)-instance")

Write-Output "DB Info: $($RDS_DB_NAME) $($RDS_DB_INSTANCE)"

$ELASTICACHE_SNG_NAME="$PROJECTNAME-redis-sg"
$ELASTICACHE_CLUSTER_ID=$("$($PROJECTNAME)-redis-clust")

Write-Output "Elasticache info: $($ELASTICACHE_SNG_NAME) $($ELASTICACHE_CLUSTER_ID)"

Write-Output "EKS Cluster Name is $EKS_CLUSTER_NAME"
eksctl get cluster --name $($EKS_CLUSTER_NAME) --output json >$null
if ($LASTEXITCODE -eq "0") {
  Write-Output "cluster already exists. Delete existing cluster before runnign this script"
  exit 2
}

aws rds describe-db-subnet-groups --db-subnet-group-name $($RDS_DB_SUBNET_GRP_NAME) >$null
if ($LASTEXITCODE -eq "0") {
  Write-Output "DB subnet group already exists. Delete existing db subnet group before running this script"
  exit 3
}

aws rds describe-db-clusters --db-cluster-identifier ${RDS_DB_CLUSTER_NAME} >$null
if ($LASTEXITCODE -eq "0") {
  Write-Output "db cluster already exists. Delete existing db cluster before running this script"
  exit 4
}

aws rds describe-db-instances --db-instance-identifier ${RDS_DB_INSTANCE} >$null
if ($LASTEXITCODE -eq "0") {
  Write-Output "DB instance already exists. Delete existing db instance before running this script"
  exit 5
}

aws elasticache describe-cache-subnet-groups --cache-subnet-group-name ${ELASTICACHE_SNG_NAME} >$null
if ($LASTEXITCODE -eq "0") {
  Write-Output "redis subnet group already exists. pass new argument or delete existing redis subnet group"
  exit 6
}

aws elasticache describe-cache-clusters --cache-cluster-id ${ELASTICACHE_CLUSTER_ID} >$null
if ($LASTEXITCODE -eq "0") {
  Write-Output "redis cluster already exists. pass new argument or delete existing redis cluster"
  exit 7
}

######## this is to be used only when there isnt an existing VPC where you want to create your cluster ###########
######## Amazon EBS CSI Driver +++
eksctl create cluster --name ${EKS_CLUSTER_NAME} --version 1.28 --region ${EC2_REGION_NAME} --zones ${EC2_ZONE_NAME_1},${EC2_ZONE_NAME_2} --nodegroup-name ${EKS_CLUSTER_NAME}-nodegroup --managed --node-type t2.large --nodes $NODECOUNT
export SERVICE_ACCOUNT_ROLE_ARN=$(aws iam get-role --role-name AmazonEKS_EBS_CSI_DriverRole | jq -r '.Role.Arn')
eksctl utils associate-iam-oidc-provider --region ${EC2_REGION_NAME} --cluster ${EKS_CLUSTER_NAME} --approve
eksctl create addon --name aws-ebs-csi-driver --cluster ${EKS_CLUSTER_NAME} --service-account-role-arn $SERVICE_ACCOUNT_ROLE_ARN --force
######## Amazon EBS CSI Driver ---

if ($LASTEXITCODE -ne "0") {
  Write-Output "Redis cluster creation failed."
  exit 8
}

$EKS_CLUSTER_OBJECT=eksctl get cluster --name $($EKS_CLUSTER_NAME) --output json | ConvertFrom-Json
Write-Output "Cluster Object is $($EKS_CLUSTER_OBJECT)"

$EKS_SECURITY_GRP_ID=$EKS_CLUSTER_OBJECT.ResourcesVpcConfig.ClusterSecurityGroupId
$EKS_SUBNET_1=$($EKS_CLUSTER_OBJECT.ResourcesVpcConfig.SubnetIds[0])
$EKS_SUBNET_2=$($EKS_CLUSTER_OBJECT.ResourcesVpcConfig.SubnetIds[1])
$EKS_SUBNET_3=$($EKS_CLUSTER_OBJECT.ResourcesVpcConfig.SubnetIds[2])
$EKS_SUBNET_4=$($EKS_CLUSTER_OBJECT.ResourcesVpcConfig.SubnetIds[3])

Write-Output "Security and Subnet Info: $($EKS_SECURITY_GRP_ID) $($EKS_SUBNET_1) $($EKS_SUBNET_2) $($EKS_SUBNET_3) $($EKS_SUBNET_4)"

# create DB subnet group

aws rds create-db-subnet-group --db-subnet-group-name  $($RDS_DB_SUBNET_GRP_NAME) --db-subnet-group-description "bank Demo DB Subnet Group" --subnet-ids $($EKS_SUBNET_1) $($EKS_SUBNET_2) $($EKS_SUBNET_3) $($EKS_SUBNET_4) #| jq '.DBSubnetGroup.DBSubnetGroupName'
if ($LASTEXITCODE -ne "0") {
  Write-Output "DB subnet group creation failed."
  exit 9
}

for ($i = 1; $i -le $USERCOUNT; $i++) {
  $ip = $i.ToString("00")

  # create DB cluster
  aws rds create-db-cluster --db-cluster-identifier $("$RDS_DB_CLUSTER_NAME-$ip") --engine aurora-postgresql --engine-version 12.9 --port 5432 --master-username microfocusadmin --master-user-password MfdbfhAdmin1 --database-name $($RDS_DB_NAME) --db-subnet-group-name $($RDS_DB_SUBNET_GRP_NAME) --vpc-security-group-ids $($EKS_SECURITY_GRP_ID) --no-storage-encrypted --no-enable-iam-database-authentication
  if ($LASTEXITCODE -ne "0") {
    Write-Output "DB cluster creation failed."
    exit 10
  }
  # create db instance
  aws rds create-db-instance --db-instance-identifier $("$RDS_DB_INSTANCE-$ip") --db-cluster-identifier $("$RDS_DB_CLUSTER_NAME-$ip") --engine aurora-postgresql --db-instance-class db.t3.medium --db-subnet-group-name $($RDS_DB_SUBNET_GRP_NAME) --no-multi-az
  if ($LASTEXITCODE -ne "0") {
    Write-Output "DB instance $RDS_DB_INSTANCE-$ip creation failed."
    exit 11
  }
}

# allow access to pg db from withing the vpc. open port 5432
aws ec2 authorize-security-group-ingress --group-id $($EKS_SECURITY_GRP_ID) --protocol tcp --port 5432 --cidr 0.0.0.0/0
if ($LASTEXITCODE -ne "0") {
  Write-Output "Adding rule to access db port failed."
  exit 12
}

#########################################################

# create redis subnet group

aws elasticache create-cache-subnet-group --cache-subnet-group-name $($ELASTICACHE_SNG_NAME) --cache-subnet-group-description "$($PROJECTNAME)-redistest" --subnet-ids $($EKS_SUBNET_1) $($EKS_SUBNET_2) $($EKS_SUBNET_3) $($EKS_SUBNET_4)
if ($LASTEXITCODE -ne "0") {
  Write-Output "redis subnet group creation failed."
  exit 13
}

for ($i = 1; $i -le $USERCOUNT; $i++) {
  $ip = $i.ToString("00")

  # create redis cluster
  aws elasticache create-cache-cluster --cache-cluster-id $("$ELASTICACHE_CLUSTER_ID-$ip") --cache-node-type cache.t2.small --engine redis --engine-version 5.0.3 --cache-subnet-group-name $($ELASTICACHE_SNG_NAME) --security-group-ids $($EKS_SECURITY_GRP_ID) --port 6379 --num-cache-nodes 1
  if ($LASTEXITCODE -ne "0") {
    Write-Output "redis cluster $ELASTICACHE_CLUSTER_ID-$ip creation failed."
    exit 14
  }
}

# allow access to redis from withing the vpc. open port 6379
aws ec2 authorize-security-group-ingress --group-id $($EKS_SECURITY_GRP_ID) --protocol tcp --port 6379 --cidr 0.0.0.0/0
if ($LASTEXITCODE -ne "0") {
  Write-Output "adding rule to access redis port failed."
  exit 15
}

#start metrics server
 $response = Invoke-RestMethod -Uri "https://api.github.com/repos/kubernetes-sigs/metrics-server/releases/latest"
 $response | Get-Member
 $DOWNLOAD_URL=$response.zipball_url
 $DOWNLOAD_VERSION=$response.tag_name
 Invoke-RestMethod -Uri "$($DOWNLOAD_URL)" -OutFile metrics-server-$DOWNLOAD_VERSION.zip
 Expand-Archive -force metrics-server-$DOWNLOAD_VERSION.zip
 Push-Location $PSScriptRoot\metrics-server-$DOWNLOAD_VERSION\*\
 kubectl apply -f .\deploy\1.8+\
 Pop-Location
 
 Remove-Item $PSScriptRoot\..\aws\metrics*zip
 Get-ChildItem -Path $PSScriptRoot\..\aws\metrics* -Recurse | Remove-Item -force -recurse
 Remove-Item $PSScriptRoot\..\aws\metrics* -force
 
 kubectl wait --for=condition=ready --timeout=300s --namespace kube-system --all=true pod
 if ($LASTEXITCODE -ne "0") {
   Write-Host "Metrics server failed to start. Exit code $LASTEXITCODE"
   exit 1
 }
 kubectl get deployment metrics-server -n kube-system

Get-Date
}