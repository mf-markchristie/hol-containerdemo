
#Install kind if not available
try {
    kind.exe --version
} catch {
    #kind isn't available, so downloading kind
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
    Invoke-WebRequest -Uri https://kind.sigs.k8s.io/dl/v0.8.1/kind-windows-amd64 -OutFile $env:TEMP\kind.exe
    $env:path += ";$($env:TEMP)"
}

# Use kind to create a cluster
kind create cluster --config=.\kind-cluster.yaml

# Setup ingress within the cluster (for tn3270)
kubectl apply -f .\tcpservices.yaml
kubectl apply -f .\ingress.yaml
# Configure ingress to route web traffic to bankdemo loan calculator
#kubectl apply -f .\webingress.yaml


Get-Date
