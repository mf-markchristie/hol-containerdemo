package main

import (
    "io/ioutil"
    "log"
    "net/http"
    "os"
    "strconv"
    "strings"
    "time"
    "github.com/prometheus/client_golang/prometheus"
    "github.com/prometheus/client_golang/prometheus/promhttp"
)

func recordMetrics() {
    // Start background thread which reads ESmonitor1.csv and 
	// updates the gauges, then sleeps 30 seconds and repeats
    go func() {
        arg1 := os.Args[1]
        time.Sleep(60 * time.Second)
        for {
            data, err1 := ioutil.ReadFile(arg1)
            if err1 != nil {
                log.Printf("File reading error: %v", err1)
                time.Sleep(60 * time.Second)
            }
            log.Printf("Contents of file: %s", string(data))

            s := strings.Split(string(data), ",")
            i1, err := strconv.ParseFloat(s[1], 64)
            log.Printf("Tasks per minute : %f", i1)
            i2, err := strconv.ParseFloat(s[2], 64)
            log.Printf("Average Latency : %f", i2)
            i3, err := strconv.ParseFloat(s[3], 64)
            log.Printf("Average task length : %f", i3)
            i4, err := strconv.ParseFloat(s[4], 64)
            log.Printf("Queued tasks : %f", i4)
            if err != nil {
                log.Printf("convert to float error: %v", err)
            }
            tPM.Set(i1)
            avgLatency.Set(i2)
            avgTaskDuration.Set(i3)
            workQueued.Set(i4)
            time.Sleep(30 * time.Second)
        }
    }()
}

// Create the gauges
var (
    tPM = prometheus.NewGauge(prometheus.GaugeOpts{
        Name: "es_tasks_per_minute",
        Help: "number of tasks per minute",
    })
    avgLatency = prometheus.NewGauge(prometheus.GaugeOpts{
        Name: "es_average_task_latency",
        Help: "average latency",
    })
    workQueued = prometheus.NewGauge(prometheus.GaugeOpts{
        Name: "es_queued_transactions",
        Help: "amount of work queued",
    })
    avgTaskDuration = prometheus.NewGauge(prometheus.GaugeOpts{
        Name: "es_average_task_duration",
        Help: "average task duration",
    })
)

func init() {
    // Metrics have to be registered to be exposed:
    prometheus.MustRegister(tPM)
    prometheus.MustRegister(avgLatency)
    prometheus.MustRegister(avgTaskDuration)
    prometheus.MustRegister(workQueued)
}

func main() {

    recordMetrics()

    // The Handler function provides a default handler to expose metrics
    // via an HTTP server. "/metrics" is the usual endpoint for that.
    http.Handle("/metrics", promhttp.Handler())

    port := os.Getenv("LISTENING_PORT")

    if port == "" {
		port = "8080"
    }
    log.Printf("listening on port:%s", port)

    err := http.ListenAndServe(":"+port, nil)
    if err != nil {
		log.Fatalf("Failed to start server:%v", err)
    }
}
