#/bin/bash
docker build -t jenkins:custom .
export ERROR_CODE=$?
if [ "$ERROR_CODE" != "0" ]; then
  echo "Failed to build docker image."
  exit 0
fi
sed -i "s/nwb-su15demo/`hostname`/" ./configuration/nodes/Linux/config.xml
chmod -R 777 configuration
docker run --restart always --name myjenkins -d -p 8081:8080 -p50000:50000 -v `pwd`/configuration:/var/jenkins_home jenkins:custom
