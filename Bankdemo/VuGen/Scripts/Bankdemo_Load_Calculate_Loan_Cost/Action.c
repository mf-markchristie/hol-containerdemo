Action()
{

	/* *** The terminal type is 3270 Display. */
	TE_connect(
		"comm-type = tn3270;"
		"host-name = {HostnameParameter};"
		"use-tn3270e-protocol = true;"
		"security-type = unsecured;"
		"r2l-screen = false;"
		"lu-name = ;"
		"terminal-type = 3270 display;"
		"terminal-model = 3278-2-e (24x80);"
		"login-command-file = ;"
		"terminal-setup-file = ;"
		, 60000);
	if (TE_errno != TE_SUCCESS) 
		return -1;
	TE_wait_sync();
	TE_wait_cursor(1, 1, 100, 90);
	lr_think_time(2);
	TE_type("<kClear_screen>");
	TE_wait_sync();
	lr_think_time(4);
	TE_type("bank<kEnter>");
	TE_wait_sync();
	lr_think_time(5);
	TE_type("b0001");
	lr_think_time(5);
	TE_type("abc<kNewline>");
	TE_type("<kEnter>");
	TE_wait_sync();
	lr_think_time(3);
	TE_type("<kDown>");
	TE_type("<kDown>");
	TE_type("<kDown>");
	TE_type("<kDown>");
	TE_type("<kDown>");
	TE_type("<kDown>");
	TE_type("x");
	lr_think_time(2);
	TE_type("<kEnter>");
	TE_wait_sync();
	lr_think_time(3);
	TE_type("1000<kTab>");
	lr_think_time(6);
	TE_type("3<kTab>");
	lr_think_time(4);
	TE_type("60<kEnter>");
	TE_wait_sync();
	lr_think_time(9);
	TE_type("<kTab>");
	lr_think_time(6);
	TE_type("<kRight>.0<kEnter>");
	TE_wait_sync();
	lr_think_time(12);
	TE_type("<kF3>");
	TE_disconnect();
	/* 	End recording */
	return 0;
}
