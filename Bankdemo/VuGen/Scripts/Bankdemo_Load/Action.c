Action()
{

	/* *** The terminal type is 3270 Display. */
	TE_connect(
		"comm-type = tn3270;"
		"host-name = {HostnameParameter};"
		"use-tn3270e-protocol = true;"
		"security-type = unsecured;"
		"r2l-screen = false;"
		"lu-name = ;"
		"terminal-type = 3270 display;"
		"terminal-model = 3278-2-e (24x80);"
		"login-command-file = ;"
		"terminal-setup-file = ;"
		, 60000);
	if (TE_errno != TE_SUCCESS) 
		return -1;
	TE_wait_sync();
	TE_wait_cursor(1, 1, 100, 90);
	lr_think_time(20);
	TE_type("<kClear_screen>");
	TE_wait_sync();
	lr_think_time(20);
	TE_type("bank<kNewline>");
	lr_think_time(20);
	TE_type("<kEnter>");
	TE_wait_sync();
	lr_think_time(20);
	TE_type("b0001");
	lr_think_time(20);
	TE_type("f<kEnter>");
	TE_wait_sync();
	TE_type("x");
	TE_type("<kNewline>");
	lr_think_time(20);
	TE_type("<kEnter>");
	TE_wait_sync();
	TE_type("x");
	TE_type("<kEnter>");
	TE_wait_sync();
	lr_think_time(20);
	TE_type("<kF3>");
	TE_disconnect(500);
	/* 	End recording */
	return 0;
}
