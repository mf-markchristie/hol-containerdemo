      *> Test Fixture for DO-LOAN-CALCULATION1, DO-LOAN-CALCULATION1

       copy "mfunit_prototypes.cpy".

       program-id. TestLoanCalc
       working-storage section.
       copy "mfunit.cpy".
       78 TEST-TESTLOANCALC value "TestLOANCALC".
       01 pp procedure-pointer.

      *> Program linkage data
       01 WS-INP-INPUT-STRUCTURE.
         02 WS-CALC-WORK-AMOUNT PIC 9(7).
         02 WS-CALC-WORK-PERCENTAGE PIC 9(3)V9(3).
         02 WS-CALC-WORK-TERM PIC 9(5).
       01 WS-OUT-OUTPUT-STRUCTURE.
         02 WS-LOAN-MONTHLY PIC S9(6)V99.

       procedure division.
           goback returning 0
       .

       entry MFU-TC-PREFIX & TEST-TESTLOANCALC.
           move 10000 to WS-CALC-WORK-AMOUNT
           move    10 to WS-CALC-WORK-PERCENTAGE 
           move   360 to WS-CALC-WORK-TERM

           call "LOANCALC" using
                       by reference WS-INP-INPUT-STRUCTURE
                       by reference WS-OUT-OUTPUT-STRUCTURE
           
           if WS-LOAN-MONTHLY not = 87.76
               goback returning MFU-FAIL-RETURN-CODE
           end-if
           
           goback returning MFU-PASS-RETURN-CODE
       .

      $region TestCase Configuration

       entry MFU-TC-SETUP-PREFIX & TEST-TESTLOANCALC.
       perform InitializeLinkageData
           *> Add any other test setup code here
           goback returning 0
       .

       InitializeLinkageData section.
           *> Load the library that is being tested
           set pp to entry "DO-LOAN-CALCULATION1"

           initialize WS-INP-INPUT-STRUCTURE
           initialize WS-OUT-OUTPUT-STRUCTURE
           exit section
       .

      $end-region

       end program.
