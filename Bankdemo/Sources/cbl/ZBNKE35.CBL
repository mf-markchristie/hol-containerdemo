000100***************************************************************** zbnke35 
000200*                                                               * zbnke35 
000300*   Copyright (C) 1998-2010 Micro Focus. All Rights Reserved.   * zbnke35 
000400*   This demonstration program is provided for use by users     * zbnke35 
000500*   of Micro Focus products and may be used, modified and       * zbnke35 
000600*   distributed as part of your application provided that       * zbnke35 
000700*   you properly acknowledge the copyright of Micro Focus       * zbnke35 
000800*   in this material.                                           * zbnke35 
000900*                                                               * zbnke35 
001000***************************************************************** zbnke35 
001100                                                                  zbnke35 
001200***************************************************************** zbnke35 
001300* Program:     ZBNKE35.CBL                                      * zbnke35 
001400* Function:    Demonstrate E35 capability.                      * zbnke35 
001500***************************************************************** zbnke35 
001600*MFJSORT provides a standard linkage area to the E15 and E35    * zbnke35 
001700*exit program. It is compatible with the mainframe utility      * zbnke35 
001800*and should be defined as follows:                              * zbnke35 
001900*                                                               * zbnke35 
002000*COBOL statement      PIC        Value                          * zbnke35 
002100* RECORD-FLAGS        9(8)COMP   0 - first record passed        * zbnke35 
002200*                                4 - subsequent records passed  * zbnke35 
002300*                                8 - last record passed         * zbnke35 
002400* ENTRY-BUFFER        X(n)       Contents of the input record.  * zbnke35 
002500*                                Do not change this area.       * zbnke35 
002600* EXIT-BUFFER         X(n)       Contents of the new or altered * zbnke35 
002700*                                  record provided by the exit. * zbnke35 
002800* UNUSED-ENTRY        9(8)COMP   Not used                       * zbnke35 
002900*                                                               * zbnke35 
003000* UNUSED-ENTRY        9(8)COMP   Not used                       * zbnke35 
003100*                                                               * zbnke35 
003200* ENTRY-RECORD-LENGTH 9(8)COMP   Length of the input record.    * zbnke35 
003300* EXIT-RECORD-LENGTH  9(8)COMP   Length of the new or altered   * zbnke35 
003400*                                  record provided by the exit. * zbnke35 
003500* UNUSED-ENTRY        9(8)COMP   Not used                       * zbnke35 
003600* EXIT-AREA-LENGTH    9(4)COMP   Length of the exit area        * zbnke35 
003700*                                  scratchpad.                  * zbnke35 
003800*                                Do not change this field.      * zbnke35 
003900* EXIT-AREA           X(n)       Exit area scratchpad used by   * zbnke35 
004000*                                  the exit to maintain         * zbnke35 
004100*                                  variables between calls to   * zbnke35 
004200*                                  the exit program.            * zbnke35 
004300*                                                               * zbnke35 
004400* Return Code         Meaning                                   * zbnke35 
004500*   0                 No action required                        * zbnke35 
004600*   4                 Delete the current record.                * zbnke35 
004700*                       For E15, the record is not sorted.      * zbnke35 
004800*                       For E35, the record is not written to   * zbnke35 
004900*                         the output dataset                    * zbnke35 
005000*   8                 Do not call this exit again;              * zbnke35 
005100*                       exit processing is no longer required   * zbnke35 
005200*   12                Insert the current record.                * zbnke35 
005300*                       For E15, the record is inserted for     * zbnke35 
005400*                         sorting.                              * zbnke35 
005500*                       For E35, the record is written to the   * zbnke35 
005600*                         output dataset                        * zbnke35 
005700*   16                Terminate. The job step is terminated     * zbnke35 
005800*                       with the condition code set to 16       * zbnke35 
005900*   20                Alter the current record.                 * zbnke35 
006000*                       For E15, the altered record is passed   * zbnke35 
006100*                         to the sort.                          * zbnke35 
006200*                       For E35, the altered record is written  * zbnke35 
006300*                         to the output dataset                 * zbnke35 
006400***************************************************************** zbnke35 
006500                                                                  zbnke35 
006600 IDENTIFICATION DIVISION.                                         zbnke35 
006700 PROGRAM-ID.                                                      zbnke35 
006800     ZBNKE35.                                                     zbnke35 
006900 DATE-WRITTEN.                                                    zbnke35 
007000     September 2002.                                              zbnke35 
007100 DATE-COMPILED.                                                   zbnke35 
007200     Today.                                                       zbnke35 
007300 ENVIRONMENT DIVISION.                                            zbnke35 
007400                                                                  zbnke35 
007500 DATA DIVISION.                                                   zbnke35 
007600                                                                  zbnke35 
007700 WORKING-STORAGE SECTION.                                         zbnke35 
007800 01  WS-MISC-STORAGE.                                             zbnke35 
007900   05  WS-PROGRAM-ID                         PIC X(8)             zbnke35 
008000       VALUE 'ZBNKE35 '.                                          zbnke35 
008100   05  WS-REC-COUNT-READ                     PIC 9(5).            zbnke35 
008200   05  WS-REC-COUNT-WRITTEN                  PIC 9(5).            zbnke35 
008300   05  WS-REC-COUNT-DELETED                  PIC 9(5).            zbnke35 
008400   05  WS-SAVED-TYPE-AND-PID.                                     zbnke35 
008500     10  WS-SAVED-TYPE                       PIC X(1).            zbnke35 
008600     10  WS-SAVED-PID                        PIC X(5).            zbnke35 
008700                                                                  zbnke35 
008800 01  WS-CONSOLE-MESSAGE                      PIC X(48).           zbnke35 
008900                                                                  zbnke35 
009000 COPY CABENDD.                                                    zbnke35 
009100                                                                  zbnke35 
009200 LINKAGE SECTION.                                                 zbnke35 
009300 01  LK-EXIT-RECORD-FLAGS                    PIC 9(8) COMP.       zbnke35 
009400   88  FIRST-RECORD                          VALUE 0.             zbnke35 
009500   88  SUBSEQUENT-RECORD                     VALUE 4.             zbnke35 
009600   88  LAST-RECORD                           VALUE 8.             zbnke35 
009700                                                                  zbnke35 
009800 01  LK-EXIT-ENTRY-BUFFER                    PIC X(116).          zbnke35 
009900                                                                  zbnke35 
010000 01  LK-EXIT-EXIT-BUFFER                     PIC X(116).          zbnke35 
010100                                                                  zbnke35 
010200 01  LK-EXIT-UNUSED-1                        PIC 9(8) COMP.       zbnke35 
010300                                                                  zbnke35 
010400 01  LK-EXIT-UNUSED-2                        PIC 9(8) COMP.       zbnke35 
010500                                                                  zbnke35 
010600 01  LK-EXIT-ENTRY-REC-LEN                   PIC 9(8) COMP.       zbnke35 
010700                                                                  zbnke35 
010800 01  LK-EXIT-EXIT-REC-LEN                    PIC 9(8) COMP.       zbnke35 
010900                                                                  zbnke35 
011000 01  LK-EXIT-SPA-LEN                         PIC 9(4) COMP.       zbnke35 
011100                                                                  zbnke35 
011200 01  LK-EXIT-SPA.                                                 zbnke35 
011300   05  LK-EXIT-SPA-BYTE                      PIC X(1)             zbnke35 
011400       OCCURS 1 TO 9999 TIMES DEPENDING ON LK-EXIT-SPA-LEN.       zbnke35 
011500                                                                  zbnke35 
011600 PROCEDURE DIVISION USING LK-EXIT-RECORD-FLAGS                    zbnke35 
011700                          LK-EXIT-ENTRY-BUFFER                    zbnke35 
011800                          LK-EXIT-EXIT-BUFFER                     zbnke35 
011900                          LK-EXIT-UNUSED-1                        zbnke35 
012000                          LK-EXIT-UNUSED-2                        zbnke35 
012100                          LK-EXIT-ENTRY-REC-LEN                   zbnke35 
012200                          LK-EXIT-EXIT-REC-LEN                    zbnke35 
012300                          LK-EXIT-SPA-LEN                         zbnke35 
012400                          LK-EXIT-SPA.                            zbnke35 
012500                                                                  zbnke35 
012600                                                                  zbnke35 
012700     IF FIRST-RECORD                                              zbnke35 
012800        MOVE 'E35 exit invoked' TO WS-CONSOLE-MESSAGE             zbnke35 
012900        PERFORM DISPLAY-CONSOLE-MESSAGE                           zbnke35 
013000        MOVE 0 TO WS-REC-COUNT-READ                               zbnke35 
013100        MOVE 0 TO WS-REC-COUNT-WRITTEN                            zbnke35 
013200        MOVE 0 TO WS-REC-COUNT-DELETED                            zbnke35 
013300        MOVE LOW-VALUES TO WS-SAVED-PID                           zbnke35 
013400     END-IF.                                                      zbnke35 
013500                                                                  zbnke35 
013600     IF FIRST-RECORD OR                                           zbnke35 
013700        SUBSEQUENT-RECORD                                         zbnke35 
013800        ADD 1 TO WS-REC-COUNT-READ                                zbnke35 
013900        IF LK-EXIT-ENTRY-BUFFER (5:1) IS NOT EQUAL TO '0' AND     zbnke35 
014000           LK-EXIT-ENTRY-BUFFER (5:1) IS NOT EQUAL TO '1'         zbnke35 
014100           ADD 1 TO WS-REC-COUNT-WRITTEN                          zbnke35 
014200           MOVE 0 TO RETURN-CODE                                  zbnke35 
014300        ELSE                                                      zbnke35 
014400           IF LK-EXIT-ENTRY-BUFFER (5:6) IS EQUAL TO              zbnke35 
014500                WS-SAVED-TYPE-AND-PID                             zbnke35 
014600              ADD 1 TO WS-REC-COUNT-DELETED                       zbnke35 
014700              MOVE 4 TO RETURN-CODE                               zbnke35 
014800           ELSE                                                   zbnke35 
014900              MOVE LK-EXIT-ENTRY-BUFFER (5:6)                     zbnke35 
015000                TO WS-SAVED-TYPE-AND-PID                          zbnke35 
015100              ADD 1 TO WS-REC-COUNT-WRITTEN                       zbnke35 
015200              MOVE 0 TO RETURN-CODE                               zbnke35 
015300           END-IF                                                 zbnke35 
015400        END-IF                                                    zbnke35 
015500     ELSE                                                         zbnke35 
015600        MOVE 'E35 exit terminating at EOF' TO WS-CONSOLE-MESSAGE  zbnke35 
015700        PERFORM DISPLAY-CONSOLE-MESSAGE                           zbnke35 
015800        STRING 'E35 exit: records read......: 'DELIMITED BY SIZE  zbnke35 
015900               WS-REC-COUNT-READ DELIMITED BY SIZE                zbnke35 
016000          INTO WS-CONSOLE-MESSAGE                                 zbnke35 
016100        PERFORM DISPLAY-CONSOLE-MESSAGE                           zbnke35 
016200        STRING 'E35 exit: records deleted...: ' DELIMITED BY SIZE zbnke35 
016300               WS-REC-COUNT-DELETED DELIMITED BY SIZE             zbnke35 
016400          INTO WS-CONSOLE-MESSAGE                                 zbnke35 
016500        PERFORM DISPLAY-CONSOLE-MESSAGE                           zbnke35 
016600        STRING 'E35 exit: records written...: ' DELIMITED BY SIZE zbnke35 
016700               WS-REC-COUNT-WRITTEN DELIMITED BY SIZE             zbnke35 
016800          INTO WS-CONSOLE-MESSAGE                                 zbnke35 
016900        PERFORM DISPLAY-CONSOLE-MESSAGE                           zbnke35 
017000        MOVE 8 TO RETURN-CODE                                     zbnke35 
017100     END-IF.                                                      zbnke35 
017200                                                                  zbnke35 
017300     GOBACK.                                                      zbnke35 
017400                                                                  zbnke35 
017500                                                                  zbnke35 
017600***************************************************************** zbnke35 
017700* Display CONSOLE messages...                                   * zbnke35 
017800***************************************************************** zbnke35 
017900 DISPLAY-CONSOLE-MESSAGE.                                         zbnke35 
018000     DISPLAY WS-PROGRAM-ID ' - ' WS-CONSOLE-MESSAGE               zbnke35 
018100       UPON CONSOLE.                                              zbnke35 
018200     MOVE ALL SPACES TO WS-CONSOLE-MESSAGE.                       zbnke35 
018300                                                                  zbnke35 
018400* $ Version 5.99c sequenced on Wednesday 3 Mar 2011 at 1:00pm     zbnke35 
