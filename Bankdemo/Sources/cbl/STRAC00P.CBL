000100***************************************************************** strac00p
000200*                                                               * strac00p
000300*   Copyright (C) 1998-2010 Micro Focus. All Rights Reserved.   * strac00p
000400*   This demonstration program is provided for use by users     * strac00p
000500*   of Micro Focus products and may be used, modified and       * strac00p
000600*   distributed as part of your application provided that       * strac00p
000700*   you properly acknowledge the copyright of Micro Focus       * strac00p
000800*   in this material.                                           * strac00p
000900*                                                               * strac00p
001000***************************************************************** strac00p
001100                                                                  strac00p
001200***************************************************************** strac00p
001300* Program:     STRAC00P.CBL (CICS Version)                      * strac00p
001400* Layer:       Screen handling                                  * strac00p
001500* Function:    Display activity on system log                   * strac00p
001600***************************************************************** strac00p
001700                                                                  strac00p
001800 IDENTIFICATION DIVISION.                                         strac00p
001900 PROGRAM-ID.                                                      strac00p
002000     STRAC00P.                                                    strac00p
002100 DATE-WRITTEN.                                                    strac00p
002200     September 2007.                                              strac00p
002300 DATE-COMPILED.                                                   strac00p
002400     Today.                                                       strac00p
002500                                                                  strac00p
002600 ENVIRONMENT DIVISION.                                            strac00p
002700                                                                  strac00p
002800 DATA DIVISION.                                                   strac00p
002900 WORKING-STORAGE SECTION.                                         strac00p
003000 01  WS-MISC-STORAGE.                                             strac00p
003100   05  WS-PROGRAM-ID                         PIC X(8)             strac00p
003200       VALUE 'STRAC00P'.                                          strac00p
003300   05  WS-TRAN-ID                            PIC X(4).            strac00p
003400   05  WS-TRACE-LEVEL                        PIC X(1)             strac00p
003500       VALUE '1'.                                                 strac00p
003600     88  TRACE-LEVEL-0                       VALUE '0'.           strac00p
003700     88  TRACE-LEVEL-1                       VALUE '1'.           strac00p
003800     88  TRACE-LEVEL-2                       VALUE '2'.           strac00p
003900   05  WS-WTO-DATA.                                               strac00p
004000     10  FILLER                              PIC X(4)             strac00p
004100         VALUE 'INT '.                                            strac00p
004200     10  FILLER                              PIC X(7)             strac00p
004300         VALUE 'Termid:'.                                         strac00p
004400     10  WS-WTO-TERM                         PIC X(4).            strac00p
004500     10  FILLER                              PIC X(9)             strac00p
004600         VALUE ', Tranid:'.                                       strac00p
004700     10  WS-WTO-TRAN                         PIC X(4).            strac00p
004800     10  FILLER                              PIC X(10)            strac00p
004900         VALUE ', Program:'.                                      strac00p
005000     10  WS-WTO-PROG                         PIC X(8).            strac00p
005100                                                                  strac00p
005200 COPY DFHAID.                                                     strac00p
005300                                                                  strac00p
005400 COPY DFHBMSCA.                                                   strac00p
005500                                                                  strac00p
005600 COPY CABENDD.                                                    strac00p
005700                                                                  strac00p
005800 LINKAGE SECTION.                                                 strac00p
005900 01  DFHCOMMAREA.                                                 strac00p
006000   05  LK-CALLING-RTN                        PIC X(8).            strac00p
006100                                                                  strac00p
006200 PROCEDURE DIVISION.                                              strac00p
006300***************************************************************** strac00p
006400* Store our transaction-id in msg                               * strac00p
006500***************************************************************** strac00p
006600     MOVE EIBTRNID TO WS-WTO-TRAN.                                strac00p
006700                                                                  strac00p
006800***************************************************************** strac00p
006900* Store our terminal id in msg                                  * strac00p
007000***************************************************************** strac00p
007100     MOVE EIBTRMID TO WS-WTO-TERM                                 strac00p
007200                                                                  strac00p
007300***************************************************************** strac00p
007400* Store any passed data in msg                                  * strac00p
007500***************************************************************** strac00p
007600     IF EIBCALEN IS EQUAL TO 0                                    strac00p
007700        MOVE 'Unknown' TO WS-WTO-PROG                             strac00p
007800     ELSE                                                         strac00p
007900        MOVE LK-CALLING-RTN(1:EIBCALEN) TO WS-WTO-PROG            strac00p
008000     END-IF.                                                      strac00p
008100                                                                  strac00p
008200***************************************************************** strac00p
008300* Display the msg                                               * strac00p
008400***************************************************************** strac00p
008500     IF NOT TRACE-LEVEL-0                                         strac00p
008600        IF EIBTRMID IS NOT EQUAL TO SPACES                        strac00p
008700           EXEC CICS WRITE                                        strac00p
008800                     OPERATOR                                     strac00p
008900                     TEXT(WS-WTO-DATA)                            strac00p
009000                     TEXTLENGTH(LENGTH OF WS-WTO-DATA)            strac00p
009100           END-EXEC                                               strac00p
009200                                                                  strac00p
009300           EXEC CICS WRITEQ TD                                    strac00p
009400                     QUEUE('CSMT')                                strac00p
009500                     FROM(WS-WTO-DATA)                            strac00p
009600                     LENGTH(LENGTH OF WS-WTO-DATA)                strac00p
009700           END-EXEC                                               strac00p
009800                                                                  strac00p
009900        END-IF                                                    strac00p
010000     END-IF.                                                      strac00p
010100                                                                  strac00p
010200***************************************************************** strac00p
010300* Now we have to have finished and can return to our invoker.   * strac00p
010400***************************************************************** strac00p
010500     EXEC CICS                                                    strac00p
010600          RETURN                                                  strac00p
010700     END-EXEC.                                                    strac00p
010800     GOBACK.                                                      strac00p
010900                                                                  strac00p
011000* $ Version 5.99c sequenced on Wednesday 3 Mar 2011 at 1:00pm     strac00p
