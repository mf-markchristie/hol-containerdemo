000100***************************************************************** udatecnv
000200*                                                               * udatecnv
000300*   Copyright (C) 1998-2010 Micro Focus. All Rights Reserved.   * udatecnv
000400*   This demonstration program is provided for use by users     * udatecnv
000500*   of Micro Focus products and may be used, modified and       * udatecnv
000600*   distributed as part of your application provided that       * udatecnv
000700*   you properly acknowledge the copyright of Micro Focus       * udatecnv
000800*   in this material.                                           * udatecnv
000900*                                                               * udatecnv
001000***************************************************************** udatecnv
001100                                                                  udatecnv
001200***************************************************************** udatecnv
001300* Program:     UDATECNV.CBL                                     * udatecnv
001400* Function:    Date conversion utility routine                  * udatecnv
001500***************************************************************** udatecnv
001600                                                                  udatecnv
001700 IDENTIFICATION DIVISION.                                         udatecnv
001800 PROGRAM-ID.                                                      udatecnv
001900     UDATECNV.                                                    udatecnv
002000 DATE-WRITTEN.                                                    udatecnv
002100     September 2002.                                              udatecnv
002200 DATE-COMPILED.                                                   udatecnv
002300     Today.                                                       udatecnv
002400                                                                  udatecnv
002500 ENVIRONMENT DIVISION.                                            udatecnv
002600                                                                  udatecnv
002700 DATA DIVISION.                                                   udatecnv
002800 WORKING-STORAGE SECTION.                                         udatecnv
002900 01  WS-MISC-STORAGE.                                             udatecnv
003000   05  comp5    pic x(2) comp-5.                                  udatecnv
003100   05  WS-PROGRAM-ID                         PIC X(8)             udatecnv
003200       VALUE 'UDATECNV'.                                          udatecnv
003300   05  WS-SAVED-DDI-DATA                     PIC X(20).           udatecnv
003400   05  WS-MONTH-TABLE.                                            udatecnv
003500     10  FILLER          VALUE 'Jan'         PIC X(3).            udatecnv
003600     10  FILLER          VALUE 'Feb'         PIC X(3).            udatecnv
003700     10  FILLER          VALUE 'Mar'         PIC X(3).            udatecnv
003800     10  FILLER          VALUE 'Apr'         PIC X(3).            udatecnv
003900     10  FILLER          VALUE 'May'         PIC X(3).            udatecnv
004000     10  FILLER          VALUE 'Jun'         PIC X(3).            udatecnv
004100     10  FILLER          VALUE 'Jul'         PIC X(3).            udatecnv
004200     10  FILLER          VALUE 'Aug'         PIC X(3).            udatecnv
004300     10  FILLER          VALUE 'Sep'         PIC X(3).            udatecnv
004400     10  FILLER          VALUE 'Oct'         PIC X(3).            udatecnv
004500     10  FILLER          VALUE 'Nov'         PIC X(3).            udatecnv
004600     10  FILLER          VALUE 'Dec'         PIC X(3).            udatecnv
004700   05  WS-MONTH-TABLE-R REDEFINES WS-MONTH-TABLE.                 udatecnv
004800     10  WS-MONTH                            PIC X(3)             udatecnv
004900         OCCURS 12 TIMES.                                         udatecnv
005000   05  WS-DAYS-TABLE.                                             udatecnv
005100     10  WS-DAYS-IN-JAN  VALUE 031           PIC 9(3).            udatecnv
005200     10  WS-DAYS-IN-FEB  VALUE 028           PIC 9(3).            udatecnv
005300     10  WS-DAYS-IN-MAR  VALUE 031           PIC 9(3).            udatecnv
005400     10  WS-DAYS-IN-APR  VALUE 030           PIC 9(3).            udatecnv
005500     10  WS-DAYS-IN-MAY  VALUE 031           PIC 9(3).            udatecnv
005600     10  WS-DAYS-IN-JUN  VALUE 030           PIC 9(3).            udatecnv
005700     10  WS-DAYS-IN-JUL  VALUE 031           PIC 9(3).            udatecnv
005800     10  WS-DAYS-IN-AUG  VALUE 031           PIC 9(3).            udatecnv
005900     10  WS-DAYS-IN-SEP  VALUE 030           PIC 9(3).            udatecnv
006000     10  WS-DAYS-IN-OCT  VALUE 031           PIC 9(3).            udatecnv
006100     10  WS-DAYS-IN-NOV  VALUE 030           PIC 9(3).            udatecnv
006200     10  WS-DAYS-IN-DEV  VALUE 031           PIC 9(3).            udatecnv
006300   05  WS-DAYS-TABLE-R REDEFINES WS-DAYS-TABLE.                   udatecnv
006400     10  WS-DAYS-IN-MONTH                    PIC 9(3)             udatecnv
006500         OCCURS 12 TIMES.                                         udatecnv
006600   05  WS-DAYS                               PIC 9(3).            udatecnv
006700   05  WS-DAYS-R REDEFINES WS-DAYS.                               udatecnv
006800     10  FILLER                              PIC X(1).            udatecnv
006900     10  WS-DAY-OF-MONTH                     PIC X(2).            udatecnv
007000   05  WS-WORK-MM                            PIC 9(2).            udatecnv
007100   05  WS-WORK-DD                            PIC 9(2).            udatecnv
007200   05  WS-TEMP                               PIC 9(2).            udatecnv
007300   05  WS-SYSTEM-TIME                        PIC 9(8).            udatecnv
007400   05  WS-SYSTEM-TIME-R REDEFINES WS-SYSTEM-TIME.                 udatecnv
007500     10  WS-SYSTEM-TIME-HHMMSS               PIC 9(6).            udatecnv
007600     10  WS-SYSTEM-TIME-DD                   PIC 9(2).            udatecnv
007700   05  WS-WORK-TIME                          PIC 9(6).            udatecnv
007800     88  WORK-TIME-INIT                      VALUE 987654.        udatecnv
007900   05  WS-WORK-TIME-R REDEFINES WS-WORK-TIME.                     udatecnv
008000     10  WS-WORK-TIME-HH                     PIC X(2).            udatecnv
008100     10  WS-WORK-TIME-MM                     PIC X(2).            udatecnv
008200     10  WS-WORK-TIME-SS                     PIC X(2).            udatecnv
008300                                                                  udatecnv
008400 COPY CABENDD.                                                    udatecnv
008500                                                                  udatecnv
008600 LINKAGE SECTION.                                                 udatecnv
008700 01  LK-DATE-WORK-AREA.                                           udatecnv
008800 COPY CDATED.                                                     udatecnv
008900                                                                  udatecnv
009000 PROCEDURE DIVISION USING LK-DATE-WORK-AREA.                      udatecnv
009100     PERFORM TIME-CONVERT THRU                                    udatecnv
009200             TIME-CONVERT-EXIT.                                   udatecnv
009300     PERFORM DATE-CONVERT THRU                                    udatecnv
009400             DATE-CONVERT-EXIT.                                   udatecnv
009500     GOBACK.                                                      udatecnv
009600                                                                  udatecnv
009700 TIME-CONVERT.                                                    udatecnv
009800     SET WORK-TIME-INIT TO TRUE.                                  udatecnv
009900     MOVE SPACES TO DD-TIME-OUTPUT.                               udatecnv
010000                                                                  udatecnv
010100     IF DD-TIME-INPUT IS NOT NUMERIC                              udatecnv
010200        GO TO TIME-CONVERT-ERROR                                  udatecnv
010300     END-IF.                                                      udatecnv
010400     IF DD-ENV-CICS                                               udatecnv
010500        MOVE DD-TIME-INPUT-N TO WS-WORK-TIME                      udatecnv
010600     END-IF.                                                      udatecnv
010700     IF DD-ENV-IMS                                                udatecnv
010800        DIVIDE 10 INTO DD-TIME-INPUT-N GIVING WS-WORK-TIME        udatecnv
010900     END-IF.                                                      udatecnv
011000     IF DD-ENV-NULL OR                                            udatecnv
011100        DD-ENV-INET                                               udatecnv
011200        ACCEPT WS-SYSTEM-TIME FROM TIME                           udatecnv
011300        MOVE WS-SYSTEM-TIME-HHMMSS TO WS-WORK-TIME                udatecnv
011400     END-IF.                                                      udatecnv
011500     IF WORK-TIME-INIT                                            udatecnv
011600         GO TO TIME-CONVERT-ERROR                                 udatecnv
011700     END-IF.                                                      udatecnv
011800     MOVE WS-WORK-TIME-HH TO DD-TIME-OUTPUT-HH.                   udatecnv
011900     MOVE ':'             TO DD-TIME-OUTPUT-SEP1.                 udatecnv
012000     MOVE WS-WORK-TIME-MM TO DD-TIME-OUTPUT-MM.                   udatecnv
012100     MOVE ':'             TO DD-TIME-OUTPUT-SEP2.                 udatecnv
012200     MOVE WS-WORK-TIME-SS TO DD-TIME-OUTPUT-SS.                   udatecnv
012300     GO TO TIME-CONVERT-EXIT.                                     udatecnv
012400                                                                  udatecnv
012500 TIME-CONVERT-ERROR.                                              udatecnv
012600     MOVE 'hh:mm:ss' TO DD-TIME-OUTPUT.                           udatecnv
012700 TIME-CONVERT-EXIT.                                               udatecnv
012800     EXIT.                                                        udatecnv
012900                                                                  udatecnv
013000 DATE-CONVERT.                                                    udatecnv
013100     MOVE SPACES TO DDO-DATA.                                     udatecnv
013200                                                                  udatecnv
013300     IF NOT DDI-ISO AND                                           udatecnv
013400        NOT DDI-YYYYMMDD AND                                      udatecnv
013500        NOT DDI-YYMMDD AND                                        udatecnv
013600        NOT DDI-YYDDD                                             udatecnv
013700        MOVE 'ERROR1' TO DDO-DATA                                 udatecnv
013800        GO TO DATE-CONVERT-EXIT                                   udatecnv
013900     END-IF.                                                      udatecnv
014000     MOVE DDI-DATA TO WS-SAVED-DDI-DATA.                          udatecnv
014100     IF DDI-ISO                                                   udatecnv
014200        PERFORM DATE-CONVERT-IP-OPT1 THRU                         udatecnv
014300                DATE-CONVERT-IP-OPT1-EXIT                         udatecnv
014400     END-IF.                                                      udatecnv
014500     IF DDI-YYYYMMDD                                              udatecnv
014600        PERFORM DATE-CONVERT-IP-OPT2 THRU                         udatecnv
014700                DATE-CONVERT-IP-OPT2-EXIT                         udatecnv
014800     END-IF.                                                      udatecnv
014900     IF DDI-YYMMDD                                                udatecnv
015000        PERFORM DATE-CONVERT-IP-OPT3 THRU                         udatecnv
015100                DATE-CONVERT-IP-OPT3-EXIT                         udatecnv
015200     END-IF.                                                      udatecnv
015300     IF DDI-YYDDD                                                 udatecnv
015400        PERFORM DATE-CONVERT-IP-OPT4 THRU                         udatecnv
015500                DATE-CONVERT-IP-OPT4-EXIT                         udatecnv
015600     END-IF.                                                      udatecnv
015700 DATE-CONVERT-EXIT.                                               udatecnv
015800     EXIT.                                                        udatecnv
015900                                                                  udatecnv
016000* Input option1 - input is ISO (yyyy-mm-dd)                       udatecnv
016100 DATE-CONVERT-IP-OPT1.                                            udatecnv
016200     EVALUATE TRUE                                                udatecnv
016300       WHEN DDO-DD-MMM-YY                                         udatecnv
016400         MOVE DDI-DATA-ISO-DD TO                                  udatecnv
016500              DDO-DATA-DD-MMM-YY-DD                               udatecnv
016600         MOVE '.' TO                                              udatecnv
016700              DDO-DATA-DD-MMM-YY-DOT1                             udatecnv
016800         MOVE WS-MONTH (DDI-DATA-ISO-MM-N) TO                     udatecnv
016900              DDO-DATA-DD-MMM-YY-MMM                              udatecnv
017000         MOVE '.' TO                                              udatecnv
017100              DDO-DATA-DD-MMM-YY-DOT2                             udatecnv
017200         MOVE DDI-DATA-ISO-YYYY (3:2) TO                          udatecnv
017300              DDO-DATA-DD-MMM-YY-YY                               udatecnv
017400       WHEN DDO-DD-MMM-YYYY                                       udatecnv
017500         MOVE DDI-DATA-ISO-DD TO                                  udatecnv
017600              DDO-DATA-DD-MMM-YYYY-DD                             udatecnv
017700         MOVE '.' TO                                              udatecnv
017800              DDO-DATA-DD-MMM-YYYY-DOT1                           udatecnv
017900         MOVE WS-MONTH (DDI-DATA-ISO-MM-N) TO                     udatecnv
018000              DDO-DATA-DD-MMM-YYYY-MMM                            udatecnv
018100         MOVE '.' TO                                              udatecnv
018200              DDO-DATA-DD-MMM-YYYY-DOT2                           udatecnv
018300         MOVE DDI-DATA-ISO-YYYY TO                                udatecnv
018400              DDO-DATA-DD-MMM-YYYY-YYYY                           udatecnv
018500       WHEN OTHER                                                 udatecnv
018600         MOVE 'ERROR2' TO DDO-DATA                                udatecnv
018700     END-EVALUATE.                                                udatecnv
018800 DATE-CONVERT-IP-OPT1-EXIT.                                       udatecnv
018900     EXIT.                                                        udatecnv
019000                                                                  udatecnv
019100* Input option2 - input is yyyymmdd                               udatecnv
019200 DATE-CONVERT-IP-OPT2.                                            udatecnv
019300     EVALUATE TRUE                                                udatecnv
019400       WHEN DDO-DD-MMM-YY                                         udatecnv
019500         MOVE DDI-DATA-YYYYMMDD-DD TO                             udatecnv
019600              DDO-DATA-DD-MMM-YY-DD                               udatecnv
019700         MOVE '.' TO                                              udatecnv
019800              DDO-DATA-DD-MMM-YY-DOT1                             udatecnv
019900         MOVE WS-MONTH (DDI-DATA-YYYYMMDD-MM-N) TO                udatecnv
020000              DDO-DATA-DD-MMM-YY-MMM                              udatecnv
020100         MOVE '.' TO                                              udatecnv
020200              DDO-DATA-DD-MMM-YY-DOT2                             udatecnv
020300         MOVE DDI-DATA-YYYYMMDD-YYYY (3:2) TO                     udatecnv
020400              DDO-DATA-DD-MMM-YY-YY                               udatecnv
020500       WHEN DDO-DD-MMM-YYYY                                       udatecnv
020600         MOVE DDI-DATA-YYYYMMDD-DD TO                             udatecnv
020700              DDO-DATA-DD-MMM-YYYY-DD                             udatecnv
020800         MOVE '.' TO                                              udatecnv
020900              DDO-DATA-DD-MMM-YYYY-DOT1                           udatecnv
021000         MOVE WS-MONTH (DDI-DATA-YYYYMMDD-MM-N) TO                udatecnv
021100              DDO-DATA-DD-MMM-YYYY-MMM                            udatecnv
021200         MOVE '.' TO                                              udatecnv
021300              DDO-DATA-DD-MMM-YYYY-DOT2                           udatecnv
021400         MOVE DDI-DATA-YYYYMMDD-YYYY TO                           udatecnv
021500              DDO-DATA-DD-MMM-YYYY-YYYY                           udatecnv
021600       WHEN OTHER                                                 udatecnv
021700         MOVE 'ERROR2' TO DDO-DATA                                udatecnv
021800     END-EVALUATE.                                                udatecnv
021900 DATE-CONVERT-IP-OPT2-EXIT.                                       udatecnv
022000     EXIT.                                                        udatecnv
022100                                                                  udatecnv
022200* Input option3 - input is yymmdd                                 udatecnv
022300 DATE-CONVERT-IP-OPT3.                                            udatecnv
022400     EVALUATE TRUE                                                udatecnv
022500       WHEN DDO-DD-MMM-YY                                         udatecnv
022600         MOVE DDI-DATA-YYMMDD-DD TO                               udatecnv
022700              DDO-DATA-DD-MMM-YY-DD                               udatecnv
022800         MOVE '.' TO                                              udatecnv
022900              DDO-DATA-DD-MMM-YY-DOT1                             udatecnv
023000         MOVE WS-MONTH (DDI-DATA-YYMMDD-MM-N) TO                  udatecnv
023100              DDO-DATA-DD-MMM-YY-MMM                              udatecnv
023200         MOVE '.' TO                                              udatecnv
023300              DDO-DATA-DD-MMM-YY-DOT2                             udatecnv
023400         MOVE DDI-DATA-YYMMDD-YY TO                               udatecnv
023500              DDO-DATA-DD-MMM-YY-YY                               udatecnv
023600       WHEN DDO-DD-MMM-YYYY                                       udatecnv
023700         MOVE DDI-DATA-YYMMDD-DD TO                               udatecnv
023800              DDO-DATA-DD-MMM-YYYY-DD                             udatecnv
023900         MOVE '.' TO                                              udatecnv
024000              DDO-DATA-DD-MMM-YYYY-DOT1                           udatecnv
024100         MOVE WS-MONTH (DDI-DATA-YYMMDD-MM-N) TO                  udatecnv
024200              DDO-DATA-DD-MMM-YYYY-MMM                            udatecnv
024300         MOVE '.' TO                                              udatecnv
024400              DDO-DATA-DD-MMM-YYYY-DOT2                           udatecnv
024500         MOVE DDI-DATA-YYMMDD-YY TO                               udatecnv
024600              DDO-DATA-DD-MMM-YYYY-YYYY (3:2)                     udatecnv
024700         IF DDI-DATA-YYMMDD-YY IS LESS THAN '50'                  udatecnv
024800            MOVE '20' TO DDO-DATA-DD-MMM-YYYY-YYYY (1:2)          udatecnv
024900         ELSE                                                     udatecnv
025000            MOVE '19' TO DDO-DATA-DD-MMM-YYYY-YYYY (1:2)          udatecnv
025100         END-IF                                                   udatecnv
025200       WHEN OTHER                                                 udatecnv
025300         MOVE 'ERROR2' TO DDO-DATA                                udatecnv
025400     END-EVALUATE.                                                udatecnv
025500 DATE-CONVERT-IP-OPT3-EXIT.                                       udatecnv
025600     EXIT.                                                        udatecnv
025700                                                                  udatecnv
025800* Input option4 - input is yyddd                                  udatecnv
025900 DATE-CONVERT-IP-OPT4.                                            udatecnv
026000     DIVIDE 4 INTO DDI-DATA-YYDDD-YY-N                            udatecnv
026100       GIVING WS-TEMP.                                            udatecnv
026200     MULTIPLY WS-TEMP BY 4 GIVING WS-TEMP.                        udatecnv
026300     IF WS-TEMP EQUAL TO DDI-DATA-YYDDD-YY-N                      udatecnv
026400        MOVE 029 TO WS-DAYS-IN-MONTH(2)                           udatecnv
026500     ELSE                                                         udatecnv
026600        MOVE 028 TO WS-DAYS-IN-MONTH(2)                           udatecnv
026700     END-IF.                                                      udatecnv
026800     MOVE DDI-DATA-YYDDD-DDD TO WS-DAYS.                          udatecnv
026900     MOVE 01 TO WS-WORK-MM.                                       udatecnv
027000 DATE-CONVERT-IP-OPT4-DAYS.                                       udatecnv
027100     IF WS-DAYS IS GREATER THAN WS-DAYS-IN-MONTH(WS-WORK-MM)      udatecnv
027200        SUBTRACT WS-DAYS-IN-MONTH(WS-WORK-MM) FROM WS-DAYS        udatecnv
027300        ADD 1 TO WS-WORK-MM                                       udatecnv
027400        GO TO DATE-CONVERT-IP-OPT4-DAYS.                          udatecnv
027500     EVALUATE TRUE                                                udatecnv
027600       WHEN DDO-DD-MMM-YY                                         udatecnv
027700         MOVE WS-DAY-OF-MONTH TO                                  udatecnv
027800              DDO-DATA-DD-MMM-YY-DD                               udatecnv
027900         MOVE '.' TO                                              udatecnv
028000              DDO-DATA-DD-MMM-YY-DOT1                             udatecnv
028100         MOVE WS-MONTH (WS-WORK-MM) TO                            udatecnv
028200              DDO-DATA-DD-MMM-YY-MMM                              udatecnv
028300         MOVE '.' TO                                              udatecnv
028400              DDO-DATA-DD-MMM-YY-DOT2                             udatecnv
028500         MOVE DDI-DATA-YYDDD-YY TO                                udatecnv
028600              DDO-DATA-DD-MMM-YY-YY                               udatecnv
028700       WHEN DDO-DD-MMM-YYYY                                       udatecnv
028800         MOVE WS-DAY-OF-MONTH TO                                  udatecnv
028900              DDO-DATA-DD-MMM-YYYY-DD                             udatecnv
029000         MOVE '.' TO                                              udatecnv
029100              DDO-DATA-DD-MMM-YYYY-DOT1                           udatecnv
029200         MOVE WS-MONTH (WS-WORK-MM) TO                            udatecnv
029300              DDO-DATA-DD-MMM-YYYY-MMM                            udatecnv
029400         MOVE '.' TO                                              udatecnv
029500              DDO-DATA-DD-MMM-YYYY-DOT2                           udatecnv
029600         MOVE DDI-DATA-YYDDD-YY TO                                udatecnv
029700              DDO-DATA-DD-MMM-YYYY-YYYY (3:2)                     udatecnv
029800         IF DDI-DATA-YYDDD-YY IS LESS THAN '50'                   udatecnv
029900            MOVE '20' TO DDO-DATA-DD-MMM-YYYY-YYYY (1:2)          udatecnv
030000         ELSE                                                     udatecnv
030100            MOVE '19' TO DDO-DATA-DD-MMM-YYYY-YYYY (1:2)          udatecnv
030200         END-IF                                                   udatecnv
030300       WHEN OTHER                                                 udatecnv
030400         MOVE 'ERROR2' TO DDO-DATA                                udatecnv
030500     END-EVALUATE.                                                udatecnv
030600 DATE-CONVERT-IP-OPT4-EXIT.                                       udatecnv
030700     EXIT.                                                        udatecnv
030800                                                                  udatecnv
030900* $ Version 5.99c sequenced on Wednesday 3 Mar 2011 at 1:00pm     udatecnv
