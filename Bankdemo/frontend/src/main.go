package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
)
//	"text/template"

var ServiceToCall = "ed80-bnkd-svc-mfdbfh"
const form = `
<html>
<!--
	Eventually by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<head>
    <title>Micro Focus Mythical Bank Demo</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="assets/css/main.css" />
</head>
<body class="is-preload"> 
    <div id="main">
        <div>
            <!-- Header -->
            <header id="header">
                <h1>Micro Focus' Demonstration Bank</h1>
				<h2>A simple website based on a mythical bank</h2>
                <p>
                    Simple demonstration web page which invokes a COBOL loan calculation routine packaged
					as an IMTK REST web service hosted within Micro Focus Enterprise Server instances running
					in a Kubernetes cluster.
                </p>
            </header>
        </div>
        <div>
            <form id="loancalc-form" method="post" action="calc">
                <input type="text" name="amount" id="amount" placeholder="Loan Amount" />
                <input type="text" name="term" id="term" placeholder="Loan Term (months)" />
                <input type="text" name="percent" id="percent" placeholder="Annual Interest Rate (percentage)" />
                <input type="submit" value="Calculate" />
            </form>
        </div>
        <div>
            <div ng-app="myApp" ng-hide="hidestoredetails">
                <div ng-controller="myCtrl as bio">
                    <div class="row">
                        <div class="col-md-3 col-md-offset-1">
                            <div class="well">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <p class="panel-title">%v</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div id="footer">
                <ul class="icons">
                    <li><a href="https://twitter.com/microfocus" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                    <li><a href="https://www.facebook.com/MicroFocusCorp" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                    <li><a href="https://www.linkedin.com/company/micro-focus" class="icon fa-linkedin"><span class="label">Linkedin</span></a></li>
          <!--          <li><a href="mailto:Guy.Sofer@MicroFocus.com" class="icon fa-envelope-o"><span class="label">Email</span></a></li> -->
                </ul>
                <ul class="copyright">
                    <li>&copy; 2020 Micro Focus.</li>
                    <li>Credits: <a href="http://html5up.net">HTML5 UP</a></li>
                </ul>
            </div>
        </div>
    </div>    
    <!-- Footer -->
   

    <!-- Scripts -->
    <script src="assets/js/main.js"></script>

</body>
</html>
`

type LoanCalcData struct {
    Monthly float32
}

func main() {

    /* Allow for alternative loan calculator service DNS name/port */
	potentialservice := os.Getenv("LOANCALCSERVICE_NAME")
	if potentialservice != "" {
		ServiceToCall = potentialservice
	}

	//tmpl := template.Must(template.ParseFiles("page/index.html"))
    //http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
    //    data := LoanCalcData{
    //        Monthly: 0,
    //    }
    //    tmpl.Execute(w, data)
    //})
	
	/* Start the web server */
	http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("./assets"))))
	http.Handle("/images/", http.StripPrefix("/images/", http.FileServer(http.Dir("./images"))))
	http.HandleFunc("/calc", handler)
	http.HandleFunc("/", view)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func view(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, form, "")
}

/* Web service input structure as created by IMTK */
type WS_REQ_STRUCT struct {
	BRE_I_WS_CALC_WORK_A int
	BRE_I_WS_CALC_WORK_P float32
	BRE_I_WS_CALC_WORK_T int
}
/* Web service output structure as created by IMTK */
type WS_RESP_STRUCT struct {
	BRE_O_WS_LOAN_MONTHL float32
}

func handler(w http.ResponseWriter, r *http.Request) {
	
	/* Get the web service input values from the request */
	percent, _ := strconv.ParseFloat(r.FormValue("percent"), 32)
	amount, _ := strconv.ParseFloat(r.FormValue("amount"),32)
	term, _ := strconv.Atoi(r.FormValue("term"))

	/* Create the web service structure with values from the web page */
	var loanCalcInput WS_REQ_STRUCT
	loanCalcInput.BRE_I_WS_CALC_WORK_A = int(amount)	
	loanCalcInput.BRE_I_WS_CALC_WORK_P = float32(percent)
	loanCalcInput.BRE_I_WS_CALC_WORK_T = term
	
	/* Invoke the IMTK web service running in an Enterprise Server */
	jsoncalc, _ := json.Marshal(loanCalcInput)
	var jsoncalcreader io.Reader = bytes.NewReader(jsoncalc)
	resp, err := http.Post("http://"+ServiceToCall+"/Bank/LoanCalculator/1.0/LOANCALC", "application/json", jsoncalcreader)
	
	/* Handle any errors */
	if err != nil {
		fmt.Fprintf(w, form, err)
		return
	}
	if resp.StatusCode != http.StatusOK {
		fmt.Fprintf(w, form, "Error response=" +resp.Status)
		return
	}

	/* Get the result of the loan calculation */
	var record WS_RESP_STRUCT
	if err := json.NewDecoder(resp.Body).Decode(&record); err != nil {
		resp.Body.Close()
		fmt.Fprintf(w, form, "Error response=" +resp.Status)
		return
	}
	resp.Body.Close()
	
	/* Display the result to the user */
	calcresult := fmt.Sprintf("Monthly payment would be %.2f for a loan of %v at %.2f percent over %v months", record.BRE_O_WS_LOAN_MONTHL, amount, percent, term)
	fmt.Fprintf(w, form, calcresult)
}
