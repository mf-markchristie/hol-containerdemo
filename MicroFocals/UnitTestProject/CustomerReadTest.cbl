      $set remove"address"
      $set remove"title"
       copy "mfunit_prototypes.cpy".
       
       identification division.
       program-id. CustomerReadTest.
       environment division.
       special-names.
         crt status is key-status.
       file-control.
         copy "customer-fc.cpy".

       data division.
       fd cust-file.
       01 f-CustomerInformation.
       copy "customerinfo.cpy" replacing ==:Prefix-:== by ==f-==.

       working-storage section.
       78 TEST-CustomerReadTest value "CustomerReadTest".
       01 CustomerInformation.
       COPY "customerinfo.cpy" replacing ==:Prefix-:== by == ==.
       COPY "common_ws.cpy".
       01 eof            pic x.
       01 counter        binary-long.
       01 ws-env         pic x(80).
       copy "mfunit.cpy".

       procedure division.
           goback returning 0
       .

       entry MFU-TC-PREFIX & TEST-CustomerReadTest.
          move "n" to eof
          move 1 to counter
          move 0 to f-Customer-Id
          start cust-file
            key > f-Customer-Id
             invalid key
              move "y" to eof
          end-start

          perform until eof equals "y"
            move all spaces to f-CustomerInformation
            read cust-file next record
              at end
                move "y" to eof
            end-read

            *> does the some of the fields look okay?
            if eof equals "n"
               if f-Title equal spaces
                   display "f-title should not be spaces"
                   goback returning 3
               end-if
               if f-Dob equal spaces
                   display "f-dob should not be spaces"
                   goback returning 4
               end-if
              add 1 to counter
            end-if
           end-perform

           *> is they enough records in the file?
           if counter < 100
               display "Sorry not enough records in customer file"
               goback returning 2
           end-if
           display "Total number of customers read : " counter

           goback returning MFU-PASS-RETURN-CODE
       .

      $region Test Configuration

       entry MFU-TC-SETUP-PREFIX & TEST-CustomerReadTest.
           accept ws-env from environment "MFOCALDIR"
           display "MFOCALDIR is " ws-env
           open i-o cust-file
           perform check-file-status-no-popup
           if ws-file-status not equal "00"
               call MFU-ASSERT-FAIL-Z using by reference z"Unable to open customer file"
               exhibit named ws-file-status
               exhibit named popup-title
               exhibit named popup-message-1
               if popup-message-2 not equal spaces
                   exhibit named popup-message-2
               end-if
               if popup-message-3 not equal space
                   exhibit named popup-message-3
               end-if
               goback returning 1
           end-if
           goback returning 0
       .

       entry MFU-TC-TEARDOWN-PREFIX & TEST-CustomerReadTest.
           close cust-file
           goback returning 0
       .

       entry MFU-TC-METADATA-SETUP-PREFIX & TEST-CustomerReadTest.
           move "Ensure that we open customer.dat and read all records" to MFU-MD-TESTCASE-DESCRIPTION
           move 10000 to MFU-MD-TIMEOUT-IN-MS
           move "smoke,customer" to MFU-MD-TRAITS
           set MFU-MD-SKIP-TESTCASE to false
           goback returning 0
       .

      $end-region

       copy "common.cpy".

       end program CustomerReadTest.