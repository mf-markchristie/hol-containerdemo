       copy "mfunit_prototypes.cpy".
       
       identification division.
       program-id. TestCountryInit.

       data division.
       working-storage section.
       copy "common_78.cpy".

       78 TEST-TestCountryInit value "TestCountryInit".
       copy "mfunit.cpy".

       01 test-data.
         03 ws-test-lat        lat-long-type.
         03 ws-test-long       lat-long-type.
         03 ws-test-len binary-long.
         03 ws-test-res pic x(32).

       01 ws-count binary-long.
       01 ws-ret-count binary-long.
       78 max-size value 64.

       01 ws-delay-error-message pic x.

       01 ws-best-store        binary-long.
       01 ws-best-dist-k       distance-type.
       01 ws-best-dist-m       distance-type.

       01 wsf-Store-Information occurs max-size.
       COPY "stores.cpy" replacing ==:Prefix-:== by ==wsf-==.

       01 p procedure-pointer.
       procedure division.
           goback returning 0
       .

       entry MFU-TC-PREFIX & TEST-TestCountryInit.
           *> Test code goes here.
           goback returning MFU-PASS-RETURN-CODE
       .

      $region Test Configuration

       entry MFU-TC-SETUP-PREFIX & TEST-TestCountryInit.
           goback returning 0
       .

       entry MFU-TC-TEARDOWN-PREFIX & TEST-TestCountryInit.
           set p to entry "mfocal"
           set p to entry "autosetup"

           move 0 to ws-ret-count

           perform init-data
           move 51.406879599999996 to ws-test-lat
           move -1.3324460000000045 to ws-test-long
           move all "*" to ws-test-res

           move max-size to ws-test-len
           call "getstoresnear" using by reference "gb "
                                      by reference ws-test-lat
                                      by reference ws-test-long
                                      by reference ws-test-len
                                      by reference wsf-Store-Information(1)
                                      by reference ws-delay-error-message
                                      by reference ws-best-store
                                      by reference ws-best-dist-k
                                      by reference ws-best-dist-m
                            returning ws-ret-count
           end-call

           if ws-ret-count equal 0
               display "No store found near " ws-test-lat " / " ws-test-long
               goback returning 1
           end-if

           perform with test after
             varying ws-count from 1 by 1
               until ws-count equals ws-ret-count or
                     wsf-id(ws-count) equal 0
               perform display-record
           end-perform
           exhibit named ws-ret-count
           goback returning 0
           .

       display-record.
           display "Id            : " wsf-id(ws-count)
           display "Name of store : " wsf-name-of-store(ws-count)
           display "Province      : " wsf-province(ws-count)
           display "County        : " wsf-county(ws-count)
           display "Post code     : " wsf-postcode(ws-count)
           display "Email         : " wsf-email(ws-count)
           display "latitude      : " wsf-latitude(ws-count)
           display "longitude     : " wsf-longitude(ws-count)
           display "Tel           : " wsf-tel(ws-count)
           display "GeoHash       : " wsf-geohash(ws-count)
           display " "
           .
       
       init-data section.
           perform with test after
             varying ws-count from 1 by 1 until ws-count equals max-size
               initialize wsf-Store-Information(ws-count)
               move 0 to wsf-latitude(ws-count) wsf-longitude(ws-count)
           end-perform
           exit section
           .
       entry MFU-TC-METADATA-SETUP-PREFIX & TEST-TestCountryInit.
           move "This is a example of a dynamic description" to MFU-MD-TESTCASE-DESCRIPTION
           move 10000 to MFU-MD-TIMEOUT-IN-MS
           move "smoke,dynmeta" to MFU-MD-TRAITS
           set MFU-MD-SKIP-TESTCASE to false
           goback returning 0
       .

      $end-region


       end program TestCountryInit.