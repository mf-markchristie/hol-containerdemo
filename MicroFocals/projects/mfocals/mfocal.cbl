       identification division.
       program-id. mfocal.
       environment division.
       special-names.
         crt status is key-status.
       input-output section.
       file-control.
       copy "stores-fc.cpy".
       copy "storeconf-fc.cpy".

       copy "stores-fd.cpy".
       copy "storeconf-fd.cpy".
       working-storage section.
       01 k                pic x.

       01 ws-store-info.
       COPY "stores.cpy" replacing ==:Prefix-:== by ==ws-==.

       copy "common_78.cpy".
       copy "gettimeslot.cpy" replacing  ==:Prefix-:== by ==ap-==.

       01 ra-menu-option   pic x.
       01 cmd-line         pic x(128).

       copy "common_ws.cpy".
       01 sf-country-id      pic xxx.
       local-storage section.
       01 ls-eof           pic x.
       01 ls-key-invalid   pic x.

       01 ls-gcd-flags     cblt-os-flags.
       01 ls-gcd-len       cblt-os-size.
       01 ls-gcd-dir-name  pic x(1024).
       SCREEN SECTION.
       copy "welcome.ss".
       copy "appscreen.ss".
       copy "reviewmenu.ss".
       copy "maintscreen.ss".
       copy "storeinfo.ss".

       copy "storesaction.ss".
       copy "common_ss.cpy".
       procedure division.
            initialize ap-time-left-in-mins
            initialize menu-option

            *> enable adis mode
            call "setadismode"

            *> setup the environment
            perform init-env

            *> application configuration
            perform setup-section


            *> Display the initial screen and wait for
            *> five seconds and then continue
            perform clr-screen
            display g-welcome
            accept g-welcome timeout after 5

           *> show the main menu and then accept a key
           *> and perform the right menu action
           perform until menu-option equals "0"
             move "Appointments Menu" TO Menu-Name
             move "M001" to Menu-Id
             perform clr-screen
             display g-menuheader
             display g-appscreen

             move space to menu-option
             perform with test after until menu-option not equal space
               move "n" to ls-key-invalid
               call "gettimeslot" using
                    by reference ap-current-timeslot
               end-call
               display app-time-slot
               accept g-appscreen timeout after 1
               move key-code-1 to key-code-1-display
               if key-type equals adis-term-prog
                evaluate key-code-1
                   when kc-escape
                        move "0" to menu-option
                   when kc-f1-key
                      perform show-f1-help
                      *> display g-apphelp
                      *> accept k at 2379
                   when other
                         move "y" to ls-key-invalid
                 end-evaluate
                end-if
             end-perform

             *> What shall we do with the menu option?
             evaluate menu-option
                when 'a'
                when 'A'
                        call "consultsum" using ws-store-info
                        cancel "consultsum"
                when 's'
                when 'S'
                        perform clr-screen
                        call "scheduleapp" using ws-store-info
                        cancel "scheduleapp"
                        perform clr-screen
                when 'r'
                when 'R'
                        perform review-menu
                when 'm'
                when 'M'
                        perform maint-menu
                when 'q'
                when 'Q'
                        move "0" to menu-option
                when "l"
                when "L"
                       perform locate-store
             end-evaluate

             *> Ensure all the popup's are in the correct state
             cancel "consultpopup"
             cancel "storepopup"
             cancel "custpopup"

            end-perform

           perform clr-screen
           display "Thank you."
           stop run.

        setup-section.
            perform clr-screen

            move spaces to cmd-line store-name
            accept cmd-line from command-line
            display cmd-line
            move "n" to ls-eof

            if cmd-line not equal spaces
                call "getonestore" using
                    by reference cmd-line
                    by reference ws-store-info
                end-call

                if return-code > 1
                    display "Sorry store name is not unique"
                    stop run
                end-if

                if return-code less than or equal 0
                    display "Store not found (" return-code ")"
                    stop run
                end-if

                move ws-name-of-store to store-name
            end-if

            *> do we have a default store?
            perform get-storeconf
            if conf-current-id equal 0
                move 0 to ls-gcd-flags
                move length of ls-gcd-dir-name to ls-gcd-len
                call "CBL_GET_CURRENT_DIR" using
                   by value ls-gcd-flags
                   by value ls-gcd-len
                   by reference ls-gcd-dir-name
                end-call

                perform show-no-config-popup

                perform clr-screen
                display g-menuheader
                move 0 to ws-id
                call "storemaintenance" using ws-store-info
                stop run
            end-if

            if cmd-line equal spaces
                *> get the default store
                call "getdefstore" using ws-store-info
                if return-code not equal 0
                   move spaces to store-name
                else
                   move ws-name-of-store to store-name
                end-if
            end-if

            if store-name equal spaces
                perform show-no-config-popup
                perform maint-menu
                stop run
            end-if
            .

        show-no-config-popup.
               move "Configuration error" to popup-title
                move "No default store configured" to popup-message-1
                move spaces to popup-message-2
                move "OK" to popup-button-1

                perform clr-screen
                call "errpopup" using popup-title,
                        popup-message-1,
                        popup-message-2
                        popup-button-1
                end-call
            .


        maint-menu.
            perform clr-screen
            move "General Maintenance Menu" TO Menu-Name
            display g-menuheader

            move "maint-menu" to f1-name-buf
            display g-maintscreen
            accept g-maintscreen

            evaluate menu-option
                when 'c'
                when 'C'
                 perform clr-screen
                 call "custmenu" using ws-store-info
                 cancel "custmenu"
                 perform clr-screen

                 *> reload record app config
                 perform setup-section

                when 't'
                when 'T'
                 perform clr-screen
                 call "constmenu" using ws-store-info
                 cancel "constmenu"
                 perform clr-screen

                when 'm'
                when 'M'
                 call "storemaintenance" using ws-store-info
                 cancel "storemaintenance"
                 perform clr-screen

                 *> reload record app config
                 perform setup-section
                when other
                   perform help-required
            end-evaluate
        .

       locate-store.
           perform clr-screen
           move "Stores Finder" to Menu-Name
           move "SF01" to Menu-Id
           move spaces to Store-name
           perform until menu-option equals "y" or "Y"
               perform clr-screen
               display G-MENUHEADER
               display G-STORESACTION
               accept G-STORESACTION

               evaluate Menu-Option
                   when "l"
                   when "L"
                       cancel "locatestore"
                       call "locatestore"
                   when "s"
                   when "S"
                       perform search-store-in-country
                   when other
                       move "y" to Menu-Option
                       move ws-name-of-store to store-name
               end-evaluate
           end-perform.

       search-store-in-country.
           move spaces to Store-name
           call "storepopup" using sf-Store-Information
           if return-code equal 0
             display G-STOREINFO
             perform press-any-key
           end-if.

        review-menu.
            perform clr-screen
            move "Review Appointments Menu" TO Menu-Name
            move "RA001" to Menu-Id
            move "review-menu" to f1-name-buf
            display g-menuheader
            display g-reviewmenu
            accept g-reviewmenu

            evaluate ra-menu-option
                when 'A'
                when 'a'
                when 'C'
                when 'c'
                when 'g'
                when 'G'
                when 'p'
                when 'P'
                   call "schedread" using
                       by reference ws-store-info
                       by reference ra-menu-option
                   end-call
                   cancel "schedread"
                when other
                   perform help-required
            end-evaluate
            perform clr-screen
        .


        help-required.
              *> move key-code-1 to key-code-1-display
              if key-type equals adis-term-prog
                evaluate key-code-1
                   when kc-escape
                      goback returning -1

                   when kc-f1-key
                      perform save-f1-screen
                      call "mfocalhelp" using
                         by reference f1-name-buf
                       end-call
                      perform restore-f1-screen
                end-evaluate
              end-if
              .

        copy "common.cpy".
        copy "storeconf_common.cpy".
