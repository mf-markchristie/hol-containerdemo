      $set sourceformat"variable"
       copy "cblproto".
      $if windows defined
      $set change-message "1122 I"
       copy "windows.cpy".
      $set change-message "1122 W"
      $end

      $set sourceformat"variable"
       program-id. autosetup.
       environment division.
       configuration section.
       repository.
       special-names.
           call-convention 74 is winapi.
       data division.
       working-storage section.

       copy "common_78.cpy".

      $if windows defined
       01 win-lcid             LCID.
      $end

       01 GetOS-block          usage cblt-os-info-params.
       01 File-Details         usage cblt-fileexist-buf.

       01 status-code          usage cblt-rtncode.

       01 poss-dir-count       binary-long value 1.
       01 poss-dirs            pic x(128) occurs 5.

       01 tmp-num-d            pic 999 display.
       01 tmp-count            binary-long.
       01 tmp-lang             pic xx. *> size of 2 on purpose...

       01 file-status          pic xx comp-x.
       01 redefines file-status.
            03 fs-byte-1  pic x.
            03 fs-byte-2  cblt-x1-compx.

       01 mfocaldir-env       pic x(255).
       01 mfocalhdir-env      pic x(255).

       linkage section.
       01 lnk-country         pic xxx.
       procedure division.
           perform init-os-info

           perform check-env-for-base-dir

           *> use
           perform guess-dirs

           *> fallback is uk...
           add 1 to poss-dir-count
           move "gb" to poss-dirs(poss-dir-count)

           perform varying tmp-count from 1 by 1
                   until tmp-count > poss-dir-count

                call "CBL_CHECK_FILE_EXIST" using
                    by reference poss-dirs(tmp-count)
                    by reference File-Details
                end-call
                move return-code to file-status
                *> is it a directory? ie: 9/21
                if fs-byte-1 equals 9 and fs-byte-2 equals 21
                 display "MFOCALDIR" upon environment-name
                 display poss-dirs(tmp-count) upon environment-value
                 display "MFOCALCP" upon environment-name
                 display poss-dirs(tmp-count) upon environment-value

                 display "MFOCALHDIR" upon environment-name
                 string
                     poss-dirs(tmp-count) delimited by space
                     "/../docs" delimited by size
                     into mfocalhdir-env
                 end-string
                 display mfocalhdir-env upon environment-value
                 goback returning AUTOSETUP-OK
                end-if
           end-perform
           goback returning AUTOSETUP-FAILED.

       init-os-info section.
           move 28 to cblte-osi-length of GetOS-block

           call CBL-GET-OS-INFO using GetOS-block
                            returning status-code
           end-call.

       guess-dirs section.
           move 0 to poss-dir-count

           if status-code not = 0
              goback returning AUTOSETUP-FAILED
           end-if

      *
      *        131 for Windows systems, 128 for COBOL system on UNIX
      *
           evaluate cblte-osi-os-type of GetOS-block
              when 131
                 perform use-countrycode-win
              when 128
                 move spaces to tmp-lang
                 display "LC_MESSAGES" upon environment-name
                 accept tmp-lang from environment-value
                 if tmp-lang not equal spaces
                  add 1 to poss-dir-count
                  move tmp-lang to poss-dirs(poss-dir-count)
                 end-if
              when other
                 *> display "Unknown OS!"
                 goback returning AUTOSETUP-FAILED
           end-evaluate
           .

       check-env-for-base-dir section.
           display "MFOCALDIR" upon environment-name
           accept mfocaldir-env from environment-value
           if mfocaldir-env not equal spaces
               call "CBL_CHECK_FILE_EXIST" using by reference mfocaldir-env
                                                 by reference File-Details
               end-call
               move return-code to file-status

               *> is it a directory? ie: 9/21
               if fs-byte-1 equals 9 and fs-byte-2 equals 21
                   goback returning AUTOSETUP-OK
               end-if
           end-if
           .

       use-countrycode-win section.
           evaluate cblte-osi-country-id of GetOS-block
                when 1
                    add 1 to poss-dir-count
                    move "us" to poss-dirs(poss-dir-count)
                when 27
                    add 1 to poss-dir-count
                    move "nz" to poss-dirs(poss-dir-count)
                when 31
                    add 1 to poss-dir-count
                    move "nl" to poss-dirs(poss-dir-count)
                when 33
                    add 1 to poss-dir-count
                    move "fr" to poss-dirs(poss-dir-count)
                when 34
                    add 1 to poss-dir-count
                    move "es" to poss-dirs(poss-dir-count)
                when 39
                    add 1 to poss-dir-count
                    move "it" to poss-dirs(poss-dir-count)
                when 43
                    add 1 to poss-dir-count
                    move "at" to poss-dirs(poss-dir-count)
                when 44
                    add 1 to poss-dir-count
                    move "gb" to poss-dirs(poss-dir-count)
                when 45
                    add 1 to poss-dir-count
                    move "dk" to poss-dirs(poss-dir-count)
                when 46
                    add 1 to poss-dir-count
                    move "se" to poss-dirs(poss-dir-count)
                when 47
                    add 1 to poss-dir-count
                    move "no" to poss-dirs(poss-dir-count)
                when 49
                    add 1 to poss-dir-count
                    move "de" to poss-dirs(poss-dir-count)
                when 52
                    add 1 to poss-dir-count
                    move "mx" to poss-dirs(poss-dir-count)
                when 55
                    add 1 to poss-dir-count
                    move "br" to poss-dirs(poss-dir-count)
                when 61
                    add 1 to poss-dir-count
                    move "au" to poss-dirs(poss-dir-count)
                when 64
                    add 1 to poss-dir-count
                    move "nz" to poss-dirs(poss-dir-count)
                when 81
                    add 1 to poss-dir-count
                    move "jp" to poss-dirs(poss-dir-count)
                when 82
                    add 1 to poss-dir-count
                    move "kp" to poss-dirs(poss-dir-count)
                when 86
                    add 1 to poss-dir-count
                    move "rc" to poss-dirs(poss-dir-count)
                when 91
                    add 1 to poss-dir-count
                    move "in" to poss-dirs(poss-dir-count)
                when 351
                    add 1 to poss-dir-count
                    move "pt" to poss-dirs(poss-dir-count)
                when 351
                    add 1 to poss-dir-count
                    move "fi" to poss-dirs(poss-dir-count)
                when 351
                    add 1 to poss-dir-count
                    move "tw" to poss-dirs(poss-dir-count)
                when other
                    display "INFO: unknown "
                       cblte-osi-country-id of GetOS-block
                    stop "Pause.."
           end-evaluate

           *> display "INFO: "
           *>             cblte-osi-country-id of GetOS-block
           add 1 to poss-dir-count
           move cblte-osi-country-id of GetOS-block to tmp-num-d
           move tmp-num-d to poss-dirs(poss-dir-count)

           .
       manual-init-ep section.
       entry "mfocal_init" using lnk-country.

           display "MFOCAL_STORES_DIRECTORY" upon environment-name
           accept mfocaldir-env from environment-value
           if mfocaldir-env not equal spaces
               call "CBL_CHECK_FILE_EXIST" using by reference mfocaldir-env
                                                 by reference File-Details
               end-call
               move return-code to file-status

               *> is it a directory? ie: 9/21, continue
               if fs-byte-1 not equals 9 and not fs-byte-2 equals 21
                   goback returning AUTOSETUP-FAILED
               end-if
           else
               move "data" to mfocaldir-env
               call "CBL_CHECK_FILE_EXIST" using by reference mfocaldir-env
                                                 by reference File-Details
               end-call
               move return-code to file-status
               
               *> is it a directory? ie: 9/21, continue
               if fs-byte-1 not equals 9 and not fs-byte-2 equals 21
                   goback returning AUTOSETUP-FAILED
               end-if
               display "MFOCAL_STORES_DIRECTORY" upon environment-name
               display mfocaldir-env upon environment-value
           end-if

           if lnk-country equal spaces
               goback returning AUTOSETUP-NO-LOCALE
           end-if

           string "$MFOCAL_STORES_DIRECTORY/" delimited by size
             lnk-country delimited by size
             " " delimited by size
             into poss-dirs(1)
           end-string

           call "CBL_CHECK_FILE_EXIST" using by reference poss-dirs(1)
                                             by reference File-Details
           end-call
           move return-code to file-status

           *> is it a directory? ie: 9/21
           if fs-byte-1 equals 9 and fs-byte-2 equals 21
               display "MFOCALDIR" upon environment-name
               display poss-dirs(1) upon environment-value

               display "MFOCALCP" upon environment-name
               display lnk-country upon environment-value

               perform setup-os-environment
               goback returning 0
           end-if

           goback returning 1
           .

       setup-os-environment section.

      $if experiment-changing-locale defined
           evaluate lnk-country
               when "dex"
                   move 1031 to win-lcid
                   call "setlocale" using by value 0
                                          by reference z"de-DE"
                   end-call
                   call winapi "SetThreadLocale" using by value win-lcid
                   call winapi "SetThreadUILanguage"  using by value win-lcid
                   call winapi "SetConsoleCP" using by value 850 
                   call winapi "SetConsoleOutputCP" using by value 850
           end-evaluate.
      $end

