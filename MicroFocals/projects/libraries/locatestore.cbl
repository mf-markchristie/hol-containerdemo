      $set sourceformat"variable"
       identification division.
       program-id. locatestore.

       environment division.
       input-output section.
       file-control.
       copy "stores-fc.cpy".
       copy "storeconf-fc.cpy".

       copy "stores-fd.cpy".
       copy "storeconf-fd.cpy".

       working-storage section.
       copy "common_ws.cpy".
 
       01 ws-best-dist-k           distance-type.
       01 ws-best-dist-m           distance-type.
       01 ws-best-dist-k-d         pic 999.999.
       01 ws-best-dist-m-d         pic 999.999.

       01 ws-best-store-info.
      $if storepopup-use-extended defined
       COPY "stores-extended.cpy" replacing ==:Prefix-:== by == ws-best-==.
      $else
       COPY "stores.cpy" replacing ==:Prefix-:== by ==ws-best-==.
      $end

       01 sf-country-id        pic xxx.

       01 geo-hash pic x(12).
       01 geo-hash-len binary-long.

       78 MAX-DISTANCE-IN-K value 40075.
       78 MAX-DISTANCE-IN-M value 24860.

       01 ws-current-dist-k distance-type.
       01 ws-current-dist-m distance-type.

       01 ws-popup-title PIC X(76).
       01 ws-popup-message-1 PIC X(76).
       01 ws-popup-message-2 PIC X(76).
       01 ws-popup-button-1 PIC X(7).

       01 lat lat-long-type.
       01 long lat-long-type.

       78 MAX-COUNTRIES        value 13.
       01 ws-country-counter   binary-long.
       01 ws-valid-countries   pic xxx occurs MAX-COUNTRIES values "au", "de" "dk" "es" "fr" "gb" "in" "it" "mx" "nl" "nz" "pt" "us".
       screen section.
      $set sourceformat"fixed"
       copy "welcome.ss".
       copy "storeinfo.ss".
       copy "locatestore.ss".
       copy "common_ss.cpy".
       copy "common_78.cpy".
      $set sourceformat"variable"
       procedure division.
           move "Locate Store " to Menu-Name
           move "LS01" to Menu-Id
           move spaces to Store-name

           move "gb" to sf-country-id


           move 51.402988 to lat
           move -1.322669 to long

           perform clr-screen
           display G-MENUHEADER
           display g-locatestore
           accept g-locatestore

           perform find-stores

           if return-code not equal 0
               perform show-error
               goback
           end-if

           if ws-best-id equal 0
               move "No store has been found" to ws-popup-message-1
               perform show-error
               goback
           else
              move ws-best-store-info to sf-Store-Information
           end-if

           *> store the distance to the best store
           move ws-best-dist-k to ws-best-dist-k-d
           move ws-best-dist-m to ws-best-dist-m-d

           *> tell the user how far away the best store is
           string "Distance to store is " delimited by size
               ws-best-dist-k-d delimited by size
               "KM (" delimited by size
               ws-best-dist-m-d delimited by size
               "M)" delimited by size
               into store-record-edit-message
           end-string
           display g-storeinfo

           perform press-any-key

           goback.

       find-stores section.
           perform validate-location
           if return-code not equal 0
               move "Invalid lat/long" to ws-popup-message-1
               exit section
           end-if.

           initialize ws-best-store-info
           move MAX-DISTANCE-IN-K to ws-best-dist-k
           move MAX-DISTANCE-IN-M to ws-best-dist-m
           if sf-country-id not equal spaces
               perform get-store-near
           else
               perform varying ws-country-counter from 1 by 1
                         until ws-country-counter > MAX-COUNTRIES
                            or return-code not = 0
                   move ws-valid-countries(ws-country-counter)
                     to sf-country-id
                   perform get-store-near
               end-perform
           end-if
           .

       validate-location section.
           move spaces to ws-popup-title

      *    Validate the supplied loction is valid
           initialize geo-hash
           move length of geo-hash to geo-hash-len
           call "geohashenc" using by reference lat
                                   by reference long
                                   by reference geo-hash-len
                                   by reference geo-hash
           end-call.

       get-store-near section.
           perform initialization
           if return-code = 0
               perform find-closest-store
           end-if.

       initialization section.
           call "autosetup"
           call "mfocal_init" using sf-country-id
           if return-code not equal 0
               move "mfocal_init" to ws-popup-message-1
               move "FAILED to open store-file" to ws-popup-message-1
           end-if.

       find-closest-store section.
           open input store-file

           move 0 to ws-current-dist-k ws-current-dist-m
           perform until exit

               read store-file next record
                   at end
                       exit perform
               end-read

               call "calcdistance" using by reference lat
                                         by reference long
                                         by reference sf-latitude
                                         by reference sf-longitude
                                         by reference ws-current-dist-k
                                         by reference ws-current-dist-m
               end-call

               if ws-current-dist-k < ws-best-dist-k
                   move ws-current-dist-k to ws-best-dist-k
                   move ws-current-dist-k to ws-best-dist-k-d
                   move ws-current-dist-m to ws-best-dist-m
                   move ws-current-dist-m to ws-best-dist-m-d
                   move sf-Store-Information to ws-best-store-info
               end-if
           end-perform

           close store-file

           exit section.

       call-errpopup section.
           call "errpopup" using ws-popup-title
                                 ws-popup-message-1
                                 ws-popup-message-2
                                 ws-popup-button-1
           end-call.
           exit section.

       show-error section.
           move "Unable to locate store" to ws-popup-title
           move "Ok" to ws-popup-button-1

           perform call-errpopup
           exit section.

       getstorenearerror section.
       entry "getstorenearerror".
           if ws-popup-title not equal spaces
               perform call-errpopup
           end-if
           move spaces to ws-popup-title
           exit section.


           copy "common.cpy".