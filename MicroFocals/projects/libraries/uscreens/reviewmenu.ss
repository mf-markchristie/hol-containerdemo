       01 G-REVIEWMENU.
         02 LINE 5 COL 2 VALUE "/=Review Appointments===================
      -"=====================================\".
         02 LINE 6 COL 2 VALUE "|
      -"                                     |".
         02 LINE 7 COL 2 VALUE "|         Review Appointments for ".
         02 COL 36 FOREGROUND-COLOR 6 HIGHLIGHT VALUE "c".
         02 COL 37 VALUE "onsultant...................  ".
         02 COL 67 FOREGROUND-COLOR 6 HIGHLIGHT VALUE "C".
         02 COL 79 VALUE "|".
         02 LINE 8 COL 2 VALUE "|
      -"                                     |".
         02 LINE 9 COL 2 VALUE "|         Review Appointments for ".
         02 COL 36 FOREGROUND-COLOR 6 HIGHLIGHT VALUE "a".
         02 COL 37 VALUE "ll consultants..............  ".
         02 COL 67 FOREGROUND-COLOR 6 HIGHLIGHT VALUE "A".
         02 COL 79 VALUE "|".
         02 LINE 10 COL 2 VALUE "|
      -"                                      |".
         02 LINE 11 COL 2 VALUE "|         Review Appointment availabili
      -"ty ".
         02 COL 44 FOREGROUND-COLOR 6 HIGHLIGHT VALUE "g".
         02 COL 45 VALUE "rid for consultant..  ".
         02 COL 67 FOREGROUND-COLOR 6 HIGHLIGHT VALUE "G".
         02 COL 79 VALUE "|".
         02 LINE 12 COL 2 VALUE "|
      -"                                      |".
         02 LINE 13 COL 2 VALUE "|         ".
         02 COL 12 FOREGROUND-COLOR 6 HIGHLIGHT VALUE "P".
         02 COL 13 VALUE "rint Daily Appointment grids..................
      -"....... ".
         02 COL 67 FOREGROUND-COLOR 6 HIGHLIGHT VALUE "P".
         02 COL 79 VALUE "|".
         02 LINE 14 COL 2 VALUE "|
      -"                                      |".
         02 LINE 15 COL 2 VALUE "\======================================
      -"======================================/".
         02 LINE 17 COL 15 VALUE "Menu Option :".
         02 LINE 17 COL 29 PIC X TO ra-menu-option AUTO.
