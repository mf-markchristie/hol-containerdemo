
      $set sourceformat"variable"
        working-storage section.
        copy "common_78.cpy".
        78 radius-of-earth value 6371.  *> radius of earth in KM
        78 km-per-mile value 1.609344.
        78 pi value 3.1415926535897932.

        01 deg2rad      comp-2.

        01 lat1            lat-long-type.
        01 lon1            lat-long-type.
        01 lat2            lat-long-type.
        01 lon2            lat-long-type.
     
        linkage section.
        01 lnk-lat1         lat-long-type.
        01 lnk-lat2         lat-long-type.
        01 lnk-lon1         lat-long-type.
        01 lnk-lon2         lat-long-type.
        01 lnk-distance     distance-type.
        01 lnk-distance-m   distance-type.

        procedure division using
                by reference lnk-lat1,
                by reference lnk-lon1,
                by reference lnk-lat2,
                by reference lnk-lon2,
                by reference lnk-distance,
                by reference lnk-distance-m.

           move lnk-lat1 to lat1
           move lnk-lon1 to lon1
           move lnk-lat2 to lat2
           move lnk-lon2 to lon2
           
           compute deg2rad = pi / 180
           compute lat1 = lat1 * deg2rad
           compute lon1 = lon1 * deg2rad
           compute lat2 = lat2 * deg2rad
           compute lon2 = lon2 * deg2rad

           move 0 to lnk-distance lnk-distance-m

           *> spherical law of cosines....
           compute lnk-distance = function acos(
                               function sin(lat1) * function sin(lat2)
                             + function cos(lat1) * function cos(lat2) * function cos (lon2 - lon1))
                             * radius-of-earth
           end-compute

           compute lnk-distance-m = lnk-distance / km-per-mile
           goback
       .