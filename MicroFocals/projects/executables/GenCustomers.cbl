
       identification division.
       program-id. gencustomers.
       environment division.
       input-output section.
       file-control.
             select tdf assign test-data-filename
             organization is line sequential
             access mode is sequential
             status is ws-file-status.

       copy "customer-fc.cpy".
       copy "stores-fc.cpy".

       copy "customer-fd.cpy".
       copy "stores-fd.cpy".

       fd tdf.
       01 tdf-rec		pic x(128).

       Working-storage section.
       copy "common_ws.cpy".
       01 ws-last-attribute     pic x(32).

       01 ws-name-count         binary-long.
       01 ws-surname-count      binary-long.

       01 ws-last-sex		    pic x.
       01 ws-last-name		    pic x(60).
       01 ws-last-surname	    pic x(60).
       01 ws-last-streetname	pic x(70).
       01 ws-last-county        pic x(70).
       01 ws-last-postcode      pic x(70).
       01 ws-last-region        pic x(70).
       01 ws-last-town          pic x(70).
       01 ws-last-occupation    pic x(40).

       01 ws-country		    pic x(40).
       01 ws-prefstoreid	    pic 99999.

       78 MAX-NAMES		        value 33540.
       78 MAX-SURNAMES		    value 1100.
       78 MAX-STREETNAMES	    value 100.
       78 MAX-COUNTY            value 50.
       78 MAX-POSTCODES         value 5000.
       78 MAX-OCCUPATIONS       value 200.

       01 ws-names-count        binary-long.
       01 ws-male-name          pic x(60) occurs MAX-NAMES.
       01 ws-female-name        pic x(60) occurs MAX-NAMES.
       01 ws-street-names	    pic x(70) occurs MAX-STREETNAMES.
       01 ws-county-names       pic x(70) occurs MAX-COUNTY.

       01 ws-postcode-map       occurs MAX-POSTCODES.
          03 ws-postcode        pic x(70).
          03 ws-region          pic x(70).
          03 ws-town            pic x(70).

       01 ws-occupations        pic x(40) occurs MAX-OCCUPATIONS.

       01 ws-stn-count          binary-long.
       01 ws-sur-count          binary-long.
       01 ws-cnt-count          binary-long.
       01 ws-ptn-count          binary-long.
       01 ws-occ-count          binary-long.

       01 ws-male-count		    binary-long.
       01 ws-female-count	    binary-long.

       01 ws-surname            pic x(60) occurs MAX-SURNAMES.

       01 ws-first-time         pic 9(1) value 0.
       01 ws-rnd-seed-x         pic x(8).
       01 ws-rnd-seed-9         redefines ws-rnd-seed-x
                                  pic 9(8).

       01 split-area-for-fullname pic x(60) occurs 10.

       01 ws-rnd-dbl            comp-2.

       *> titles are cut-down but enough for what we need
       01 male-titles pic x(8) occurs 6 values
                "Mr", "Master", "Sir", "Lord"
                "Dr", "Prof".

       01 female-titles pic x(8) occurs 6 values
                "Ms", "Miss", "Mrs",
                "Madam", "Lady", "Ms".

       01 ws-stn-z          pic 9999.
       01 ws-htel-prefix    pic 99999.
       01 ws-wtel-prefix    pic 99999.
       01 ws-hrand-tel      pic 999999.
       01 ws-wrand-tel      pic 999999.

       *> Random counters
       01 ws-name-rand      pic xxxx comp-x.
       01 ws-sur-rand       pic xxxx comp-x.
       01 ws-stn-rand       pic xxxx comp-x.
       01 ws-stnz-rand      pic xxxx comp-x.
       01 ws-add-rand       pic xxxx comp-x.
       01 ws-town-rand      pic xxxx comp-x.
       01 ws-hrand          pic xxxx comp-x.
       01 ws-wrand          pic xxxx comp-x.
       01 ws-occ-rand       pic xxxx comp-x.

       01 ws-res-dob        pic xxxx comp-x.

       01 date-today-temp                        pic x(8).
       01 date-today redefines date-today-temp.
         03 dt-yyyy      pic xxxx.
         03 dt-mm        pic xx.
         03 dt-dd        pic xx.

       01 ws-to-generate-count pic 9(7).

       01 ws-left-counter      pic 9(9).

       01 ws-homeemail-dom     pic x(20) value "homeemail.com".
       01 ws-workemail-dom     pic x(20) value "workemail.com".
       01 ws-gender            pic x value "M".

       01 cmd-line             pic x(32).

       screen section.
       copy "gencustomers.ss".
       copy "common_ss.cpy".
       Procedure division .

           *> setup the environment
            perform init-env


           *> pickup the command line and use it as the
           *> filename... if not assume is in gb..
           move spaces to test-data-filename
           accept cmd-line from command-line
           if cmd-line equal spaces
            move "TestData_gb.xml" to test-data-filename
           else
            move cmd-line to test-data-filename
           end-if

           *> COBOL's random seed needs to setup
           perform setup-random-seed

           *> read the names from the xml file
           perform read-names

           *> Ensure the xml looks okay..
           if ws-country equal spaces or
             ws-htel-prefix equal spaces or
             ws-wtel-prefix equal spaces
             or ws-names-count < 16
             or ws-sur-count < 16
             or ws-male-count < 16
             or ws-female-count < 16

            display "ERROR: XML does not include either"
                      " country/htel or wtel prefix"
            display "       or the data itself is sparse"
            display " "
            display "      Please fix and retry.."

            exhibit named ws-names-count
            exhibit named ws-sur-count

            exhibit named ws-male-count
            exhibit named ws-female-count

            exhibit named ws-ptn-count
            exhibit named ws-occ-count

            stop run
           end-if

           perform clr-screen
           display g-gencustomers
           accept g-gencustomers

           open i-o cust-file
           if ws-file-status not equals "00"
            close cust-file
            open output cust-file
            perform check-file-status
           end-if

           *> Show an indicator on the screen
           display "Generating customer id : " at 1510
           display "Records left to write  : " at 1610
           move ws-to-generate-count to ws-left-counter
           perform ws-to-generate-count times
             move spaces to f-customerinformation
             call "gencustid" using f-Customer-Id
             display f-Customer-Id at 1535
             display ws-left-counter at 1635
             subtract 1 from ws-left-counter

             *> setup customer since date
             accept date-today-temp from date YYYYMMDD
             move dt-mm to f-cs-mm
             move dt-dd to f-cs-dd

             move 1 to f-dob-mm f-dob-dd
             move 1960 to f-dob-yyyy

             move "N" to f-alert

             move "Y" to f-Diabetic-retinopathy
             move "Y" to f-Glaucoma
             move "Y" to f-Cataracts
             move "Y" to f-Colour-blindness

             if ws-gender equals "m" or "M"
              perform with test after
               until ws-male-name(ws-name-rand) not equal spaces
                perform with test after until ws-name-rand not equal 0
                 compute ws-name-rand = 1 +
                    (function random() * ws-male-count)
                end-perform
              end-perform
             else
              perform with test after
               until ws-female-name(ws-name-rand) not equal spaces
                perform with test after until ws-name-rand not equal 0
                 compute ws-name-rand = 1 +
                    (function random() * ws-female-count)
                end-perform
              end-perform

             end-if

             *> generate random dob 1960 + random(40)
             perform with test after until ws-res-dob not equal 0
              compute ws-res-dob = 1 + (function random() * 40)
             end-perform
             add ws-res-dob to f-dob-yyyy

             perform with test after until ws-sur-rand not equal 0
              compute ws-sur-rand = 1 +
                            (function random() * ws-sur-count)
             end-perform

             perform with test after until ws-stn-rand not equal 0
              compute ws-stn-rand = 1 +
                            (function random() * ws-stn-count)
             end-perform

             perform with test after until ws-stnz-rand not equal 0
              compute ws-stnz-rand = 1 + (function random() * 1000 )
             end-perform

             perform with test after until ws-add-rand not equal 0
              compute ws-add-rand = 1 + (function random()
                                        * ws-cnt-count )
             end-perform

             perform with test after until ws-town-rand not equal 0
              compute ws-town-rand = 1 + (function random()
                                        * ws-ptn-count )
             end-perform

             perform with test after until ws-hrand not equal 0
              compute ws-hrand = 1 + (function random() * 999999)
             end-perform

             perform with test after until ws-wrand not equal 0
              compute ws-wrand = 1 + (function random() * 999999)
             end-perform

             perform with test after until ws-occ-rand not equal 0
              compute ws-occ-rand = 1 +
                         (function random() * ws-occ-count)
             end-perform


             if ws-gender equals "m" or "M"
              string ws-male-name(ws-name-rand) delimited by "  "
                 " " delimited by size
                ws-surname(ws-sur-rand) into f-fullname

             string ws-male-name(ws-name-rand)
                delimited by "  "
                "." delimited by size
                ws-surname(ws-sur-rand) delimited by "  "
                "@" delimited by size
                ws-workemail-dom delimited by space
                 into f-work-email
            else
              string ws-female-name(ws-name-rand) delimited by "  "
                 " " delimited by size
                ws-surname(ws-sur-rand) into f-fullname

             string ws-female-name(ws-name-rand)
                delimited by "  "
                "." delimited by size
                ws-surname(ws-sur-rand) delimited by "  "
                "@" delimited by size
                ws-workemail-dom delimited by space
                 into f-work-email
            end-if

             move ws-stnz-rand to ws-stn-z
             string ws-stn-z
                " " delimited by size
                ws-street-names(ws-stn-rand)  delimited by "   "
                into f-address(1)

             move ws-town(ws-town-rand) to f-address(2)
             move ws-region(ws-town-rand) to f-address(3)
             move ws-county-names(ws-add-rand) to f-address(4)
             move ws-postcode(ws-town-rand) to
                f-postcode of f-customerinformation
             move ws-occupations(ws-occ-rand) to f-occupation

             perform gen-initials

             string f-initials delimited by "  "
                "@" delimited by size
                 ws-homeemail-dom delimited by space
                into f-home-email

             move ws-gender to f-Gender
             move "N" to f-Deceased

             compute ws-name-rand = 1 + (function random() * 6)

             if ws-gender equals "m" or "M"
              move male-titles(ws-name-rand) to f-title
             else
              move female-titles(ws-name-rand) to f-title
             end-if

             move ws-country to f-country

             move ws-hrand to ws-hrand-tel
             string ws-htel-prefix "-" ws-hrand-tel into f-home-tel

             move ws-wrand to ws-wrand-tel
             string ws-wtel-prefix "-" ws-wrand-tel into f-work-tel

             move ws-prefstoreid to f-Preferred-Store-Id

             write f-CustomerInformation
             perform check-file-status

             *> remove the current name
             if ws-gender equals "m" or "M"
              move spaces to ws-male-name(ws-name-rand)
             else
              move spaces to ws-female-name(ws-name-rand)
             end-if
            end-perform

           display ws-left-counter at 1635

           close cust-file

           goback
        .

       gen-initials.
              unstring f-fullname
                delimited by spaces into
                    split-area-for-fullname(1)
                    split-area-for-fullname(2)
                    split-area-for-fullname(3)
                    split-area-for-fullname(4)
                    split-area-for-fullname(5)
                    split-area-for-fullname(6)
                    split-area-for-fullname(7)
                    split-area-for-fullname(8)
                    split-area-for-fullname(9)
                    split-area-for-fullname(10)
              end-unstring

              move split-area-for-fullname(1) to f-initials(1:1)
              move split-area-for-fullname(2) to f-initials(2:1)
              move split-area-for-fullname(3) to f-initials(3:1)
              move split-area-for-fullname(4) to f-initials(4:1)
              move split-area-for-fullname(5) to f-initials(5:1)
              move split-area-for-fullname(6) to f-initials(6:1)
              move split-area-for-fullname(7) to f-initials(7:1)
              move split-area-for-fullname(8) to f-initials(8:1)
        .

       read-names.
           move 0 to ws-surname-count ws-name-count
             ws-names-count ws-sur-count
             ws-male-count ws-female-count
             ws-stn-count
             ws-cnt-count
             ws-ptn-count
             ws-occ-count

           open input tdf
           if ws-file-status not equals "00"
             display "ERROR: read error on file"
             display  test-data-filename
             perform check-file-status
             stop run
           end-if
           read tdf
           XML PARSE tdf-rec PROCESSING PROCEDURE xml-pc-handler
              ON EXCEPTION
                  display 'XML document error ' XML-CODE
              NOT ON EXCEPTION
                  display 'XML document successfully parsed'
           END-XML
           close tdf
           .

       setup-random-seed.
           move 32768 to ws-rnd-seed-9
           perform until ws-rnd-seed-9 < 32768
              accept ws-rnd-seed-x from time
              move function reverse(ws-rnd-seed-x) to ws-rnd-seed-x
              compute ws-rnd-seed-9 = ws-rnd-seed-9 / 3060
           end-perform
           compute ws-rnd-dbl = function random(ws-rnd-seed-9)
           move 1 to ws-first-time
       .

        xml-pc-handler.
            evaluate XML-EVENT
                when 'ATTRIBUTE-NAME'
                    move xml-text to ws-last-attribute
                when 'ATTRIBUTE-CHARACTERS'
                    perform handle-xml-characters
               when 'END-OF-INPUT'
                  read tdf
                  if ws-file-status equals "00"
                    move 1 to xml-code
                  end-if
               when 'END-OF-ELEMENT'
                   perform handle-xml-end
           end-evaluate
           .

        handle-xml-end.
            if ws-last-name not equal spaces
              add 1 to ws-names-count
              if ws-last-sex equals "m" or ws-last-sex equals "M"
                  add 1 to ws-male-count
                 move ws-last-name to ws-male-name(ws-male-count)
              end-if

               if ws-last-sex equals "f" or ws-last-sex equals "F"
                  add 1 to ws-female-count
                  move ws-last-name to
                        ws-female-name(ws-female-count)
              end-if
            end-if

            if ws-last-surname not equal spaces
                add 1 to ws-sur-count
                move ws-last-surname to
                    ws-surname(ws-sur-count)
            end-if

            if ws-last-streetname not equal spaces
                add 1 to ws-stn-count
                move ws-last-streetname to
                    ws-street-names(ws-stn-count)
            end-if

            if ws-last-county not equal spaces
                add 1 to ws-cnt-count
                move ws-last-county to
                    ws-county-names(ws-cnt-count)
            end-if

            if ws-last-postcode not equal spaces
                add 1 to ws-ptn-count
                move ws-last-postcode to ws-postcode(ws-ptn-count)
                move ws-last-town to ws-town(ws-ptn-count)
                move ws-last-region to ws-region(ws-ptn-count)
            end-if


            if ws-last-occupation not equal spaces
                add 1 to ws-occ-count
                move ws-last-occupation to
                    ws-occupations(ws-occ-count)
            end-if

            move spaces to ws-last-attribute
                    ws-last-surname
                    ws-last-sex
                    ws-last-name
                    ws-last-streetname
                    ws-last-county
                    ws-last-postcode
                    ws-last-region
                    ws-last-town
                    ws-last-occupation
                   .

        handle-xml-characters.
            evaluate ws-last-attribute
                when 'SurName'
                    inspect xml-text converting
                       'ABCDEFGHIJKLMNOPQRSTUVWXYZ' TO
                              'abcdefghijklmnopqrstuvwxyz'
                    inspect xml-text(1:1) converting
                               'abcdefghijklmnopqrstuvwxyz' TO
                               'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                    move xml-text to ws-last-surname
                when 'HomeTelPrefix'
                    move function numval(xml-text) to
                        ws-htel-prefix
                when 'WorkTelPrefix'
                    move function numval(xml-text) to
                        ws-wtel-prefix
                when 'PrefStoreId'
                    move function numval(xml-text) to
                        ws-prefstoreid
                when 'CountryName'
                    move xml-text to ws-country
                when 'Occupation'
                    move xml-text to ws-last-occupation
                when 'region'
                    move xml-text to ws-last-region
                when 'town'
                    move xml-text to ws-last-town
                when 'postcode'
                    move xml-text to ws-last-postcode
                when 'County'
                    move xml-text to ws-last-county
                when 'StreetName'
                    move xml-text to ws-last-streetname
                when 'Name'
                    move xml-text to ws-last-name
                when 'Sex'
                    move xml-text to ws-last-sex
                when "NameCount"
                    move function numval(xml-text)
                        to ws-name-count
                when "SurNameCount"
                    move function numval(xml-text)
                        to ws-surname-count
*>                when other
*>                    display ws-last-attribute " -> " xml-text
            end-evaluate
            .

        copy "common.cpy".




