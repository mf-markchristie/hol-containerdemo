      $set remove"address"
      $set remove"title"
       program-id. custread.
       environment division.
       special-names.
         crt status is key-status.
       file-control.
       copy "customer-fc.cpy"   .

       data division.
       fd cust-file.
       01 f-CustomerInformation.
       copy "customerinfo.cpy" replacing ==:Prefix-:== by ==f-==.

       WORKING-STORAGE SECTION.
       01 CustomerInformation.
       COPY "customerinfo.cpy" replacing ==:Prefix-:== by == ==.
       COPY "common_ws.cpy".


       01 date-today-temp                        pic x(8).
       01 date-today redefines date-today-temp.
         03 dt-yyyy      pic xxxx.
         03 dt-mm        pic xx.
         03 dt-dd        pic xx.

        01 gender-prompt  pic x value "M".
       01 spaces10       pic x(10) value spaces.
       01 x79            pic x(79) value all "-".
       01 xline          pic x(60).
       01 eof            pic x.
       01 invalid-mess   pic x(40).
       01 GetOS-block          usage cblt-os-info-params.
       01 status-code          usage cblt-rtncode.
       01 counter        binary-long.
       01 prev-cust-name   pic x(60).
       01 cust-name       pic x(60).
       01 cust-name-len  binary-long.
       linkage section.
       copy "common_lnk.cpy".
       SCREEN SECTION.
       COPY "CUSTADD.ss".
       copy "common_ss.cpy".
       PROCEDURE DIVISION.
          initialize cust-name prev-cust-name
          accept cust-name from command-line
          call "autosetup"

          move 0 to cust-name-len
          inspect function reverse(cust-name)
               tallying cust-name-len for leading spaces
          compute cust-name-len = length of cust-name - cust-name-len
          if cust-name-len equal 0
                display "No command line given..."
                stop run
          end-if

          open i-o cust-file with lock
          perform check-file-status
          move "n" to eof
          move 1 to counter
          move 0 to f-Customer-Id
          move cust-name to f-fullname
          move cust-name to prev-cust-name
          start cust-file
            key >= f-fullname
             invalid key
              perform check-file-status
              move "y" to eof
          end-start

          perform with test after
                until eof equals "y"

            move f-fullname to prev-cust-name
            move all spaces to f-CustomerInformation
            read cust-file next record
              at end
                move "y" to eof
            end-read

            if f-fullname(1:cust-name-len) not equal
                    cust-name(1:cust-name-len)
                move "y" to eof
            end-if

            if eof equals "n"
              move all spaces to xline
              perform display-record
              add 1 to counter
            end-if
          end-perform
          close cust-file

          GOBACK.

       .

       display-record.
           display f-Customer-Id " -> " with no advancing
           string *> f-title delimited by "   "
                     " " delimited by size
                     f-FullName delimited by "   "
                     " ("
                      f-initials delimited by "  "
                      ") ["
                     f-lc-FullName delimited by "   "
                     "]"
               into xline
           display xline
           *> display spaces10 "Deceased : " f-deceased
           *> display spaces10 "Gender   : " f-gender
           *> display spaces10 "Address  : " f-address(1)
           *> display spaces10 "         : " f-address(2)
           *> display spaces10 "         : " f-address(3)
           *> display spaces10 "         : " f-address(4)
           *> display spaces10 "Postcode : " f-postcode
           *> display spaces10 "Country  : " f-country
           *> display spaces10 "DOB      : " f-dob
           *> display spaces10 "CustSince: " f-customer-since
           *> display spaces10 "H-Email  : " f-home-email
           *> display spaces10 "H-Tel    : " f-home-tel
           *> display spaces10 "W-Email  : " f-work-email
           *> display spaces10 "W-Tel    : " f-work-tel
           *> display spaces10 "GP Name  : " f-gp-name
           *> display spaces10 "Store Id : " f-Preferred-Store-Id
           *> display spaces10 "Occupation: " f-occupation
              .


       copy "common.cpy".

       end program CustRead.
