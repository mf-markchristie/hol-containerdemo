       Identification division.
       Program-id. cclookup.
       environment division.
       input-output section.
       file-control.
             select f1 assign 'CountryPhoneCodes.xml'
             organization is line sequential
             access mode is sequential
             file status is f1-stat.
       Data division.

       file section.
       fd f1.
       01 f1-rec		     pic x(128).

       Working-storage section.
       01 xml-document-length pic 999.
       01 F1-STAT.
           03 f1-stata  pic x.
           03 f1-statb  pic X.

       01 last-attribute     pic x(10).

       01 ws-country-code	 pic xx.
       01 ws-phone-code      pic xx.
       01 ws-name		     pic x(60).
       linkage section.
       01 lnk-country-code	 pic xx.
       01 lnk-phone-code     pic xx.
       01 lnk-name		     pic x(60).

       Procedure division using
            by reference lnk-country-code
            by reference lnk-phone-code
            by reference lnk-name
            .

       mainline section.
           open input f1
           read f1
           XML PARSE f1-rec PROCESSING PROCEDURE xml-pc-handler
              ON EXCEPTION
                  move spaces to lnk-phone-code lnk-name
           END-XML

           close f1
           goback.

       xml-pc-handler section.
           evaluate XML-EVENT
                when 'ATTRIBUTE-NAME'
                    move xml-text to last-attribute
                when 'ATTRIBUTE-CHARACTERS'
                    evaluate last-attribute
                       when "name"
                          move xml-text to ws-name
                       when "code"
                          move xml-text to ws-country-code
                       when "phoneCode"
                          move xml-text to ws-phone-code
                   end-evaluate
                when 'END-OF-INPUT'
                  read f1
                  if F1-STAT equals "00"
                    move 1 to xml-code
                  end-if
                when 'END-OF-ELEMENT'
                    if lnk-country-code equals ws-country-code
                       move ws-phone-code to lnk-phone-code
                       move ws-name to lnk-name
                    end-if
                    move spaces to last-attribute
           end-evaluate
           .




