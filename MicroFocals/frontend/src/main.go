package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
)

var ServiceToCall = "vc80-mfocal-svc-findstore"
var ip = ""
const form = `
<html>
<!--
	Eventually by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<head>
    <title>Micro Focal Mythical Optician Demo</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="assets/css/main.css" />
</head>
<body class="is-preload"> 
    <div id="main">
        <div>
            <!-- Header -->
            <header id="header">
                <h1>Micro Focals Demonstration Opticians</h1>
				<h2>A simple website based on a mythical opticians</h2>
                <p>
                    Simple demonstration web page which invokes a COBOL routine packaged as an IMTK REST web service 
					hosted within Micro Focus Enterprise Server instances running in a Kubernetes cluster. The routine
					searches COBOL data files to find the optician store closest to the specified geographic location,
					either globally or within the specified country.
                </p>
            </header>
        </div>
        <div>
			<button id = "find-me">Get my location</button><br/>
            <form id="storfinder-form" method="post" action="find">
                <input type="text" name="Latitude" id="latitude" placeholder="Latitude" />
                <input type="text" name="Longitude" id="longitude" placeholder="Longitude" />
				<select name="Country" id="country">
					<option value="  " selected>Anywhere</option>
				    <option value="au">Australia</option>
					<option value="dk">Denmark</option>
					<option value="fr">France</option>
					<option value="de">Germany</option>
					<option value="gb">Great Britain</option>
					<option value="in">India</option>
					<option value="it">Italy</option>
					<option value="mx">Mexico</option>
					<option value="nl">Netherlands</option>
					<option value="nz">New Zealand</option>
					<option value="pt">Portugal</option>
 				    <option value="es">Spain</option>
					<option value="us">USA</option>
				</select>
                <input type="submit" value="Find Closest Store" />
            </form>
			<p id = "status"></p>
        </div>
        <div>
            <div ng-app="myApp" ng-hide="hidestoredetails">
                <div ng-controller="myCtrl as bio">
                    <div class="row">
                        <div class="col-md-3 col-md-offset-1">
                            <div class="well">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <p class="panel-title">%v</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div id="footer">
                <ul class="icons">
                    <li><a href="https://twitter.com/microfocus" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                    <li><a href="https://www.facebook.com/MicroFocusCorp" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                    <li><a href="https://www.linkedin.com/company/micro-focus" class="icon fa-linkedin"><span class="label">Linkedin</span></a></li>
          <!--          <li><a href="mailto:Guy.Sofer@MicroFocus.com" class="icon fa-envelope-o"><span class="label">Email</span></a></li> -->
                </ul>
                <ul class="copyright">
                    <li>&copy; 2020 Micro Focus.</li>
                    <li>Credits: <a href="http://html5up.net">HTML5 UP</a></li>
                </ul>
            </div>
        </div>
    </div>    
    <!-- Footer -->
   

    <!-- Scripts -->
	<script>
	function geoFindMe() {
		const status = document.querySelector('#status');
		status.textContent = 'Finding location...';

		var xhttp = new XMLHttpRequest();
		xhttp.open('GET', 'https://ipapi.co/json/');
        xhttp.addEventListener('load', getIP);
		xhttp.send();
	}

	 function getIP() {
		var json = JSON.parse(this.responseText);
		if (json.latitude && json.longitude && json.city) {
			const status = document.querySelector('#status');
			const latitudeText = document.querySelector('#latitude');
			const longitudeText = document.querySelector('#longitude');

			if (json.city && json.country_name) {
				status.textContent = 'Found location from IP address: ' + json.city + ", " + json.country_name;
			}
			else {
				status.textContent = 'Found location from IP address';
			}
			latitudeText.value = json.latitude;
			longitudeText.value = json.longitude;
	   }
	   else{
			status.textContent = 'Unable to retrieve your location';
	   }
  }
	document.querySelector('#find-me').addEventListener('click', geoFindMe);

	</script>
    <script src="assets/js/main.js"></script>
</body>
</html>
`

func main() {

    /* Allow for alternative store finder service DNS name/port */
	potentialservice := os.Getenv("FINDSTORESERVICE_NAME")
	if potentialservice != "" {
		ServiceToCall = potentialservice
	}

	/* Start the web server */
	http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("./assets"))))
	http.Handle("/images/", http.StripPrefix("/images/", http.FileServer(http.Dir("./images"))))
	http.HandleFunc("/find", handler)
	http.HandleFunc("/", view)
	
    log.Fatal(http.ListenAndServe(":8080", nil))
}

func view(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, form, "")
}

/* Web service input structure as created by IMTK */
type WS_REQ_STRUCT struct {
	BRE_I_BRE_WRAP_WS_FI string
	BRE_I_SF_COUNTRY_ID string
	BRE_I_LAT float32
	BRE_I_LONG float32
}
/* Web service output structure as created by IMTK */
type WS_RESP_STRUCT struct {
	BRE_O_WS_BEST_DIST_K float32
	BRE_O_WS_BEST_DIST_M float32
	BRE_O_WS_BEST_STORE struct {
		BRE_O_WS_BEST_ID1 int
		BRE_O_WS_BEST_NAME_O string
		BRE_O_WS_BEST_PROVIN string
		BRE_O_WS_BEST_COUNTY string
		BRE_O_WS_BEST_POSTCO string
		BRE_O_WS_BEST_EMAIL string
		BRE_O_WS_BEST_LOCATI struct {
			BRE_O_WS_BEST_LATITU float32
			BRE_O_WS_BEST_LONGIT float32
		}
		BRE_O_WS_BEST_TEL string
		BRE_O_WS_BEST_GEOHAS string
		BRE_O_WS_BEST_CONSUL struct {
			BRE_O_WS_BEST_CONSUL0[16] int
		}
	}
	BRE_O_WS_POPUP_MESSA string
	BRE_O_BRE_WRAP_WS_FI string
}

func handler(w http.ResponseWriter, r *http.Request) {
	/* Get the web service input values from the request */
	latitudeText := r.FormValue("Latitude");
	latitude, _ := strconv.ParseFloat(latitudeText, 32)
	longitudeText := r.FormValue("Longitude")
	longitude, _ := strconv.ParseFloat(longitudeText, 32)
	country := r.FormValue("Country")
	
	/* Create the web service structure with values from the web page */
	var findStoreInput WS_REQ_STRUCT
	findStoreInput.BRE_I_LONG = float32(longitude)
	findStoreInput.BRE_I_LAT = float32(latitude)
	findStoreInput.BRE_I_SF_COUNTRY_ID = country
	
	/* Invoke the IMTK web service running in an Enterprise Server */
	jsonStore, _ := json.Marshal(findStoreInput)
	var jsonStoreReader io.Reader = bytes.NewReader(jsonStore)
	resp, err := http.Post("http://"+ServiceToCall+"/MicroFocals/FindStoreService/1.0/FINDSTORE", "application/json", jsonStoreReader)
	
	/* Handle any errors */
	if err != nil {
		fmt.Fprintf(w, form, err)
		return
	}
	if resp.StatusCode != http.StatusOK {
		fmt.Fprintf(w, form, "1 Error response=" +resp.Status)
		return
	}

	/* Get the result of the search */
	var record WS_RESP_STRUCT
	if err := json.NewDecoder(resp.Body).Decode(&record); err != nil {
		resp.Body.Close()
		fmt.Fprintf(w, form, err)
		return
	}
	resp.Body.Close()
	
	/* Display the result to the user */
	if country == "  " {
		storeResult := fmt.Sprintf("Closest store to %v/%v globally is %v at %.2f km", latitudeText, longitudeText, record.BRE_O_WS_BEST_STORE.BRE_O_WS_BEST_NAME_O, record.BRE_O_WS_BEST_DIST_K)
		fmt.Fprintf(w, form, storeResult)
	} else {
	    countryCode := [13]string {"au","dk","fr","de","gb","in","it","mx","nl","nz","pt","es","us"}
		countryName := [13]string{"Australia","Denmark","France","Germany","Great Britain","India","Italy","Mexico","Netherlands","New Zealand","Portugal","Spain","USA"}
		var i int
		for i = 0; i<13 && countryCode[i] != country ; i++ {
			// loop until found
		}
		
		
		storeResult := fmt.Sprintf("Closest store to %v/%v within %v is %v at %.2f km", latitudeText, longitudeText, countryName[i], record.BRE_O_WS_BEST_STORE.BRE_O_WS_BEST_NAME_O, record.BRE_O_WS_BEST_DIST_K)
		fmt.Fprintf(w, form, storeResult)
	}
}
