# Create the base image with needed dependencies
FROM microfocus/cobolserver:sles15.1_8.0_x64 as base
ARG ES_SERVER=SERVICE
ENV ES_SERVER $ES_SERVER
ARG CCITCP2_PORT=34570
ENV CCITCP2_PORT $CCITCP2_PORT

# Install helpful utils and rsyslog to get "logger" command
RUN zypper install -y hostname curl rsyslog

# Build the application
FROM microfocus/vcdevhub:sles15.1_8.0_x64 as BuildMicroFocals
LABEL stage=intermediate
COPY projects /MicroFocals/Projects/
RUN . $MFPRODBASE/bin/cobsetenv $MFPRODBASE && \
	cd /MicroFocals/Projects/libraries && \
	$COBDIR/remotedev/ant/apache-ant-1.9.9/bin/ant -lib $COBDIR/lib/mfant.jar  -logger com.microfocus.ant.CommandLineLogger -f .cobolBuild

FROM base as publish
# Create directories
RUN mkdir -p /home/esadm/deploy/Logs /home/esadm/ctflogs
# Copy application binary files
COPY --from=BuildMicroFocals /MicroFocals/bin/*.so /home/esadm/deploy/loadlib/

# Copy scripts used to start and stop the container
COPY System/startserver.sh /home/esadm/deploy/
COPY System/vaultconfig.sh /home/esadm/deploy/
COPY System/pre-stop.sh /home/esadm/deploy/
# Copy the region configuration
COPY System/${ES_SERVER}.xml /home/esadm/deploy/
# Copy trace configuration file so it is available if needed
COPY System/ctf.cfg /home/esadm/
# Enable audit logging
COPY System/audit.cfg $MFPRODBASE/bin/audit.cfg

# Change permissions of copied files prior to switching from root
RUN chmod +x /home/esadm/deploy/vaultconfig.sh && \
    chmod +x /home/esadm/deploy/startserver.sh && \
    chmod +x /home/esadm/deploy/pre-stop.sh && \
    chown -R esadm /home/esadm && \
    chgrp -R esadm /home/esadm

# Import the region definition into the MFDS directory along with the web service deployment descriptor
RUN /bin/sh -c '. $MFPRODBASE/bin/cobsetenv && \
	/bin/sh -c "$COBDIR/bin/mfds64 &" && \
	/bin/sh -c "while ! mdump -a 127.0.0.1 -p $CCITCP2_PORT -s 1 -v 2 > /dev/null; do sleep 1; done; "&& \
	$COBDIR/bin/mfds64 -g 5 /home/esadm/deploy/$ES_SERVER.xml S && \
	mv /var/mfcobol/logs/journal.dat /var/mfcobol/logs/import_journal.dat	'

# Ensure credentials are accessed from the secrets vault
ENV MFDS_USE_VAULT Y

# Make the Linux container compatible with Windows style IDX filenames used by BANKDEMO
COPY System/extfh.cfg /home/esadm/deploy/
ENV EXTFH /home/esadm/deploy/extfh.cfg

# Application expects LANG=C to ensure character encoding is correct
ENV LANG C

# Swap away from being the root user so Enterprise Server is not running with elevated privileges  
USER esadm

WORKDIR "/home/esadm/deploy"

EXPOSE $CCITCP2_PORT 9005 10086

HEALTHCHECK --interval=30s CMD ps -eaf | grep casmgr | grep -v grep || exit 1

ENTRYPOINT ["/home/esadm/deploy/startserver.sh"]
