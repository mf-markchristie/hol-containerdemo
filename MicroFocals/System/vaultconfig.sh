#!/bin/bash

. $MFPRODBASE/bin/cobsetenv

echo Setting up the vault
# Setup ESM LDAP password
$COBDIR/bin/mfsecretsadmin write microfocus/MFDS/ESM/1.2.840.5043.14.001.1573468236.2-LDAPPwd $LDAP_PASSWORD
# Setup ES admin credentials
$COBDIR/bin/mfsecretsadmin write microfocus/CAS/casstart-user $ES_USERNAME
$COBDIR/bin/mfsecretsadmin write microfocus/CAS/casstart-password $ES_PASSWORD
