Purpose:
Docker container which runs MFDS and ESCWA, imports the specified region .xml file into MFDS and then exports it each time a change is made. 
Connect your browser to either the exposed MFDS (86) or ESCWA (10086) ports and change the regions settings.
N.B. this does not allow you to alter CICS resources and expects to work on regions previously exported with the "mfds -x 5" command, it does import/export security settings.

Build with:
docker build -t mfdseditor -f Dockerfile.editor .

Run with:

docker run --rm -v <input directory>:/regionin -v <output directory>/regionout:/regionout -p :86 -p :10086 mfdseditor <regionname>
where:
<input directory> contains <regionname>.xml
<output directory> is a pre-existing directory into which the changed regionname.xml is saved