apiVersion: apps/v1
kind: Deployment
metadata:
  name: microfocus-escwa
spec:
  replicas: 1
  selector:
    matchLabels:
      app: microfocus-escwa
  template:
    metadata:
      labels:
        app: microfocus-escwa
    spec:
      serviceAccountName: pod-access-sa
      nodeSelector:
        "beta.kubernetes.io/os": linux
      securityContext:
        runAsUser: 500
      containers:
      - name: application
        image: <registry_url>/escwa:latest
        args: [
               # Configure ESCWA to dynamically include PODS with the label matching the selector (see K8S documentation https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/#api)
               '--K8sConfig={"Direct":false,"Port":"8001","LabelEnvironment":false,"Label":"escwa%3Ddiscover","Namespace":"default"}',
               # Configure ESCWA to include the Scale Out Repository ed80-bnkd-vc-redis
               '--SorList=[{"SorName":"BankDemo","SorDescription":"Default PAC instance","SorType":"redis","SorConnectPath":"ed80-bnkd-svc-redis:6379","Uid" : "1"}]',
               # Configure ESCWA to remove the default localhost directory server
               '--MfdsList=[]',
               # Configure use of OpenLDAP running at es-svc-openldap for access control
               '--EsmList=[{"Name":"OpenLDAP","Module":"mldap_esm","ConnectionPath":"ldap:\/\/es-svc-openldap:389","AuthorizedID":"cn=admin,dc=example,dc=org","Enabled":true,"CacheLimit":1024,"CacheTTL":600,"Config":"[LDAP]\nprovider=libldap_r-2.4.so.2\nBase=DC=example,DC=org\nuser class=microfocus-MFDS-User\nuser container=CN=Enterprise Server Users,CN=Micro Focus\ngroup container=CN=Enterprise Server User Groups,CN=Micro Focus\nresource container=CN=Enterprise Server Resources,CN=Micro Focus","Description":"Test LDAP","Uid":"$(ACTIVE_ESM_ID)"}]',
               '--SecurityConfig.ActiveEsms=["$(ACTIVE_ESM_ID)"]',
               # Configure ESCWA to use TLS
               '--BasicConfig.MfRequestedEndpoint=tcpssl:localhost:10086',
               '--BasicConfig.MfSSLCertificateLocation=/home/esadm/certs/EscwaCertificate.pem',
               '--BasicConfig.MfSSLKeyfile=/home/esadm/certs/EscwaKey.pem',
               '--BasicConfig.MfTLSProtocols=+ALL',
               '--BasicConfig.MfTLSCipherSuites=DEFAULT',
               # Configure MFCS log directory
               '--BasicConfig.LogDirectory=/var/mfcobol/escwa/logs'
               ]
        env:
        # Password required to connect to OpenLDAP server
        - name: LDAP_PASSWORD
          valueFrom:
            secretKeyRef:
              name: ldap-secret
              key: ldap-password
        - name: ACTIVE_ESM_ID
          value: "open-ldap-esm-uid"
        # Server certificate for OpenLDAP server
        - name: OPENLDAP_CAROOT
          value: /var/run/secrets/microfocus/ca.crt
        ports:
        - containerPort: 10086
          protocol: TCP
        volumeMounts:
        - mountPath: /home/esadm/certs/EscwaKey.pem
          name: certificate-secret-volume
          subPath: tls.key
          readOnly: true
        - mountPath: /home/esadm/certs/EscwaCertificate.pem
          name: certificate-secret-volume
          subPath: tls.crt
          readOnly: true
        - mountPath: /var/mfcobol/escwa/logs
          name: escwa-mfcs-volume
        - mountPath: /var/run/secrets/microfocus/ca.crt
          name: certificate-secret-volume
          subPath: ca.crt
          readOnly: false
        readinessProbe:
          exec:
            command:
            - curl
            - -k
            - https://localhost:10086
          initialDelaySeconds: 30
          periodSeconds: 10          
        livenessProbe:
          exec:
            command:
            - curl
            - -k
            - https://localhost:10086
          initialDelaySeconds: 60
          periodSeconds: 30          
      # Sidecar for logging mfcs log.html
      - name: mfcs-log
        image: busybox
        args: [/bin/sh, -c, 'while [ ! -f /var/mfcobol/escwa/logs/log.html ]; do sleep 3; done; tail -n+1 -F /var/mfcobol/escwa/logs/log.html']
        volumeMounts:
        - mountPath: /var/mfcobol/escwa/logs
          name: escwa-mfcs-volume
      # Sidecar for kubernetes access
      - name: k8s-sidecar
        image: <registry_url>/kubeproxy:latest
        ports:
            - containerPort: 8081
      imagePullSecrets:
      - name: <registry_name>
      volumes:
      - name: certificate-secret-volume
        secret:
          secretName: example-org-tls
      - name: ldap-certificate-secret-volume
        secret:
          secretName: bankdemo-ldap-tls
      - name: escwa-mfcs-volume
        emptyDir: {}
---
kind: Service
apiVersion: v1
metadata:
  name: microfocus-escwa
spec:
  selector:
    app: microfocus-escwa
  ports:
    - name: http
      protocol: TCP
      port: 10086
      targetPort: 10086
  type: NodePort
