#!/bin/sh

# Setup the Micro Focus environment variables
. $MFPRODBASE/bin/cobsetenv $MFPRODBASE

# Populate the vault with the required credentials
$COBDIR/bin/mfsecretsadmin write microfocus/escwa/ESM/$ACTIVE_ESM_ID $LDAP_PASSWORD
unset LDAP_PASSWORD

# Setup trusted root certificate for OpenLDAP server
echo TLS_CACERT $OPENLDAP_CAROOT > ~/.ldaprc

# Start ESCWA with the arguments passed to this script
$COBDIR/bin/escwa "$@"