# Hands on lab container demo
- An example of using containers to build and then run COBOL Server and Enterprise Server applications in Kubernetes, 
the applications can be deployed into AWS EKS (used by the enablement hands-on-lab), Azure AKS or on-premise Kind clusters.

## Overview
- This directory contains applications and scripts which demonstrate how you can use Micro Focus Visual COBOL and Enterprise
Developer Linux containers to build and run simple applications. The applications are built using Jenkinsfile pipelines.
- A Jenkins CI system is setup on nwb-su15demo to help validate changes.

## Multi-cloud
- Scripts have been created to allow the built containers to be deployed in AWS EKS, Azure AKS or local Kind Kubernetes clusters.
- use deployment/common/createcluster.ps1 to create and deploy the cluster including the bankdemo application.

## Updating for an COBOL product version
To update for a new COBOL version:
1. Create new jobs in nwb-su15demo to download and build the the vcdevhub, cobolserver, entdevhub and entserver containers (e.g. create based on the existing jobs)
2. Update the Dockerfiles (Bankdemo/System and MicroFocus/System) to reference the new tag e.g. microfocus/entserver:sles15.1_7.0_x64 becomes microfocus/entserver:sles15.1_8.0_x64
3. Update deployment/hol/aws-setupjenkins.sh to reference the new tag (as above)
4. Update the yaml files to change the deployment, service etc references for the new version e.g. ed70- becomes ed80-, vc70- becomes vc80-
5. Update Bankdemo/System/copymodule.sh to change the service reference for the new version e.g. ed70- becomes ed80-
6. Update Bankdemo/frontend/page/assets/js/main.js, Bankdemo/frontend/src/main.go to change the deployment, service etc references for the new version e.g. ed70- becomes ed80-
7. Update MicroFocals/Vugen/MicroFocus_http/data/CodeGenerationLog.txt to change the deployment, service etc references for the new version e.g. vc70- becomes vc80-
8. MicroFocals/Vugen/MicroFocus_http/data/http.db/resp_002_001.dat to change the deployment, service etc references for the new version e.g. vc70- becomes vc80-
9. MicroFocals/Vugen/MicroFocus_http/data/t1_main.html to change the deployment, service etc references for the new version e.g. vc70- becomes vc80-
10. MicroFocals/Vugen/MicroFocus_http/replay.har to change the deployment, service etc references for the new version e.g. vc70- becomes vc80-
11. MicroFocals/frontend/page/assets/js/main.js to change the deployment, service etc references for the new version e.g. vc70- becomes vc80-
12. MicroFocals/frontend/src/main.go to change the deployment, service etc references for the new version e.g. vc70- becomes vc80-
13. Update the ps1 files to change the deployment, service etc references for the new version e.g. ed70- becomes ed80-, vc70- becomes vc80-
14. Update deployment/config/odbc.ini e.g. ed70- becomes ed80-
15. Update escwa/escwa.yaml e.g. ed70- becomes ed80-
16. Run the Build_Hol_ Jenkins jobs successfully
17. At this stage you could deploy to your own cloud (e.g. Azure to test) or...
18. Run the save_suse_images Jenkins job to update the AWS S3 bucket with the newly built images
19. Deploy in AWS and test

## Keeping current with Kubernetes etc
- The utilities and infrastructure used by the demo is constantly being updated, so changes sometimes need to be made to yaml files, Helm commands to keep current.