# Visual COBOL in Kubernetes
# Creating a COBOL service for SOA region 
1. Open Eclipse
2. Import project from C:\hol-containerdemo\MicroFocals\projects\libraries
2. Open libraries\COBOL Programs\locatestore.cbl in the editor
3. Locate find-stores section (line 108)
    * Right click->Extrace Section/Paragraph to Prgram
    * Enter FindStores.cbl in the edit box and press OK
4. Select the project, right click->New -> REST web service
    * Enter the name: FindStoreService
    * Select Mapping: None radio button
    * Click Browse, Select libraries/FindStores.cbl, press Finish
5. In COBOL Explorer, select Web Services, FindStoreService
6. Right Click->New->Operation
    * Enter the Operation name FINDSTORE, press OK
7. In LoanCalculator editor, LOANCALC Operation - Interface Fields pane 
    * Drag and drop the BRE\_INP\_INPUT\_STRUCTURE from the Linkage Section pane into the Interface Fields pane
    * Drag and drop the BRE\_OUT\_OUTPUT\_STRUCTURE from the Linkage Section pane into the Interface Fields pane
    * Right Click on BRE\_OUT\_OUTPUT\_STRUCTURE in the Interface Fields pane->Properties
    * Change direction to Output, press OK 
8. Select the libraries->Web Services->FindStoreService in the COBOL Explorer view
    * Right click->Properties,
    * Click the Advanced... button, change the base path to /MicroFocals
    * Switch to the Application Files tab
    * Set the Deployed application path to: /home/esadm/deploy/loadlib
    * Press OK
9. Select the libraries project in the COBOL Explorer view
    * Right click->Properties
    * Click on Micro Focus->Build Configurations->Link
    * Set Package services as COBOL archive (.car) files to Yes
    * Press Apply and Close
10. Edit Dockerfile under C:\hol-containerdemo\MicroFocals\System
    * In the publish stage add the following with the other COPY statements (around line 24):
```
COPY --from=BuildMicroFocals /MicroFocals/Projects/libraries/repos/FindStoreService.deploy/*.car /home/esadm/deploy/packages/
```
    * The following around line 39:
```
COPY data/ /home/esadm/data/
```
    * The following around line 41:
```
ENV MFOCAL_STORES_DIRECTORY /home/esadm/data
ENV MFOCALDIR /home/esadm/data/gb
```
    * Within the RUN block which starts mfds, add the following before the mv statement (around line 54):
```
$COBDIR/bin/mfds64 -g 5 /home/esadm/deploy/$ES_SERVER.xml S && \
cd /home/esadm/deploy/packages && $COBDIR/bin/mfdepinst -s --server SERVICE --listener "Web Services and J2EE" FindStoreService.car && \
```
11. Click Window->Show Perspective->Other->Git
    * Click on the Git Staging view select the following files:
    ** .cobolBuild - MicroFocals/Projects/libraries
    ** .cobolProj - MicroFocals/Projects/libraries
    ** .inventory.si - MicroFocals/Projects/libraries
    ** Dockerfile - MicroFocals/system
    ** locatestore.cbl - MicroFocals/Projects/libraries
    ** .gitignore - MicroFocals/Projects/libraries/repos
    ** FindStores.cbl - MicroFocals/Projects/libraries
    ** FindStores.xml - MicroFocals/Projects/libraries/repos
    ** FindStoresService.rtc - MicroFocals/Projects/libraries/repos
    ** FindStoresService.xml - MicroFocals/Projects/libraries/repos
    * Click the green + to stage the change
    * Enter a Commit message 
    * Press Commit and Push...
12. Kick off a build in Jenkins
13. Edit the deployment file C:\hol-containerdemo\deployment\cloudp-backend_microfocals.yaml
    * Replace all occurrences of the referenced tag master_10 with the updated tag from jenkins
    * Run kubectl apply -f cloudp-backend_microfocals.yaml
    * Run kubectl apply -f cloudp-frontend_microfocals.yaml
14. Open the front-end in the browser and test